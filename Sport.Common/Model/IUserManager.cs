﻿namespace Sport.Common.Model
{
    // used in DataContext 
    public interface IUserManager
    {
        /// <summary>
        /// Returns username  of current user
        /// </summary>
        string CurrentUsername { get; }
    }
}