﻿using System;

namespace Sport.Common.Extensions
{
    public interface IHasMigrationId
    {
        Guid MigrationId { get; set; } 
    }
}