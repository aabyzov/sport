﻿namespace Sport.Common.Model
{
    public interface IHasTimestamp
    {
        byte[] Timestamp { get; set; }
    }
}