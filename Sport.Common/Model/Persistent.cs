﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sport.Common.Extensions;

namespace Sport.Common.Model
{
    //[DebuggerDisplay("Id={Id}")]
    public abstract class Persistent : IHasPrimaryKey, IEquatable<Persistent>
    {
        //protected Persistent()
        //{
        //    //Id = Guid.Empty;
        //}

        
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public bool Equals(Persistent other)
        {
            if (other == null) return false;
            return Id == other.Id;
        }
    }
}