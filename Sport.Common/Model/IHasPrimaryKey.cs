﻿using System;

namespace Sport.Common.Extensions
{
    public interface IHasPrimaryKey
    {
        Guid Id { get; set; }
    }
}