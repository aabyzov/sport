﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;

namespace Sport.Common.Validation
{
    public class ValidationResult
    {
        private readonly IList<Tuple<string, bool>> _messages = new List<Tuple<string, bool>>();


        public static ValidationResult Default
        {
            get
            {
                return new ValidationResult();
            }
        }
        public bool SkipWarnings { get; set; }

        private const string Separator = ".  ";

        public ValidationResult()
        {

        }

        public ValidationResult(IEnumerable<DbEntityValidationResult> dbEntityValidationResults)
        {
            //var entityValidationResults = dbEntityValidationResults as IList<DbEntityValidationResult> ?? dbEntityValidationResults.ToList();
            if (dbEntityValidationResults == null) return;
            var entityValidationResults = dbEntityValidationResults.ToList();
            foreach (var result in entityValidationResults)
            {
                AddMessage(string.Join(Separator, result.ValidationErrors));
            }
        }


        /// <summary>
        /// If the result has any messages, it's automatically considered invalid.
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (this.SkipWarnings)
                {
                    return _messages.All(m => !m.Item2);
                }

                return !_messages.Any();
            }
        }

        /// <summary>
        /// Error or warning messages that can be displayed in the UI.
        /// </summary>
        public string[] Messages
        {
            get { return this._messages.Select(m => m.Item1).ToArray(); }
        }

        /// <summary>
        /// Error messages that can be displayed in the UI.
        /// </summary>
        public string[] ErrorMessages
        {
            get { return this._messages.Where(x => x.Item2).Select(x => x.Item1).ToArray(); }
        }

        /// <summary>
        /// Warning messages that can be displayed in the UI.
        /// </summary>
        public string[] WarningMessages
        {
            get { return this._messages.Where(x => !x.Item2).Select(x => x.Item1).ToArray(); }
        }

        public ValidationResult AddMessage(string message, bool isError = true)
        {
            this._messages.Add(new Tuple<string, bool>(message, isError));
            return this;
        }

        public ValidationResult AddError(string message)
        {
            return this.AddMessage(message, true);
        }

        public T CopyTo<T>(T destination) where T : ValidationResult
        {
            if (destination == null)
            {
                return null;
            }

            foreach (var m in _messages)
            {
                destination.AddMessage(m.Item1, m.Item2);
            }

            return destination;
        }
    }
}