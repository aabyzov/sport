﻿using System;
using ILogger = Castle.Core.Logging.ILogger;

namespace Sport.Common.Utility
{
    /// <summary>
    /// This wrapper should be used for logging. It requires initialization on applicatiobn start, otherwise logging will be off.
    /// For example: 
    ///             var nLogFactory = new NLogFactory();
    ///             Logger.SetLogger(() => nLogFactory.Create("agiboo.common"));
    /// </summary>
    public class Logger
    {
        private static Func<ILogger> _getInstance;

        public static void SetLogger(Func<ILogger> getInstance)
        {
            _getInstance = getInstance;
        }

        public static ILogger Current
        {
            get
            {
                if (_getInstance == null) 
                    return new DummyLogger();

                return _getInstance() ?? new DummyLogger();
            }
        }
    }
}