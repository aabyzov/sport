using System;
using Castle.Core.Logging;

namespace Sport.Common.Utility
{
    /// <summary>
    /// Used to degrade gracefully if logging wasnt initialized
    /// </summary>
    public class DummyLogger: ILogger
    {

        public bool IsDebugEnabled { get; private set; }

        public bool IsInfoEnabled { get; private set; }

        public bool IsWarnEnabled { get; private set; }

        public bool IsErrorEnabled { get; private set; }

        public bool IsFatalEnabled { get; private set; }

        public bool IsFatalErrorEnabled { get; private set; }

        public void Debug(string message)
        {
        }

        public void Debug(Func<string> messageFactory)
        {
            
        }

        public void Debug(string message, Exception exception)
        {
        }

        public void Debug(string format, params object[] args)
        {
        }

        public void DebugFormat(string format, params object[] args)
        {
        }

        public void DebugFormat(Exception exception, string format, params object[] args)
        {
        }

        public void DebugFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
        }

        public void DebugFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
        }

        public void Info(string message)
        {
        }

        public void Info(Func<string> messageFactory)
        {
            
        }

        public void Info(string message, Exception exception)
        {
        }

        public void Info(string format, params object[] args)
        {
        }

        public void InfoFormat(string format, params object[] args)
        {
        }

        public void InfoFormat(Exception exception, string format, params object[] args)
        {
        }

        public void InfoFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
        }

        public void InfoFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
        }

        public void Warn(string message)
        {
        }

        public void Warn(Func<string> messageFactory)
        {
            
        }

        public void Warn(string message, Exception exception)
        {
        }

        public void Warn(string format, params object[] args)
        {
        }

        public void WarnFormat(string format, params object[] args)
        {
        }

        public void WarnFormat(Exception exception, string format, params object[] args)
        {
        }

        public void WarnFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
        }

        public void WarnFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
        }

        public void Error(string message)
        {
        }

        public void Error(Func<string> messageFactory)
        {
            
        }

        public void Error(string message, Exception exception)
        {
        }

        public void Error(string format, params object[] args)
        {
        }

        public void ErrorFormat(string format, params object[] args)
        {
        }

        public void ErrorFormat(Exception exception, string format, params object[] args)
        {
        }

        public void ErrorFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
        }

        public void ErrorFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
        }

        public void Fatal(string message)
        {
        }

        public void Fatal(Func<string> messageFactory)
        {
            
        }

        public void Fatal(string message, Exception exception)
        {
        }

        public void Fatal(string format, params object[] args)
        {
        }

        public void FatalFormat(string format, params object[] args)
        {
        }

        public void FatalFormat(Exception exception, string format, params object[] args)
        {
        }

        public void FatalFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
        }

        public void FatalFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
        }

        public void FatalError(string message)
        {
        }

        public void FatalError(string message, Exception exception)
        {
        }

        public void FatalError(string format, params object[] args)
        {
        }

        public ILogger CreateChildLogger(string loggerName)
        {
            return null;
        }
    }
}