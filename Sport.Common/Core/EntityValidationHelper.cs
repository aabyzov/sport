﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Sport.Common.Extensions;

namespace Sport.Common.Core
{
    public class EntityValidationHelper<T> where T : IHasPrimaryKey
    {
        public EntityValidationHelper(Expression<Func<T, bool>> validationExpression, string field, string errorMessage)
        {
            ValidationExpression = validationExpression;
            Errors = new Dictionary<string, string> { { field, errorMessage } };
        }

        public EntityValidationHelper<T> AddError(string field, string errorMessage)
        {
            Errors.Add(field, errorMessage);
            return this;
        }

        public Expression<Func<T, bool>> ValidationExpression { get; private set; }

        public Dictionary<string, string> Errors { get; set; }
    }
}