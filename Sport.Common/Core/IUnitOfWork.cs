﻿namespace Sport.Common.Core
{
    /// <summary>
    /// Defines a Unit Of Work needed to share a DbContext among several repositories.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Maybe this should be called Commit().
        /// </summary>
        void Save();
    }
}