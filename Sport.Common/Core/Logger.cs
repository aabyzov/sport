﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using NLog;

namespace Sport.Common.Core
{
    public class Logger : ILogger
    {
        private readonly global::NLog.Logger _logger;
        private readonly string _prefix;

        public Logger(string logger, string prefix)
        {
            if (logger == null) throw new ArgumentNullException("logger");
            _logger = LogManager.GetLogger(logger);
            _prefix = prefix;
        }

        /// <summary>
        /// Logs an exception as an error (non-fatal).
        /// </summary>
        /// <param name="ex">The exception to log. Should not be <c>null</c>.</param>
        public void LogError(Exception ex)
        {
            if (null == ex)
            {
                _logger.Error("LogError was called but the exception is NULL.");
                return;
            }

            string info = string.Empty;
            while (ex != null)
            {
                info += ex.Message + " \r\n";
                ex = ex.InnerException;
            }
            info = _prefix + ": " + info;

            _logger.Log(typeof(Logger), LogEventInfo.Create(LogLevel.Error, _logger.Name, info, ex));
            Debug.WriteLine("{0} [{1}]: {2}", _prefix, LogLevel.Error, info);
            //AG. ex will be null here.
            //Debug.WriteLine(ex.ToString());
        }

        /// <summary>
        /// Log an information message.
        /// </summary>
        /// <param name="message">The message, may contain .NET string formatting placeholders.</param>
        /// <param name="args">Optional parameters for the message.</param>
        public void LogInfo(string message, params object[] args)
        {
            if (message == null)
                return;

            Log(LogLevel.Info, message, args);
        }

        /// <summary>
        /// Log a debug message.U se debug messages to provide additional information for troublshooting.
        /// </summary>
        /// <param name="message">The message, may contain .NET string formatting placeholders.</param>
        /// <param name="args">Optional parameters for the message.</param>
        public void LogDebug(string message, params object[] args)
        {
            if (message == null)
                return;

            Log(LogLevel.Debug, message, args);
        }

        /// <summary>
        /// Log trace message. Trace is the lowest level and may be used for detailed messages, it's ok for these messages to be verbose.
        /// </summary>
        /// <param name="message">The message, may contain .NET string formatting placeholders.</param>
        /// <param name="args">Optional parameters for the message.</param>
        public void LogTrace(string message, params object[] args)
        {
            if (message == null)
                return;

            Log(LogLevel.Trace, message, args);
        }

        /// <summary>
        /// Log an error message.
        /// </summary>
        /// <param name="message">The message, may contain .NET string formatting placeholders.</param>
        /// <param name="args">Optional parameters for the message.</param>
        /// <remarks>Use error messages to log recoverable errors and exceptions.</remarks>
        public void LogError(string message, params object[] args)
        {
            if (message == null)
                return;

            Log(LogLevel.Error, message, args);
        }

        /// <summary>
        /// Log a fatal error message. Use fatal messages to log errors and exceptions that stop the software from functioning properly.
        /// </summary>
        /// <param name="message">The message, may contain .NET string formatting placeholders.</param>
        /// <param name="args">Optional parameters for the message.</param>
        public void LogFatal(string message, params object[] args)
        {
            if (message == null)
                return;

            Log(LogLevel.Fatal, message, args);
        }

        /// <summary>
        /// Log a warning message. Use warning messages to log problems that require attention but are not preventing the current task from completing.
        /// </summary>
        /// <param name="message">The message, may contain .NET string formatting placeholders.</param>
        /// <param name="args">Optional parameters for the message.</param>
        public void LogWarning(string message, params object[] args)
        {
            if (message == null)
                return;

            Log(LogLevel.Warn, message, args);
        }

        private void Log(LogLevel level, string message, params object[] args)
        {
            _logger.Log(typeof(Logger), LogEventInfo.Create(level, _logger.Name, CultureInfo.InvariantCulture, _prefix + ": " + message, args));
            Debug.WriteLine("{0} [{1}]: {2}", _prefix, level, SafeFormat(message, args));
        }

        private string SafeFormat(string format, params object[] args)
        {
            try
            {
                return string.Format(format, args);
            }
            catch (FormatException)
            {
                // safe fallback in case of a missing argument(s)
                return string.Format(format, args.Concat(new[] { "?", "?", "?", "?", "?", "?", "?", "?", "?", "?" }).ToArray());
            }
        }
    }
}