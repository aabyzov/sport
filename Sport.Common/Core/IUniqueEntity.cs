﻿using System.Collections.Generic;
using Sport.Common.Extensions;

namespace Sport.Common.Core
{
    public interface IUniqueEntity<T> where T : IHasPrimaryKey
    {
        List<EntityValidationHelper<T>> CheckForUnique();
    }
}