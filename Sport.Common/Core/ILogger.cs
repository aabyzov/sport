﻿using System;

namespace Sport.Common.Core
{
    public interface ILogger
    {
        /// <summary>
        /// Log an error message. 
        /// </summary>
        /// <remarks>Use error messages to log recoverable errors and exceptions.</remarks>
        /// <param name="message">The message, may contain .NET string formatting placeholders.</param>
        /// <param name="args">Optional parameters for the message.</param>
        void LogError(string message, params object[] args);

        /// <summary>
        /// Logs an exception as an error (non-fatal).
        /// </summary>
        /// <param name="ex">The exception to log. Should not be <c>null</c>.</param>
        void LogError(Exception ex);

        /// <summary>
        /// Log an information message.
        /// </summary>
        /// <param name="message">The message, may contain .NET string formatting placeholders.</param>
        /// <param name="args">Optional parameters for the message.</param>
        void LogInfo(string message, params object[] args);

        /// <summary>
        /// Log a debug message.U se debug messages to provide additional information for troublshooting.
        /// </summary>
        /// <param name="message">The message, may contain .NET string formatting placeholders.</param>
        /// <param name="args">Optional parameters for the message.</param>
        void LogDebug(string message, params object[] args);

        /// <summary>
        /// Log trace message. Trace is the lowest level and may be used for detailed messages, it's ok for these messages to be verbose.
        /// </summary>
        /// <param name="message">The message, may contain .NET string formatting placeholders.</param>
        /// <param name="args">Optional parameters for the message.</param>
        void LogTrace(string message, params object[] args);

        /// <summary>
        /// Log a fatal error message. Use fatal messages to log errors and exceptions that stop the software from functioning properly.
        /// </summary>
        /// <param name="message">The message, may contain .NET string formatting placeholders.</param>
        /// <param name="args">Optional parameters for the message.</param>
        void LogFatal(string message, params object[] args);

        /// <summary>
        /// Log a warning message. Use warning messages to log problems that require attention but are not preventing the current task from completing.
        /// </summary>
        /// <param name="message">The message, may contain .NET string formatting placeholders.</param>
        /// <param name="args">Optional parameters for the message.</param>
        void LogWarning(string message, params object[] args);
    }
}