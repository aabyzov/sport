﻿using System.Globalization;

namespace Sport.Common.Core
{
    public class Constants
    {
        /// <summary>
        /// The ToString("Format") requires the format without {0:}
        /// </summary>
        public static string DATE_FORMAT_NET_TOSTRING
        {
            get
            {
                CultureInfo cultureInfo = UserPreferredCulture;
                return cultureInfo.DateTimeFormat.ShortDatePattern;
            }
        }

        public static CultureInfo UserPreferredCulture
        {
            get
            {
                return System.Threading.Thread.CurrentThread.CurrentUICulture;
            }
        }

        /// <summary>
        /// Format to display date and time
        /// </summary>
        public static string DATE_TIME_FORMAT_NET_TOSTRING
        {
            get
            {
                CultureInfo cultureInfo = UserPreferredCulture;
                return cultureInfo.DateTimeFormat.FullDateTimePattern;
            }
        }

        public static class Errors
        {
            public static string MissingMatchPlayerInEvents { get { return "The player {0} that misses match can't be in an event {1}"; } }
        }

        public static string International { get { return "International"; } }
        public static string Host { get { return "Host"; } }
        public static string WorldCup { get { return "World Cup"; } }
        public static int Year2006 { get { return 2006;}}
        public static string Football { get { return "Football"; } }
        public static string GroupStage { get { return "Group Stage"; } }
        public static string PlayoffStage { get { return "Playoff Stage"; } }
        public static string GroupA { get { return "Group A"; } }
        public static string GroupB { get { return "Group B"; } }
        public static string Friendly { get { return "Friendl"; } }

        public static class Zones
        {
            public static string Europe { get { return "UEFA"; } }
            public static string SouthAmerica { get { return "CONMEBOL"; } }
            public static string Oceania { get { return "OFC"; } }
            public static string NorthAmerica { get { return "CONCACAF"; } }
            public static string Africa { get { return "CAF"; } }
            public static string Asia { get { return "AFC"; } }
            public static string Friendly { get { return "Friendly"; } }
            public static string Host { get { return "Host"; } }

        }
        public static class Teams
        {
            public static string France { get { return "France"; } }
            public static string England { get { return "England"; } }
            public static string Italy { get { return "Italy"; } }
            public static string Germany { get { return "Germany"; } }
            public static string Spain { get { return "Spain"; } }
            public static string Brazil { get { return "Brazil"; } }
            public static string Argentina { get { return "Argentina"; } }
        }

        public static class Players
        {
            public static string Rooney { get { return "Rooney"; } }
            public static string Walcott { get { return "Walcott"; } }
            public static string Welbeck { get { return "Welbeck"; } }
            public static string Lampard { get { return "Lampard"; } }
            public static string Gerrard { get { return "Gerrard"; } }
            public static string Totti { get { return "Totti"; } }
            public static string Balotelli { get { return "Balotelli"; } }
            public static string Cassano { get { return "Cassano"; } }
            public static string Pirlo { get { return "Pirlo"; } }
            public static string Nesta { get { return "Nesta"; } }
            public static string Schweinsteiger { get { return "Schweinsteiger"; } }
            public static string Ballack { get { return "Ballack"; } }
            public static string Muller { get { return "Muller"; } }
            public static string Klose { get { return "Klose"; } }
            public static string Ozil { get { return "Ozil"; } }
            public static string Ribery { get { return "Ribery"; } }
            public static string Zidane { get { return "Zidane"; } }
            public static string Nasri { get { return "Nasri"; } }
            public static string Henry { get { return "Henry"; } }
            public static string Trezeguet { get { return "Trezeguet"; } }
            public static string Villa { get { return "Villa"; } }
            public static string Torres { get { return "Torres"; } }
            public static string Xavi { get { return "Xavi"; } }
            public static string Iniesta { get { return "Iniesta"; } }
            public static string Silva { get { return "Silva"; } }
            public static string CristianoRonaldo { get { return "Cristiano Ronaldo"; } }
            public static string Nani { get { return "Nani"; } }
            public static string Ronaldo { get { return "Ronaldo"; } }
            public static string Neymar { get { return "Neymar"; } }
            public static string Hulk { get { return "Hulk"; } }
            public static string RobertoCarlos { get { return "Roberto Carlos"; } }
            public static string Oscar { get { return "Oscar"; } }
            public static string Messi { get { return "Messi"; } }
            public static string Tevez { get { return "Tevez"; } }
            public static string Zanetti { get { return "Zanetti"; } }
            public static string Cambiasso { get { return "Cambiasso"; } }
            public static string Batistuta { get { return "Batistuta"; } }
        }

    }
}