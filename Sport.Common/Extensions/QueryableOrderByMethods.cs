﻿using System.Linq;
using System.Reflection;

namespace Sport.Common.Extensions
{
    public class QueryableOrderByMethods : IOrderByMethods
    {
        public QueryableOrderByMethods()
        {
            OrderByMethod = typeof(Queryable).GetMethods().Single(method => method.Name == "OrderBy"
                                                                                   && method.IsGenericMethodDefinition
                                                                                   && method.GetGenericArguments().Length == 2
                                                                                   && method.GetParameters().Length == 2);
            OrderByDescendingMethod = typeof(Queryable).GetMethods().Single(method => method.Name == "OrderByDescending"
                                                                                             && method.IsGenericMethodDefinition
                                                                                             && method.GetGenericArguments().Length == 2
                                                                                             && method.GetParameters().Length == 2);
            OrderThenByMethod = typeof(Queryable).GetMethods().Single(method => method.Name == "ThenBy"
                                                                                       && method.IsGenericMethodDefinition
                                                                                       && method.GetGenericArguments().Length == 2
                                                                                       && method.GetParameters().Length == 2);
            OrderThenByDescendingMethod = typeof(Queryable).GetMethods().Single(method => method.Name == "ThenByDescending"
                                                                                                 && method.IsGenericMethodDefinition
                                                                                                 && method.GetGenericArguments().Length == 2
                                                                                                 && method.GetParameters().Length == 2);
        }

        public MethodInfo OrderByMethod { get; set; }
        public MethodInfo OrderByDescendingMethod { get; set; }
        public MethodInfo OrderThenByMethod { get; set; }
        public MethodInfo OrderThenByDescendingMethod { get; set; }
    }

    public interface IOrderByMethods
    {
        MethodInfo OrderByMethod { get; set; }
        MethodInfo OrderByDescendingMethod { get; set; }
        MethodInfo OrderThenByMethod { get; set; }
        MethodInfo OrderThenByDescendingMethod { get; set; }
    }
}