﻿using System;

namespace Sport.Common.Extensions
{
    public static class ExtensionCaseInsensitiveContains
    {
        public static bool Contains(this string source, string value, StringComparison comparisonType)
        {
            return source.IndexOf(value, comparisonType) != -1;
        }

        public static bool Contains(this string source, string value, bool ignoreCase)
        {
            return ignoreCase ? source.Contains(value, StringComparison.InvariantCultureIgnoreCase) : source.Contains(value, StringComparison.InvariantCulture);
        }

        public static bool ContainsIgnoreCase(this string source, string value)
        {
            return source.Contains(value, true);
        }
    }
}