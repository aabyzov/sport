﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Sport.Common.Extensions
{
    public static class OrderExtensions
    {
        /// <summary>
        /// 	Creates the filter expression
        /// </summary>
        /// <typeparam name = "T">The name of the property</typeparam>
        /// <typeparam name="R">The type of the result</typeparam>
        /// <param name = "property">The filter column</param>
        /// <returns>The filter expression</returns>
        public static Expression<Func<T, R>> From<T, R>(string property)
        {
            //Get parameter
            ParameterExpression c = Expression.Parameter(typeof(T), "c");
            //Get property from <T>
            Expression tagsProperty = c;

            if (property.Contains("."))
            {
                // nested property: obj.Prop1.Prop2.PropX
                string[] properties = property.Split(new[] { '.' });
                tagsProperty = properties.Aggregate(tagsProperty, (current, item) => Expression.Property(current, item));
            }
            else
            {
                tagsProperty = Expression.Property(tagsProperty, property);
            }

            return Expression.Lambda<Func<T, R>>(tagsProperty, c);
        }

        // http://confluence.jetbrains.net/display/ReSharper/Static+field+in+generic+type
        // static readonly QueryableOrderByMethods queryableOrderByMethods = new QueryableOrderByMethods();
        // static readonly EnumerableOrderByMethods enumerableOrderByMethods = new EnumerableOrderByMethods();

        /// <summary>
        /// Orders a queryable collection ascending or descending depending on the user input
        /// </summary>
        /// <typeparam name="T">The type of the elements in the collection</typeparam>
        /// <param name="collection">The collection to be ordered</param>
        /// <param name="direction">Direction of sorting</param>
        /// <param name="fieldName">The field to be sorted upon</param>
        /// <returns>The fieldName in the queryString can be a nested field: Property.NestedProperty</returns>
        public static IQueryable<T> OrderBy<T>(this IQueryable<T> collection, ListSortDirection direction, string fieldName)
        {
            return collection.OrderBy(string.Format("{0}|{1}", fieldName, direction == ListSortDirection.Ascending ? "asc" : "desc"));
        }

        /// <summary>
        /// 	Orders a queryable collection ascending or descending depending on the user input
        /// </summary>
        /// <typeparam name = "T">The type of the elements in the collection</typeparam>
        /// <param name = "collection">The collection to be ordered</param>
        /// <param name = "queryString">The user input (must be in the form: fieldName|asc/desc, fieldName|asc/desc).</param>
        /// <returns>The ordered collection</returns>
        /// <remarks>
        /// 	The fieldName in the queryString can be a nested field: Property.NestedProperty
        /// </remarks>
        public static IQueryable<T> OrderBy<T>(this IQueryable<T> collection, string queryString)
        {
            return OrderBy<T, IQueryable<T>>(collection, queryString, new QueryableOrderByMethods(), l => l); // we use the expression for queryables
        }

        /// <summary>
        /// 	Orders an enumerable collection ascending or descending depending on the user input
        /// </summary>
        /// <typeparam name = "T">The type of the elements in the collection</typeparam>
        /// <param name = "collection">The collection to be ordered</param>
        /// <param name = "queryString">The user input (must be in the form: fieldName|asc/desc, fieldName|asc/desc).</param>
        /// <returns>The ordered collection</returns>
        /// <remarks>
        /// 	The fieldName in the queryString can be a nested field: Property.NestedProperty
        /// </remarks>
        public static IEnumerable<T> OrderBy<T>(this IEnumerable<T> collection, string queryString)
        {
            return OrderBy<T, IEnumerable<T>>(collection, queryString, new EnumerableOrderByMethods(), l => l.Compile()); // we need to compile for Enumerables
        }

        /// <summary>
        /// 	Orders a collection ascending or descending depending on the user input
        /// </summary>
        /// <typeparam name = "T">The type of the elements in the collection</typeparam>
        /// <typeparam name = "Q">The type of the elements in the collection</typeparam>
        /// <param name = "collection">The collection to be ordered</param>
        /// <param name = "queryString">The user input (must be in the form: fieldName|asc/desc, fieldName|asc/desc).</param>
        /// <param name = "orderByMethods">The order by methods to be used</param>
        /// <param name = "PrepareExpression">Lambda expression (can be compiled)</param>
        /// <returns></returns>
        static Q OrderBy<T, Q>(Q collection, string queryString, IOrderByMethods orderByMethods, Func<LambdaExpression, object> PrepareExpression) where Q : class
        {
            if (String.IsNullOrEmpty(queryString))
                return collection;

            var type = typeof(T);
            ParameterExpression argumentParameter = Expression.Parameter(type);

            bool isFirst = true;
            MethodInfo method;
            Type delegateType;
            LambdaExpression lambdaExpression;

            // split the queryString
            foreach (string orderDef in queryString.Split(','))
            {
                //always start from the base type
                type = typeof(T);

                // split the part of the queryString to fieldName & asc/desc
                string[] order = orderDef.Split('|');
                // if the fieldName contains nested properties, we'll split that too
                string[] props = order[0].Split('.');

                Expression expression = argumentParameter;
                foreach (string prop in props)
                {
                    // in case of nested properties we need the type of the leaf property
                    PropertyInfo pi = type.GetProperty(prop);
                    expression = Expression.Property(expression, pi);
                    type = pi.PropertyType;
                }

                // create a Expression<Func<T,TKey>> method:
                delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
                lambdaExpression = Expression.Lambda(delegateType, expression, argumentParameter);

                // select the correct orderBy method
                if (order[1].Trim() == "asc")
                {
                    method = isFirst ? orderByMethods.OrderByMethod : orderByMethods.OrderThenByMethod;
                }
                else
                {
                    method = isFirst ? orderByMethods.OrderByDescendingMethod : orderByMethods.OrderThenByDescendingMethod;
                }

                // invoke the OrderBy method on the collection
                collection = method.MakeGenericMethod(typeof(T), type).Invoke(null, new[] { collection, PrepareExpression(lambdaExpression) }) as Q;

                if (isFirst)
                    isFirst = false;
            }

            return collection;
        }
    }
}