﻿using System.Linq;
using System.Reflection;

namespace Sport.Common.Extensions
{
    public class EnumerableOrderByMethods : IOrderByMethods
    {
        public EnumerableOrderByMethods()
        {
            OrderByMethod = typeof(Enumerable).GetMethods().Single(method => method.Name == "OrderBy"
                                                                             && method.IsGenericMethodDefinition
                                                                             && method.GetGenericArguments().Length == 2
                                                                             && method.GetParameters().Length == 2);
            OrderByDescendingMethod = typeof(Enumerable).GetMethods().Single(method => method.Name == "OrderByDescending"
                                                                                       && method.IsGenericMethodDefinition
                                                                                       && method.GetGenericArguments().Length == 2
                                                                                       && method.GetParameters().Length == 2);
            OrderThenByMethod = typeof(Enumerable).GetMethods().Single(method => method.Name == "ThenBy"
                                                                                 && method.IsGenericMethodDefinition
                                                                                 && method.GetGenericArguments().Length == 2
                                                                                 && method.GetParameters().Length == 2);
            OrderThenByDescendingMethod = typeof(Enumerable).GetMethods().Single(method => method.Name == "ThenByDescending"
                                                                                           && method.IsGenericMethodDefinition
                                                                                           && method.GetGenericArguments().Length == 2
                                                                                           && method.GetParameters().Length == 2);
        }

        public MethodInfo OrderByMethod { get; set; }
        public MethodInfo OrderByDescendingMethod { get; set; }
        public MethodInfo OrderThenByMethod { get; set; }
        public MethodInfo OrderThenByDescendingMethod { get; set; }
    }
}