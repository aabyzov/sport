﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Linq;
using EntityKey = System.Data.Entity.Core.EntityKey;

namespace Sport.Common.Audit
{
    /// <summary>
    /// Original solution for tracking N-M relations taken from: 
    /// http://stackoverflow.com/questions/7490509/entity-framework-4-1-many-to-many-relationships-change-tracking
    /// </summary>
    public static class IaExtensions
    {
        /// <summary>
        /// Return changes in many-to-many independent associations
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static IEnumerable<DbAssociationEntry> GetRelationships(this DbContext context)
        {
            return context.GetAddedRelationships().Union(context.GetDeletedRelationships());
        }
            
        public static IEnumerable<DbAssociationEntry> GetAddedRelationships(
            this DbContext context)
        {
            return GetRelationships(context, System.Data.Entity.EntityState.Added, (e, i) => e.CurrentValues[i]).Select(e => new DbAssociationEntry
                {
                    State = System.Data.Entity.EntityState.Added,
                    End1 = e.Item1,
                    End1Type = ObjectContext.GetObjectType(e.Item1.GetType()),
                    End2 = e.Item3,
                    End2Type = ObjectContext.GetObjectType(e.Item3.GetType()),
                    EntitySet = e.Item2
                });
        }

        public static IEnumerable<DbAssociationEntry> GetDeletedRelationships(
            this DbContext context)
        {
            return GetRelationships(context, System.Data.Entity.EntityState.Deleted, (e, i) => e.OriginalValues[i]).Select(e => new DbAssociationEntry
                {
                    State = System.Data.Entity.EntityState.Deleted,
                    End1 = e.Item1,
                    End1Type = ObjectContext.GetObjectType(e.Item1.GetType()),
                    End2 = e.Item3,
                    End2Type = ObjectContext.GetObjectType(e.Item3.GetType()),
                    EntitySet = e.Item2
                });
        }

        private static IEnumerable<Tuple<object, string, object>> GetRelationships(
            this DbContext context,
            System.Data.Entity.EntityState relationshipState,
            Func<System.Data.Entity.Core.Objects.ObjectStateEntry, int, object> getValue)
        {
            context.ChangeTracker.DetectChanges();
            var objectContext = ((IObjectContextAdapter)context).ObjectContext;

            return objectContext
                .ObjectStateManager
                .GetObjectStateEntries(relationshipState)
                .Where(e => e.IsRelationship)
                .Select(
                    e => Tuple.Create(
                        objectContext.GetObjectByKey((EntityKey) getValue(e, 0)),
                        e.EntitySet.Name,
                        objectContext.GetObjectByKey((EntityKey) getValue(e, 1))))
                ;
        }
    }
}