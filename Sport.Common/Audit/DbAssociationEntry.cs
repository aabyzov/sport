﻿using System;

namespace Sport.Common.Audit
{
    public class DbAssociationEntry
    {
        public System.Data.Entity.EntityState State { get; set; }

        public string EntitySet { get; set; }

        public object End1 { get; set; }

        public object End2 { get; set; }

        public Type End1Type { get; set; }

        public Type End2Type { get; set; }
    }
}