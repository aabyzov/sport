﻿using Sport.Common.Model;

namespace Sport.Common.Audit
{
    public class AuditRecordField : Persistent
    {
        public int AuditRecordId { get; set; }
        public string MemberName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }

        public virtual AuditRecord AuditRecord { get; set; }
    }
}
