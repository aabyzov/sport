﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Reflection;
using System.Threading;
using Sport.Common.Extensions;

namespace Sport.Common.Audit
{
    public class AuditTrail
    {
		#region Variables
		private readonly IEnumerable<DbEntityEntry> _changes;
		private readonly IEnumerable<DbAssociationEntry> _relations;
    	private readonly Dictionary<AuditRecord, object> _dictionary;
        private readonly Dictionary<AuditRecord, object> _associationDictionary;

    	private readonly List<AuditRecord> _auditRecords;

        #endregion

		#region Constructor

        public AuditTrail(IEnumerable<DbEntityEntry> changes)
            : this(changes, null)
        {
        }

        public AuditTrail(IEnumerable<DbEntityEntry> changes, IEnumerable<DbAssociationEntry> relations)
		{
			_changes = changes;
		    _relations = relations;
			_dictionary = new Dictionary<AuditRecord, object>();
            _associationDictionary = new Dictionary<AuditRecord, object>();
			_auditRecords = new List<AuditRecord>();
		}
 
		#endregion

		#region Public methods
		public void ProcessAuditChanges()
		{
			foreach (DbEntityEntry dbEntryEntity in _changes)
			{
				if (dbEntryEntity.Entity != null && !(dbEntryEntity.Entity is AuditRecord) && !(dbEntryEntity.Entity is AuditRecordField))
				{
                    Type entityType = ObjectContext.GetObjectType(dbEntryEntity.Entity.GetType());
					string entityName = entityType.Name;

                    // I suspect this is done because before the EF proxy objects were being used to get the names
                    // So this should be obsolete now
					if (entityName.Contains("_"))
						entityName = entityName.Substring(0, entityName.IndexOf("_"));

					AuditRecord auditRecord = new AuditRecord();
					auditRecord.AuditDate = DateTime.Now;
					auditRecord.EntityTable = entityName;
                    auditRecord.EntityNamespace = entityType.Namespace;
					auditRecord.UserName = Thread.CurrentPrincipal.Identity.Name;

					_dictionary.Add(auditRecord, dbEntryEntity.Entity);

					auditRecord.Action = (byte)Convert(dbEntryEntity.State);

					if (dbEntryEntity.State == EntityState.Modified)
					{
						foreach (string propName in dbEntryEntity.OriginalValues.PropertyNames)
						{
                            // timestamp field updates still should be logged to be able to handle concurrency errors caused by the same user (as one saving now) 
                            if (propName.ToLower().Contains("audit") /*|| propName.ToLower().Contains("timestamp")*/)
								continue;

							PropertyInfo propInfo = dbEntryEntity.Entity.GetType().GetProperty(propName);
							if (propInfo == null || !propInfo.CanWrite)
								continue;

							if (propInfo.Name.ToLower() == "id")
							{
								try
								{
									auditRecord.EntityTableKey = Guid.Parse(dbEntryEntity.GetDatabaseValues()[propName].ToString());
								}
								catch
								{
									// nasty. We'll have to fix this: http://social.msdn.microsoft.com/Forums/en-US/adodotnetentityframework/thread/2f3f3b92-3004-4957-bc51-9c0adbde698c
								}
							}

							object originalValue = null;
							try
							{
								originalValue = dbEntryEntity.GetDatabaseValues()[propName];
							}
							catch
							{
								// nasty. We'll have to fix this: http://social.msdn.microsoft.com/Forums/en-US/adodotnetentityframework/thread/2f3f3b92-3004-4957-bc51-9c0adbde698c
							}

							object currentValue = dbEntryEntity.CurrentValues[propName];

							if (!Equals(originalValue, currentValue))
							{
								AuditRecordField auditField = new AuditRecordField();
								auditField.MemberName = propName;
								auditField.OldValue = ConvertToString(originalValue);
								auditField.NewValue = ConvertToString(currentValue);

								auditRecord.AuditRecordFields.Add(auditField);
							}
						}
					}

                    if (dbEntryEntity.State == EntityState.Added)
					{
						foreach (string propName in dbEntryEntity.CurrentValues.PropertyNames)
						{
							if (propName.ToLower().Contains("audit") || propName.ToLower() == "id")
								continue;

							object currentValue = dbEntryEntity.CurrentValues[propName];

							if (currentValue != null)
							{
								var auditField = new AuditRecordField();
								auditField.MemberName = propName;
								auditField.OldValue = string.Empty;
								auditField.NewValue = currentValue.ToString();

								auditRecord.AuditRecordFields.Add(auditField);
							}
						}
					}

                    if (dbEntryEntity.State == EntityState.Deleted)
					{
						foreach (string propName in dbEntryEntity.OriginalValues.PropertyNames)
						{
							if (propName.ToLower().Contains("audit"))
								continue;

							PropertyInfo propInfo = dbEntryEntity.Entity.GetType().GetProperty(propName);
							if (propInfo == null || !propInfo.CanWrite)
								continue;

                            if (propInfo.Name.ToLower() == "id")
                            {
                                try
                                {
                                    auditRecord.EntityTableKey = Guid.Parse(dbEntryEntity.GetDatabaseValues()[propName].ToString());
                                }
                                catch
                                {
                                    // whoomp. Swallow. EF bug - GetDatabaseValues() doesn't work when mixing namespaces. 
                                }
                            }
						}
					}


					if (dbEntryEntity.State == EntityState.Added|| dbEntryEntity.State == EntityState.Deleted || auditRecord.AuditRecordFields.Count > 0)
					{
						_auditRecords.Add(auditRecord);
					}
				}
			}

            //separately handle independent associations (we are interested there in N-M relations)
            foreach (DbAssociationEntry dbAssociationEntry in _relations)
            {
                if (dbAssociationEntry.End1 is AuditRecord || dbAssociationEntry.End2 is AuditRecord)
                {
                    continue;
                }

                ProcessAssociationEntry(dbAssociationEntry, dbAssociationEntry.End1, dbAssociationEntry.End1Type, dbAssociationEntry.End2, dbAssociationEntry.End2Type);
                ProcessAssociationEntry(dbAssociationEntry, dbAssociationEntry.End2, dbAssociationEntry.End2Type, dbAssociationEntry.End1, dbAssociationEntry.End1Type);
            }
		}

        private void ProcessAssociationEntry(DbAssociationEntry dbAssociationEntry, object end1, Type end1Type, object end2, Type end2Type)
        {
            if (end1 == null || end2 == null || !(end1 is IHasPrimaryKey))
            {
                return;
            }

            AuditRecord auditRecord = new AuditRecord();
            auditRecord.AuditDate = DateTime.Now;
            auditRecord.EntityTable = end1Type.Name;
            auditRecord.EntityNamespace = end1Type.Namespace;
            auditRecord.UserName = Thread.CurrentPrincipal.Identity.Name;
            auditRecord.Action = (byte) Convert(dbAssociationEntry.State);

            auditRecord.EntityTableKey = (end1 as IHasPrimaryKey).Id;
            
            if (end2 is IHasPrimaryKey)
            {
                auditRecord.AssociationTable = dbAssociationEntry.EntitySet;
                auditRecord.AssociationTableKey = (end2 as IHasPrimaryKey).Id;
            }

            _auditRecords.Add(auditRecord);

            _dictionary.Add(auditRecord, end1);
            _associationDictionary.Add(auditRecord, end2);
        }

        private static ExtendedAuditAction Convert(EntityState state)
        {
            switch (state)
            {
                case EntityState.Added:
                    return ExtendedAuditAction.Create;
                case EntityState.Modified:
                    return ExtendedAuditAction.Change;
                case EntityState.Deleted:
                    return ExtendedAuditAction.Delete;
            }

            return ExtendedAuditAction.Change;
        }

        public void ProcessCreatedEntities()
		{
			foreach (AuditRecord auditRecord in _dictionary.Keys)
			{
				if (auditRecord.Action == (byte)ExtendedAuditAction.Create)
				{
					var entity = _dictionary[auditRecord] as IHasPrimaryKey;
					if (entity != null)
					{
						auditRecord.EntityTableKey = entity.Id;
					}
				}
			}			
            
            foreach (AuditRecord auditRecord in _associationDictionary.Keys)
			{
				if (auditRecord.Action == (byte)ExtendedAuditAction.Create)
				{
					var entity = _associationDictionary[auditRecord] as IHasPrimaryKey;
					if (entity != null)
					{
						auditRecord.AssociationTableKey = entity.Id;
					}
				}
			}
		} 
		#endregion

		#region Public properties
		public List<AuditRecord> AuditRecords
		{
			get { return _auditRecords; }
		} 		
		#endregion

        #region Private properties
        private static string ConvertToString(object value)
        {
            if (value == null) return string.Empty;

            if (value is byte[]) 
                return BitConverter.ToString(value as byte[]).Replace("-", "");

            return value.ToString();
        }
        #endregion
    }
}
