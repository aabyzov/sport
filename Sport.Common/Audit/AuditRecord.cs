﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Sport.Common.Core;
using Sport.Common.Model;

namespace Sport.Common.Audit
{
	public class AuditRecord : Persistent
    {
        public AuditRecord()
        {
            this.AuditRecordFields = new HashSet<AuditRecordField>();
        }

        public byte Action { get; set; }
        public string EntityTable { get; set; }
        public string EntityNamespace { get; set; }
        public Guid EntityTableKey { get; set; }
        public string AssociationTable { get; set; }
        public Guid? AssociationTableKey { get; set; }
        public System.DateTime AuditDate { get; set; }
        public string UserName { get; set; }

        public virtual ICollection<AuditRecordField> AuditRecordFields { get; set; }

        [NotMapped]
        public string DateText
        {
            get
            {
                if (AuditDate == DateTime.MinValue)
                    return string.Empty;
                else
					return AuditDate.ToString(Constants.DATE_FORMAT_NET_TOSTRING, Constants.UserPreferredCulture);
            }
        }

		[NotMapped]
		public string DateTimeText
		{
			get
			{
				if (AuditDate == DateTime.MinValue)
					return string.Empty;
				else
                    return AuditDate.ToString(Constants.DATE_TIME_FORMAT_NET_TOSTRING, Constants.UserPreferredCulture);
			}
		}

        [NotMapped]
        public string Reason
        {
            get
            {
                AuditRecordField reason = AuditRecordFields.SingleOrDefault(arf => arf.MemberName == "Audit Reason");
                if (reason == null)
                    return ReasonPrefix + string.Empty;
                else
                    return ReasonPrefix + reason.NewValue;
            }
        }

        [NotMapped]
        public string ReasonPrefix { get; set; }

		[NotMapped]
		public string ActionText
		{
			get { return ((ExtendedAuditAction) Action).ToString(); }
		}
    }
}
