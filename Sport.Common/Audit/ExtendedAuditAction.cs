﻿namespace Sport.Common.Audit
{
    /// <summary>
    /// Do not change this values! They are used in the DB as well!
    /// </summary>
    public enum ExtendedAuditAction : int
    {
        Create = 0,
        Change = 1,
        Delete = 2,
        Cancel = 3,
        Split = 4,
        Merge = 5,
        Allocate = 6,

        // the sub entity realted actions must be in the same order as their main counterpart actions
        SubEntity = 99, // this must be one less than the first sub entity action!
        SubEntity_Create = 100,
        SubEntity_Change = 101,
        SubEntity_Delete = 102,
        SubEntity_Cancel = 103,
        SubEntity_Split = 104,
        SubEntity_Merge = 105,
        SubEntity_Allocate = 106,
    }
}
