cd "C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\IDE\Extensions\Microsoft\Entity Framework Tools\NuGet Packages\EntityFramework.6.0.0\tools"

function Update-Database($solutionDir, $projectBinDir, $assemblyName, $appConfigFile)
{
    $efMigrateExe = "$solutionDir\packages\EntityFramework.6.0.1\tools\migrate.exe"
    Write-Host "Updating Entity Framework Database"
    Write-Host "    Migrate.exe at $efMigrateExe"
    Write-Host "    EF project binary at $projectBinDir"
    Write-Host "    EF config at $appConfigFile"
    . "$efMigrateExe" "$assemblyName" /startupConfigurationFile="$appConfigFile" /startupDirectory="$projectBinDir" /verbose
}

Update-Database "C:\Projects\Git\Sport" "C:\Projects\Git\Sport\Sport.Domain\bin\Debug" Sport.Domain.dll "C:\Projects\Git\Sport\Sport.Web.ResultTracking\web.config"
# migrate.exe Sport.Domain /StartupDirectory:"C:\Projects\Git\Sport\Sport.IntegrationTests\bin" /ConnectionStringName:"Data Source=(local);Initial Catalog=SportDB_Tests;User Id=sa;Password=1;MultipleActiveResultSets=True" /StartupConfigurationFile:"C:\Projects\Git\Sport\Sport.IntegrationTests"
