﻿using System;

namespace Sport.Domain.Services.ScheduleTables
{
    public class FinalsWcScheduleTable : ScheduleTable
    {
        public override StageSchedule[] GetStages(int year)
        {
            var groupMatchDay1 = ScheduleHelper.GetNthDayOfMonthYear(2, DayOfWeek.Thursday, 06, year).AddHours(19);
            var groupMatchDay2 = groupMatchDay1.AddDays(5);
            var groupMatchDay3 = groupMatchDay2.AddDays(5);
            var oneEightDay = groupMatchDay3.AddDays(5);
            var oneFourDay = oneEightDay.AddDays(3);
            var semiFinalDay = oneFourDay.AddDays(2);
            var finalsDay = semiFinalDay.AddDays(2);
            
            var stages = new[]
            {
                new StageSchedule()
                {
                    MatchDays = new []
                            {
                                groupMatchDay1,
                                groupMatchDay2,
                                groupMatchDay3
                            }
                },
                new StageSchedule()
                {
                    MatchDays = new []
                            {
                                oneEightDay
                            }
                },
                new StageSchedule()
                {
                    MatchDays = new []
                            {
                                oneFourDay
                            }
                },
                new StageSchedule()
                {
                    MatchDays = new []
                            {
                                semiFinalDay
                            }
                },
                new StageSchedule()
                {
                    MatchDays = new []
                            {
                                finalsDay
                            }
                }
            };
            return stages;
        }
    }
}