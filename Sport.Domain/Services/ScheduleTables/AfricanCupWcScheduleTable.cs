﻿using System;

namespace Sport.Domain.Services.ScheduleTables
{
    public class AfricanCupWcScheduleTable : ScheduleTable
    {
        public override StageSchedule[] GetStages(int year)
        {
            var thirdSaturdayOfJanuary = ScheduleHelper.GetNthDayOfMonthYear(3, DayOfWeek.Saturday, 1, year - 1);
            var matchDay1 = thirdSaturdayOfJanuary;
            var matchDay2 = matchDay1.AddDays(2);
            var matchDay3 = matchDay2.AddDays(2);
            var matchDay4 = matchDay3.AddDays(2);
            var matchDay5 = matchDay4.AddDays(2);
            var matchDay6 = matchDay5.AddDays(2);
            var matchDay7 = matchDay6.AddDays(2);
            var stages = new[]
            {
                new StageSchedule()
                {
                    MatchDays = new []
                            {
                                matchDay1,
                                matchDay2,
                                matchDay3,
                                matchDay4,
                                matchDay5,
                                matchDay6,
                                matchDay7
                            },
                    Groups = new[]
                    {
                        new GroupSchedule()
                        {
                            Fixtures = new[]
                            {
                                new FixtureSchedule()
                                {
                                    Date = matchDay1.AddHours(18)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay1.AddHours(18)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay1.AddHours(20)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay1.AddHours(20)
                                }, 

                                new FixtureSchedule()
                                {
                                    Date = matchDay2.AddHours(18)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay2.AddHours(18)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay2.AddHours(20)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay2.AddHours(20)
                                }, 

                                new FixtureSchedule()
                                {
                                    Date = matchDay3.AddHours(18)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay3.AddHours(18)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay3.AddHours(20)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay3.AddHours(20)
                                }, 

                                new FixtureSchedule()
                                {
                                    Date = matchDay4.AddHours(18)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay4.AddHours(18)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay4.AddHours(20)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay4.AddHours(20)
                                }, 

                                new FixtureSchedule()
                                {
                                    Date = matchDay5.AddHours(18)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay5.AddHours(18)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay5.AddHours(20)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay5.AddHours(20)
                                }, 

                                new FixtureSchedule()
                                {
                                    Date = matchDay6.AddHours(18)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay6.AddHours(18)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay6.AddHours(20)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay6.AddHours(20)
                                }, 

                                new FixtureSchedule()
                                {
                                    Date = matchDay7.AddHours(19)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay7.AddHours(19)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay7.AddHours(19)
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay7.AddHours(19)
                                }, 
                            }
                        }
                    }
                        
                            
                        
                }
            };
            return stages;
        }
    }
}