﻿using System;

namespace Sport.Domain.Services.ScheduleTables
{
    public class SouthAmericanWcScheduleTable : ScheduleTable
    {
        public override StageSchedule[] GetStages(int year)
        {

            var matchDay1 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Friday, 10, year - 3).AddHours(18);
            var matchDay2 = ScheduleHelper.GetNthDayOfMonthYear(2, DayOfWeek.Friday, 11, year - 3).AddHours(18);
            var matchDay3 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Saturday, 06, year - 2).AddHours(18);
            var matchDay4 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Friday, 09, year - 2).AddHours(18);
            var matchDay5 = ScheduleHelper.GetNthDayOfMonthYear(2, DayOfWeek.Friday, 10, year - 2).AddHours(18);
            var matchDay6 = ScheduleHelper.GetNthDayOfMonthYear(4, DayOfWeek.Friday, 03, year - 1).AddHours(18);
            var matchDay7 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Friday, 06, year - 1).AddHours(18);
            var matchDay8 = ScheduleHelper.GetNthDayOfMonthYear(2, DayOfWeek.Friday, 10, year - 1).AddHours(18);
            var matchDay9 = matchDay7.AddDays(4);
            var stages = new[]
            {
                new StageSchedule()
                {
                    MatchDays = new []
                    {
                        matchDay1,
                        matchDay2,
                        matchDay3,
                        matchDay4,
                        matchDay5,
                        matchDay6,
                        matchDay7
                    },

                    Groups = new[]
                    {
                        new GroupSchedule()
                        {
                            Fixtures = new[]
                            {
                                new FixtureSchedule()
                                {
                                    Date = matchDay1 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay1 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay1 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay1 
                                }, 

                                new FixtureSchedule()
                                {
                                    Date = matchDay2 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay2 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay2 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay2 
                                }, 

                                new FixtureSchedule()
                                {
                                    Date = matchDay3 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay3 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay3 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay3 
                                }, 

                                new FixtureSchedule()
                                {
                                    Date = matchDay4 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay4 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay4 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay4 
                                }, 

                                new FixtureSchedule()
                                {
                                    Date = matchDay5 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay5 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay5 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay5 
                                }, 

                                new FixtureSchedule()
                                {
                                    Date = matchDay6 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay6 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay6 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay6 
                                }, 

                                new FixtureSchedule()
                                {
                                    Date = matchDay7 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay7 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay7 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay7 
                                }, 
                            }
                        }
                    }
                },
                new StageSchedule()
                {
                    MatchDays = new []{ matchDay8, matchDay9}
                }, 
            };
            return stages;
        }
    }
}