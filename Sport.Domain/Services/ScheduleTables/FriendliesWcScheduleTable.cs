﻿using System;

namespace Sport.Domain.Services.ScheduleTables
{
    public class FriendliesWcScheduleTable : ScheduleTable
    {
        public override StageSchedule[] GetStages(int year)
        {
            var matchDay0 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Tuesday, 09, year - 2).AddHours(19);
            var matchDay1 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Sunday, 09, year - 2).AddHours(19);
            var matchDay2 = ScheduleHelper.GetNthDayOfMonthYear(2, DayOfWeek.Sunday, 10, year - 2).AddHours(19);
            var matchDay3 = ScheduleHelper.GetNthDayOfMonthYear(4, DayOfWeek.Sunday, 03, year - 1).AddHours(19);
            var matchDay4 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Sunday, 06, year - 1).AddHours(19);
            var matchDay5 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Sunday, 09, year - 1).AddHours(19);
            var matchDay6 = ScheduleHelper.GetNthDayOfMonthYear(2, DayOfWeek.Sunday, 10, year - 1).AddHours(19);
            var stages = new[]
            {
                new StageSchedule()
                {
                   MatchDays = new []
                            {
                                matchDay0,
                                matchDay1,
                                matchDay2,
                                matchDay3,
                                matchDay4,
                                matchDay5,
                                matchDay6
                            }
                },
                new StageSchedule()
                {
                    MatchDays = new []{matchDay1}
                }, 
                new StageSchedule()
                {
                    MatchDays = new []{matchDay2}
                }, 
                new StageSchedule()
                {
                    MatchDays = new []{matchDay3}
                }, 
                new StageSchedule()
                {
                    MatchDays = new []{matchDay4}
                }, 
                new StageSchedule()
                {
                    MatchDays = new []{matchDay5}
                }, 
                new StageSchedule()
                {
                    MatchDays = new []{matchDay6}
                }, 
            };
            return stages;
        }
    }
}