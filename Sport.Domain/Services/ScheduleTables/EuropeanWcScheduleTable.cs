﻿using System;

namespace Sport.Domain.Services.ScheduleTables
{
    public class EuropeanWcScheduleTable : ScheduleTable
    {
        public override StageSchedule[] GetStages(int year)
        {

            var matchDay1 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Friday, 09, year - 2).AddHours(19);
            var matchDay2 = ScheduleHelper.GetNthDayOfMonthYear(2, DayOfWeek.Friday, 10, year - 2).AddHours(19);
            var matchDay3 = ScheduleHelper.GetNthDayOfMonthYear(4, DayOfWeek.Friday, 03, year - 1).AddHours(19);
            var matchDay4 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Friday, 06, year - 1).AddHours(19);
            var matchDay5 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Friday, 09, year - 1).AddHours(19);
            var matchDay6 = ScheduleHelper.GetNthDayOfMonthYear(2, DayOfWeek.Friday, 10, year - 1).AddHours(19);
            var stages = new[]
            {
                new StageSchedule()
                {
                   MatchDays = new []
                            {
                                matchDay1,
                                matchDay2,
                                matchDay3,
                                matchDay4,
                                matchDay5,
                                matchDay6
                            }
                }
            };
            return stages;
        }
    }
}