﻿using System;

namespace Sport.Domain.Services.ScheduleTables
{
    public class NorthAmericanWcScheduleTable : ScheduleTable
    {
        public override StageSchedule[] GetStages(int year)
        {
            var matchDay1 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Wednesday, 02, year - 2).AddHours(18);
            var matchDay2 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Wednesday, 06, year - 2).AddHours(18);
            var matchDay3 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Wednesday, 09, year - 2).AddHours(18);
            var matchDay4 = ScheduleHelper.GetNthDayOfMonthYear(1, DayOfWeek.Wednesday, 02, year - 1).AddHours(18);
            var matchDay5 = ScheduleHelper.GetNthDayOfMonthYear(2, DayOfWeek.Wednesday, 10, year - 1).AddHours(18);
            var matchDay6 = ScheduleHelper.GetNthDayOfMonthYear(2, DayOfWeek.Wednesday, 11, year - 1).AddHours(18);
            var matchDay7 = ScheduleHelper.GetNthDayOfMonthYear(2, DayOfWeek.Sunday, 11, year - 1).AddHours(18);
            var matchDay8 = ScheduleHelper.GetNthDayOfMonthYear(3, DayOfWeek.Wednesday, 11, year - 1).AddHours(18);
            var stages = new[]
            {
                new StageSchedule()
                {
                     MatchDays = new []
                    {
                        matchDay1,
                        matchDay2,
                        matchDay3,
                        matchDay4,
                        matchDay5,
                        matchDay6
                    },

                    Groups = new[]
                    {
                        new GroupSchedule()
                        {
                            Fixtures = new[]
                            {
                                new FixtureSchedule()
                                {
                                    Date = matchDay1 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay1 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay1 
                                }, 
                                
                                new FixtureSchedule()
                                {
                                    Date = matchDay2 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay3 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay3 
                                }, 

                                new FixtureSchedule()
                                {
                                    Date = matchDay3 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay3 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay4 
                                }, 

                                new FixtureSchedule()
                                {
                                    Date = matchDay4 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay4 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay4 
                                }, 
                                
                                new FixtureSchedule()
                                {
                                    Date = matchDay5 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay5 
                                }, 
                                new FixtureSchedule()
                                {
                                    Date = matchDay5 
                                }, 
                            }
                        }
                    }
                },
                new StageSchedule()
                {
                    MatchDays = new[]
                    {
                        matchDay7,
                        matchDay8
                    }
                }
            };
            return stages;
        }
    }
}