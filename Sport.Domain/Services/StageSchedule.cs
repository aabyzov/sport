﻿using System;

namespace Sport.Domain.Services
{
    public class StageSchedule
    {
        public GroupSchedule[] Groups { get; set; }
        public DateTime[] MatchDays { get; set; }
    }
}