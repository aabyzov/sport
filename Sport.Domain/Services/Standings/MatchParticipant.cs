using Sport.Domain.Models;

namespace Sport.Domain.Services.Standings
{
    public class MatchParticipant
    {
        public FootballParticipantScore Score { get; set; }

        public GroupTeam Team { get; set; }
        string Name { get; set; }
        FactCollection Facts { get; set; }
        bool IsHome { get; set; }
        FootballEvent FootballEvent { get; set; }
    }
}