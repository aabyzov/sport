using Sport.Domain.Models;

namespace Sport.Domain.Services.Standings
{
    public interface IStandingsManager
    {
        Standings BuildStandings(Group group, ChampTemplate champTemplate);
    }
}