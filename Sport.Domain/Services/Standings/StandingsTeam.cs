using Sport.Domain.Models;

namespace Sport.Domain.Services.Standings
{
    public class StandingsTeam
    {
        public StandingsTeam(GroupTeam team)
        {
            Team = team;
        }
        public override string ToString()
        {
            return Team.Team.Name;
        }

        public int Place { get; set; }
        public GroupTeam Team { get; private set; }
        public int Points { get; set; }
        public int Wins { get; set; }
        public int Draws { get; set; }
        public int Loses { get; set; }
        public int Forward { get; set; }
        public int Against { get; set; }
        public int PartialPoints { get; set; }
        public int PartialAwayGoals { get; set; }
        public int PartialWins { get; set; }
        public int PartialDraws { get; set; }
        public int PartialLoses { get; set; }
        public int PartialForward { get; set; }
        public int PartialAgainst { get; set; }
        public bool IsMatchesBetweenPlayed { get; set; }

    }
}