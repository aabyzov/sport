using System.Collections.Generic;
using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Services.Standings
{
    public class FootballEventCollection : List<FootballEvent>
    {
        public FootballEventCollection(IEnumerable<FootballEvent> footballEvents)
        {
            AddRange(footballEvents);
        }

        public FootballEventCollection()
        {

        }

        public IEnumerable<FootballEvent> EventsBetweenTeams(List<GroupTeam> groupTeams)
        {
            return this.Where(x => groupTeams.Contains(x.FixtureParticipant.GroupTeam) && groupTeams.Contains(x.FixtureParticipant.GroupTeam));
        }
    }
}