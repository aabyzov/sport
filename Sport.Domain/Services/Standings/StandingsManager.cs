﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Services.Standings
{

   
    public class StandingsManager : IStandingsManager
    {
        public Standings BuildStandings(Group group, ChampTemplate champTemplate)
        {
            var playedEvents = group.Fixtures.Where(x => x.IsPlayed).ToList();

            var teams = new StandingsTeamCollection();
            //var champTemplate = group.Stage.Champ.ChampTemplate;
            foreach (var footballEvent in playedEvents)
            {
                

                //var groupTeams = new GroupTeamCollection();
                var groupTeams = new List<GroupTeam>();
                groupTeams.AddRange(teams.Select(team => team.Team));
                var homeTeam = footballEvent.Home.GroupTeam;//.Team;
                var awayTeam = footballEvent.Away.GroupTeam;//.Team;
                if (!groupTeams.Contains(homeTeam))
                    teams.Add(new StandingsTeam(homeTeam));
                if (!groupTeams.Contains(awayTeam))
                    teams.Add(new StandingsTeam(awayTeam));

                MakeStandings(teams, footballEvent, champTemplate);
            }

            var standingTeams = new StandingsTeamCollection();
            var playedEventsCollection = new List<Fixture>(playedEvents);
            standingTeams.AddRange(CalculatePlaces(teams, playedEventsCollection, champTemplate));

            Standings standings = new Standings(standingTeams);
            
            standings.Group = group;
            //group.Standings = standings;
            return standings;
        }

        public IList<StandingsTeam> CalculatePlaces(StandingsTeamCollection standingTeams, List<Fixture> events, ChampTemplate champTemplate)
        {
            var result = new StandingsTeamCollection();
            var groupedByPoints = standingTeams.OrderByDescending(x => x.Points).GroupBy(x => x.Points);

            foreach (var groupedByPointsItem in groupedByPoints)
            {
                if (groupedByPointsItem.Count() > 1)
                {
                    var teamCount = groupedByPointsItem.Count();
                    var groupTeams = groupedByPointsItem.Select(x => x.Team).ToList();
                    //var partialEvents = this.EventsBetweenTeams(new List<Fixture>(groupTeams));
        //            public IEnumerable<FootballEvent> EventsBetweenTeams(List<GroupTeam> groupTeams)
        //{
        //    return this.Where(x => groupTeams.Contains(x.FixtureParticipant.GroupTeam) && groupTeams.Contains(x.FixtureParticipant.GroupTeam));
        //}
                    Func<List<Fixture>, List<GroupTeam>, List<Fixture>> fun = (fixtures, groupTeamsInt) => fixtures.Where(x => groupTeams.Contains(x.Home.GroupTeam) && groupTeams.Contains(x.Away.GroupTeam)).ToList();

                    var partialEvents = fun(events, new List<GroupTeam>(groupTeams));

                    var isMatchesBetweenPlayed = (CalcEventsCount(teamCount) == partialEvents.Count());
                    Array.ForEach(groupedByPointsItem.ToArray(), x => x.IsMatchesBetweenPlayed = isMatchesBetweenPlayed);
                    if (!isMatchesBetweenPlayed)
                    {
                        Console.WriteLine("Not all matches played where teams have equal points");
                    }

                    MakePartialStandings(standingTeams, new List<Fixture>(partialEvents), champTemplate);
                    var func = champTemplate.StandingsOrdering;
                    var afterSort = func(groupedByPointsItem);
                    result.AddRange(afterSort);
                }
                else
                {
                    result.Add(groupedByPointsItem.FirstOrDefault());
                }
            }

            return result;
        }


        private int CalcEventsCount(int teamCount)
        {
            var result = 0;
            for (var i = teamCount - 1; i > 0; i--)
            {
                result += i;
            }
            return result;
        }

        public void MakePartialStandings(StandingsTeamCollection teams, List<Fixture> events, ChampTemplate champTemplate)
        {
            foreach (var @event in events)
            {
                var homeTeam = teams.FirstOrDefault(x => x.Team.Id == @event.Home.GroupTeamId);
                var awayTeam = teams.FirstOrDefault(x => x.Team.Id == @event.Away.GroupTeamId);
                var homeScore = @event.Home.Score;
                var awayScore = @event.Away.Score;
                var pointsForWin = champTemplate.PointsForWin;
                var pointsForDraw = champTemplate.PointsForDraw;
                if (homeScore.Value > awayScore.Value)
                {
                    homeTeam.PartialPoints += pointsForWin;
                    homeTeam.PartialWins += 1;
                    awayTeam.PartialLoses += 1;

                }
                if (homeScore.Value == awayScore.Value)
                {
                    homeTeam.PartialPoints += pointsForDraw;
                    awayTeam.PartialPoints += pointsForDraw;
                    homeTeam.PartialDraws += 1;
                    awayTeam.PartialDraws += 1;
                }
                if (homeScore.Value < awayScore.Value)
                {
                    awayTeam.PartialPoints += pointsForWin;
                    awayTeam.PartialWins += 1;
                    homeTeam.PartialLoses += 1;
                }
                awayTeam.PartialAwayGoals += awayScore.Value;
                homeTeam.PartialForward += homeScore.Value;
                homeTeam.PartialAgainst += awayScore.Value;
                awayTeam.PartialForward += awayScore.Value;
                awayTeam.PartialAgainst += homeScore.Value;
            }
        }

        public void MakeStandings(StandingsTeamCollection teams, Fixture @event, ChampTemplate champTemplate)
        {
            var homeTeam = teams.FirstOrDefault(x => x.Team.Id == @event.Home.GroupTeam.Id);
            var awayTeam = teams.FirstOrDefault(x => x.Team.Id == @event.Away.GroupTeam.Id);
            var homeScore = @event.Home.Score;
            var awayScore = @event.Away.Score;
            var pointsForWin = champTemplate.PointsForWin;
            var pointsForDraw = champTemplate.PointsForDraw;
            if (homeScore.Value > awayScore.Value)
            {
                homeTeam.Points += pointsForWin;
                homeTeam.Wins += 1;
                awayTeam.Loses += 1;

            }
            if (homeScore.Value == awayScore.Value)
            {
                homeTeam.Points += pointsForDraw;
                awayTeam.Points += pointsForDraw;
                homeTeam.Draws += 1;
                awayTeam.Draws += 1;
            }
            if (homeScore.Value < awayScore.Value)
            {
                awayTeam.Points += pointsForWin;
                awayTeam.Wins += 1;
                homeTeam.Loses += 1;
            }
            homeTeam.Forward += homeScore.Value;
            homeTeam.Against += awayScore.Value;
            awayTeam.Forward += awayScore.Value;
            awayTeam.Against += homeScore.Value;

        }

    }
}