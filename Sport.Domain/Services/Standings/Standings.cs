using Sport.Domain.Models;

namespace Sport.Domain.Services.Standings
{
    public class Standings
    {
        public Standings(StandingsTeamCollection standingsTeamCollection)
        {
            Teams = standingsTeamCollection;
        }
        public StandingsTeamCollection Teams { get; private set; }
        public Group Group { get; set; }

        public override string ToString()
        {
            return "Standings in " + Group;
        }
    }
}