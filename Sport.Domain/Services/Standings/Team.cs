using System;

namespace Sport.Domain.Services.Standings
{
    public class Team
    {
        string FullName { get; set; }
        bool IsInternational { get; set; }
        //ICountry Country { get; set; }
        ////ITeamPlayerLinkCollection TeamPlayerLinks { get; set; }
        //IPlayerCollection Players { get; }
        //ILeague League { get; set; }
        Guid LeagueId { get; set; }
    }
}