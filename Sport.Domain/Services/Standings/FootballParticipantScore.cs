namespace Sport.Domain.Services.Standings
{
    public class FootballParticipantScore
    {
        public int Goals { get; set; }
        int? GoalsInInjuryTime { get; set; }
        int? GoalsOnPenalty { get; set; }
        int? GoalsInFulltime { get; set; }
    }
}