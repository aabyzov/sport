namespace Sport.Domain.Services.Standings
{
    public class IFootballEventFact
    {
        int Minute { get; set; }
        //IPlayer Player { get; set; }
        int EventOrder { get; set; }
        int EventHalfTime { get; set; }
        //FixtureEventsType Type { get; set; }
        MatchParticipant MatchParticipant { get; set; }
        //string ToConsole();
    }
}