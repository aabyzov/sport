using System;
using System.Linq;

namespace Sport.Domain.Services.Standings
{
    public abstract class ChampTemplate
    {
        //IStageCollection StageCollection { get; set; }
        //ICountry Country { get; set; }
        string Name { get; set; }
        bool IsInternational { get; set; }
        public abstract int PointsForWin { get; }
        public abstract int PointsForDraw { get; }
        public abstract Func<IGrouping<int, StandingsTeam>, IOrderedEnumerable<StandingsTeam>> StandingsOrdering { get;  }
        //IChampCollection Champs { get; set; }
        //IEntities.Enums.ChampTemplates ChampType { get; set; }
    }
}