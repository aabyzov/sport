﻿namespace Sport.Domain.Services
{
    public abstract class ScheduleTable
    {
        public abstract StageSchedule[] GetStages(int year);
    }
}