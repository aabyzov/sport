using System.Collections.Generic;
using Sport.Domain.Models;

namespace Sport.Domain.Services
{
    public interface IScheduleManager
    {
        ScheduleTable GetScheduleTable(Zone zone, ChampType champType);
        int[,] GenerateTable(ICollection<GroupTeam> groupTeams);
    }
}