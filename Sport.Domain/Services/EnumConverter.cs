﻿using System;
using System.Runtime.Remoting.Messaging;
using Sport.Domain.Models;

namespace Sport.Domain.Services
{
    public class EnumConverter
    {
        public static OutOfFixtureReasons Convert(SuspensionReason suspensionReason)
        {
            switch (suspensionReason)
            {
                    case SuspensionReason.Injury:
                        return OutOfFixtureReasons.Injury;
                    case SuspensionReason.RedCard:
                        return OutOfFixtureReasons.RedCard;
                    case SuspensionReason.YellowCard:
                        return OutOfFixtureReasons.YellowCard;
                    
            }
            throw new ArgumentException("Can't convert to OutOfFixtureReasons");
        }
    }
}