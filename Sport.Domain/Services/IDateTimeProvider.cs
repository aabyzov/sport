﻿using System;
using Sport.Domain.Models;

namespace Sport.Domain.Services
{
    public interface IDateTimeProvider
    {
        DateTime CurrentMatchDate { get; }
        Fixture CurrentMatch { get; }
    }
}