﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sport.Common.Core;
using Sport.Domain.Services.Standings;

namespace Sport.Domain.Services.Templates
{
    public class WorldCupTemplate : ChampTemplate
    {
        //private IObjectFactory Factory;
        //public WorldCupTemplate(IObjectFactory factory)
        //{
        //    Factory = factory;
        //    Init();
        //    ChampType = IEntities.Enums.ChampTemplates.WorldCup;

        //}



        //public override IEntities.Enums.ChampTemplates ChampType { get; set; }

        //public void Init()
        //{
        //    var stages = new List<IStage>();
        //    IStage groupStage = Factory.Stage(Constants.Constants.General.GroupStage, 8, 1, 32, 2, StageType.Group);
        //    IStage oneEighthStage = Factory.Stage(Name = Constants.Constants.General.EighthFinal, 8, 1, 16, 1, StageType.Playoff);
        //    IStage oneForthStage = Factory.Stage(Constants.Constants.General.QuarterFinal, 4, 1, 8, 1, StageType.Playoff);
        //    IStage semiStage = Factory.Stage(Name = Constants.Constants.General.SemiFinal, 2, 1, 4, 1, StageType.Playoff);
        //    IStage final = Factory.Stage(Name = Constants.Constants.General.Final, 1, 1, 2, 1, StageType.Playoff);
        //    stages.Add(groupStage);
        //    stages.Add(oneEighthStage);
        //    stages.Add(oneForthStage);
        //    stages.Add(semiStage);
        //    stages.Add(final);
        //    StageCollection = new StageCollection(stages);
        //}


        //public override IChampCollection Champs { get; set; }

        public override int PointsForWin
        {
            get { return 3; }
        }

        public override int PointsForDraw
        {
            get { return 1; }
        }

        public override Func<IGrouping<int, StandingsTeam>, IOrderedEnumerable<StandingsTeam>> StandingsOrdering
        {
            get
            {
                return
                    item =>
                    item.OrderByDescending(x => x.IsMatchesBetweenPlayed ? x.PartialPoints : 0).ThenByDescending(x => x.IsMatchesBetweenPlayed ? x.PartialForward - x.PartialAgainst : 0).ThenByDescending(
                        x => x.IsMatchesBetweenPlayed ? x.PartialAwayGoals : 0).ThenByDescending(x => x.Forward - x.Against).ThenByDescending(x => x.Forward).ThenByDescending(x => x.Team.Team.Name);
            }
        }
    }
}