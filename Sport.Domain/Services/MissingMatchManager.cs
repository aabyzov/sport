﻿#region

using System;
using System.Collections.Generic;
using System.Linq;
using Sport.Domain.Models;
using Sport.Domain.UoW;

#endregion

namespace Sport.Domain.Services
{
    public class MissingMatchManager : IMissingMatchManager
    {
        private readonly ISportUnitOfWork _sportUnitOfWork;

        public MissingMatchManager(ISportUnitOfWork sportUnitOfWork)
        {
            if (sportUnitOfWork == null) throw new ArgumentNullException("sportUnitOfWork");
            _sportUnitOfWork = sportUnitOfWork;
        }

        public OutOfFixturePlayer[] BeforeMatch(Fixture fixture)
        {
            if (!fixture.HasOutOfFixtureProcessed)
                DoBeforeMatch(fixture);
            return fixture.FixtureParticipants.SelectMany(x => x.OutOfFixturePlayers).ToArray();
        }

        public void AfterMatch(Fixture fixture)
        {
            UpdateInjuryRedCardSuspension(fixture);
            UpdateYellowCardSuspension(fixture);
        }

        private void DoBeforeMatch(Fixture fixture)
        {
            var players = GetFixturePlayers(fixture);
            foreach (var player in players)
            {
                var activeSuspensions = _sportUnitOfWork.Suspensions.FindAllActiveBy(fixture, player).ToList();
                activeSuspensions.ForEach(UpdateSuspensionAndOutOfFixture(fixture, player));
            }
            fixture.HasOutOfFixtureProcessed = true;
            _sportUnitOfWork.Fixtures.Save(fixture);
            _sportUnitOfWork.Save();
        }

        private void UpdateInjuryRedCardSuspension(Fixture fixture)
        {
            var fixtureEvents =
                _sportUnitOfWork.FixtureEvents.FootballEventsShort()
                    .Where(fe => fe.FixtureParticipant.FixtureId == fixture.Id);
            foreach (var @event in fixtureEvents.Where(x => x is FootballRedCard || x is FootballInjury))
            {
                var footballEvent = @event;
                if (!_sportUnitOfWork.Suspensions.Any(x => x.FixtureEventId == footballEvent.Id))
                {
                    if (@event is FootballInjury && ((@event as FootballInjury).MissedMatchCountInjury > 0))
                    {
                        var injury = (@event as FootballInjury);
                        var injurySuspension = new Suspension()
                        {
                            Start = fixture.Date,
                            End = injury.RecoveryDate,
                            FixtureEvent = @event,
                            MissedMatchCount = injury.MissedMatchCountInjury ?? 0,
                            Reason = SuspensionReason.Injury
                        };
                        _sportUnitOfWork.Suspensions.Save(injurySuspension);
                    }
                    if (@event is FootballRedCard && ((@event as FootballRedCard).MissedMatchCountRedCard > 0))
                    {
                        var redCard = (@event as FootballRedCard);
                        var redCardSuspension = new Suspension()
                        {
                            Start = fixture.Date,
                            FixtureEvent = @event,
                            MissedMatchCount = redCard.MissedMatchCountRedCard,
                            Reason = SuspensionReason.RedCard
                        };
                        _sportUnitOfWork.Suspensions.Save(redCardSuspension);
                    }
                }
            }
            _sportUnitOfWork.Save();
        }

        private void UpdateYellowCardSuspension(Fixture fixture)
        {
            var players = GetFixturePlayers(fixture);

            UpdateSuspensionOfYellowCardsForPlayers(players, fixture);
        }

        private Action<Suspension> UpdateSuspensionAndOutOfFixture(Fixture fixture, Player player)
        {
            return activeSuspension =>
            {
                var outOfFixtureReason = EnumConverter.Convert(activeSuspension.Reason);
                var outOfFixturePlayer = new OutOfFixturePlayer()
                {
                    MissedMatchCount = activeSuspension.MissedMatchCount,
                    Player = player,
                    Reason = outOfFixtureReason
                };

                // Does player belong to home or away team?
                var belongsToHome = fixture.Home.Players.Any(x => x.Id == player.Id);
                if (belongsToHome)
                {
                    if (fixture.Home.OutOfFixturePlayers.All(x => x.PlayerId != outOfFixturePlayer.PlayerId))
                        fixture.Home.OutOfFixturePlayers.Add(outOfFixturePlayer);
                }
                else
                {
                    if (fixture.Away.OutOfFixturePlayers.All(x => x.PlayerId != outOfFixturePlayer.PlayerId))
                        fixture.Away.OutOfFixturePlayers.Add(outOfFixturePlayer);
                }

                _sportUnitOfWork.OutOfFixturePlayers.Save(outOfFixturePlayer);

                activeSuspension.MissedMatchCount--;
                var suspendedFixture = new SuspendedFixture()
                {
                    Fixture = fixture,
                    Suspension = activeSuspension
                };

                _sportUnitOfWork.SuspendedFixtures.Save(suspendedFixture);
            };
        }

        private IEnumerable<Player> GetFixturePlayers(Fixture fixture)
        {
            return _sportUnitOfWork.Players.FindAllByFixtureWithAnyYellowCard(fixture).ToList();
        }

        private void UpdateSuspensionOfYellowCardsForPlayers(IEnumerable<Player> players, Fixture fixture)
        {
            foreach (var player in players)
            {
                var thePlayer = player;

                var yellowCardsInOfficialMatches =
                    thePlayer.FootballYellowCardsInOfficialMatchesBeforeFixture(null).ToArray();

                var groupedYellowCards = from y in yellowCardsInOfficialMatches
                    group y by y.FixtureParticipant.GroupTeam.Group.Stage.Champ
                    into g
                    select new {Champ = g.Key, YellowCards = g};

                foreach (var groupedYellowCard in groupedYellowCards)
                {
                    var playerYellowCardNumberInChamp = 0;
                    var currentChamp = groupedYellowCard.Champ;
                    foreach (var yellowCard in groupedYellowCard.YellowCards)
                    {
                        var card = yellowCard;
                        playerYellowCardNumberInChamp++;
                        if (playerYellowCardNumberInChamp != currentChamp.YellowsToMissNextMatch) continue;
                        if (_sportUnitOfWork.Suspensions.Any(x => x.FixtureEventId == card.Id)) continue;
                        var suspension = new Suspension()
                        {
                            Reason = SuspensionReason.YellowCard,
                            FixtureEvent = yellowCard,
                            MissedMatchCount = 1,
                        };
                        _sportUnitOfWork.Suspensions.Save(suspension);
                    }
                }
            }
            _sportUnitOfWork.Save();
        }
    }
}