using System;

namespace Sport.Domain.Services
{
    public class FixtureSchedule
    {
        public DateTime Date { get; set; }
    }
}