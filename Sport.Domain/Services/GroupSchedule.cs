﻿using System;

namespace Sport.Domain.Services
{
    public class GroupSchedule
    {
        public FixtureSchedule[] Fixtures { get; set; }
        public DateTime[] MatchDays { get; set; }
    }
}