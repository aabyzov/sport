﻿using System;
using System.Collections.Generic;
using Sport.Common.Core;
using Sport.Domain.Models;
using Sport.Domain.Services.ScheduleTables;

namespace Sport.Domain.Services
{
    public class ScheduleManager : IScheduleManager
    {
        public Func<Zone, ChampType, ScheduleTable> Selector;
        public ScheduleManager()
        {

            Selector = (zone, champType) =>
            {
                //TODO: should be refactored
                if (champType.Name.Contains(Constants.WorldCup))
                {
                    if (zone.Name == Constants.Zones.Africa)
                        return new AfricanCupWcScheduleTable();
                    if (zone.Name == Constants.Zones.SouthAmerica)
                        return new SouthAmericanWcScheduleTable();
                    if (zone.Name == Constants.Zones.NorthAmerica)
                        return new NorthAmericanWcScheduleTable();
                    if (zone.Name == Constants.Zones.Europe)
                        return new EuropeanWcScheduleTable();
                    if (zone.Name == Constants.Zones.Friendly)
                        return new FriendliesWcScheduleTable();
                    if (zone.Name == Constants.Zones.Host)
                        return new FinalsWcScheduleTable();
                }
                return null;
            };
        }

        public ScheduleTable GetScheduleTable(Zone zone, ChampType champType)
        {
            return Selector(zone, champType);
        }

        public int[,] GenerateTable(ICollection<GroupTeam> groupTeams)
        {
            var teamCount = groupTeams.Count;
            int[,] fixtureTable;
            if (teamCount % 2 == 0)
            {
                fixtureTable = GenerateTableWithEvenTeamCount(teamCount);
            }
            else
            {
                fixtureTable = GenerateTableWithOddTeamCount(teamCount);
            }
            return fixtureTable;
        }

        protected static int[,] GenerateTableWithEvenTeamCount(int teamCount)
        {
            const int nullValue = -1;
            var table = new int[teamCount, teamCount];
            for (var capacity = 0; capacity < teamCount; capacity++)
                table[capacity, capacity] = nullValue;

            for (int row = 0; row < teamCount - 1; row++)
            {
                int tour = row;
                for (int column = 0; column < teamCount; column++)
                {
                    if (column == teamCount - 1)
                    {
                        var takeNotAssignedTour = TakeNotAssignedTour(table, row, teamCount);
                        table[row, column] = takeNotAssignedTour;
                        table[column, row] = takeNotAssignedTour;
                    }
                    else
                    {
                        if (table[row, column] != nullValue)
                        {
                            table[row, column] = tour;
                            //table[column, row] = tour;
                        }
                    }
                    tour++;
                    if (tour == teamCount)
                        tour = 1;
                }

            }
            return table;
        }

        private static int TakeNotAssignedTour(int[,] table, int row, int teamCount)
        {
            for (int tour = 1; tour <= teamCount; tour++)
            {
                bool isFound = false;
                int column = 0;
                while (!isFound && column < teamCount)
                {
                    if (table[row, column] == tour)
                    {
                        isFound = true;
                    }
                    column++;
                }
                if (!isFound)
                    return tour;
            }
            return 0;
        }

        protected static int[,] GenerateTableWithOddTeamCount(int teamCount)
        {
            var table = new int[teamCount, teamCount];


            for (int row = 0; row < teamCount; row++)
            {
                int tour = row + 1;
                for (int column = 0; column < teamCount; column++)
                {
                    table[row, column] = tour;
                    if (tour == teamCount)
                        tour = 1;
                    else
                    {
                        tour++;
                    }
                }
            }
            return table;
        }
    }
}
