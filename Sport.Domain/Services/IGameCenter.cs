﻿#region

using System.Collections;
using System.Collections.Generic;
using Sport.Domain.Models;

#endregion

namespace Sport.Domain.Services
{
    public interface IGameCenter
    {
        void SaveResult(Fixture fixture, int homeScore, int awayScore);

        IEnumerable<Fixture> GetResults();
    }
}