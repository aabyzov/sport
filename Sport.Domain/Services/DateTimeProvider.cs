﻿using System;
using System.Linq;
using Sport.Domain.Models;
using Sport.Domain.UoW;

namespace Sport.Domain.Services
{
    public class DateTimeProvider : IDateTimeProvider
    {
        private readonly ISportUnitOfWork _sportUnitOfWork;

        public DateTimeProvider(ISportUnitOfWork sportUnitOfWork)
        {
            _sportUnitOfWork = sportUnitOfWork;
        }

        public DateTime CurrentMatchDate
        {
            get
            {
                var firstUplayedMatch = CurrentMatch;
                if (firstUplayedMatch == null) return DateTime.Now;
                return (firstUplayedMatch.Date) ?? DateTime.Now;
            }
        }

        public Fixture CurrentMatch
        {
            get
            {
                var firstUplayedMatch = _sportUnitOfWork.Fixtures.FindAllBy(x => !x.IsPlayed).OrderBy(x => x.Date).FirstOrDefault();
                if (firstUplayedMatch != null)
                    return firstUplayedMatch;
                // If there is no matches to play return null
                return null;
            }
        }
    }
}