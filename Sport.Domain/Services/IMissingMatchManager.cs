﻿#region

using Sport.Domain.Models;

#endregion

namespace Sport.Domain.Services
{
    public interface IMissingMatchManager
    {
        OutOfFixturePlayer[] BeforeMatch(Fixture fixture);

        void AfterMatch(Fixture fixture);
    }
}