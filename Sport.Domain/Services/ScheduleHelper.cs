using System;

namespace Sport.Domain.Services
{
    public class ScheduleHelper
    {
        public static DateTime GetNthDayOfMonthYear(int n, DayOfWeek day, int month, int year)
        {
            if (n > 5 || n < 1 || month < 1 || month > 12 || year == 0) throw new InvalidOperationException();
            var currentN = 1;
            for (var i = 1; i < DateTime.DaysInMonth(year, month); i++)
            {
                if (n == currentN && new DateTime(year, month, i).DayOfWeek == day)
                    return new DateTime(year, month, i);
                if (new DateTime(year, month, i).DayOfWeek == day)
                    currentN++;
            }
            throw new Exception("There is no such day on the specified condition");
        }
    }
}