﻿#region

using System;
using System.Collections.Generic;
using Sport.Domain.Models;
using Sport.Domain.UoW;

#endregion

namespace Sport.Domain.Services
{
    public class GameCenter : IGameCenter
    {
        private readonly IMissingMatchManager _missingMatchManager;
        private readonly ISportUnitOfWork _sportUnitOfWork;

        public GameCenter(ISportUnitOfWork sportUnitOfWork, IMissingMatchManager missingMatchManager)
        {
            _sportUnitOfWork = sportUnitOfWork;
            if (missingMatchManager == null) throw new ArgumentNullException("missingMatchManager");
            _missingMatchManager = missingMatchManager;
        }

        public void SaveResult(Fixture fixture, int homeScore, int awayScore)
        {
            fixture.Home.Score = homeScore;
            fixture.Away.Score = awayScore;
            fixture.IsPlayed = true;
            _missingMatchManager.BeforeMatch(fixture);
            SaveFixtureEvents(fixture);
            _missingMatchManager.AfterMatch(fixture);
        }

        public IEnumerable<Fixture> GetResults()
        {
            throw new NotImplementedException();
        }

        private void SaveFixtureEvents(Fixture fixture)
        {
            foreach (var footballEvent in fixture.FootballEvents)
            {
                _sportUnitOfWork.FixtureEvents.Save(footballEvent);
            }
            _sportUnitOfWork.Save();
        }
    }
}