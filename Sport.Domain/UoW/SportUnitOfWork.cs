﻿using System;
using System.Linq;
using System.Transactions;
using Sport.Common.Core;
using Sport.Domain.Models;
using Sport.Domain.Repositories;

namespace Sport.Domain.UoW
{
    public class SportUnitOfWork : ISportUnitOfWork
    {
        private readonly SportContext _db;
        private readonly ILogger _logger;
        public SportUnitOfWork(SportContext context, ILogger logger, ISportKindRepository sportKindRepository, IChampTypeRepository champTypeRepository, IChampSeasonRepository champSeasonRepository, 
            IChampRepository champRepository, IStageRepository stageRepository, IGroupRepository groupRepository, IGroupTeamRepository groupTeamRepository,
            IZoneRepository zoneRepository, ITeamRepository teamRepository, ITeamMemberRepository teamMemberRepository,IPlayerRepository playerRepository, 
            ILeagueRepository leagueRepository, IFixtureRepository fixtureRepository, IFixtureParticipantRepository fixtureParticipantRepository,
            IFixtureEventRepository fixtureEventRepository, IOutOfFixturePlayerRepository outOfFixturePlayerRepository, ISuspensionRepository suspensionRepository, ISuspendedFixtureRepository suspendedFixtureRepository
            )
        {
            Suspensions = suspensionRepository;
            SuspendedFixtures = suspendedFixtureRepository;
            _db = context;
            _logger = logger;
            SportKinds = sportKindRepository;
            ChampTypes = champTypeRepository;
            ChampSeasons = champSeasonRepository;
            Champs = champRepository;
            Stages = stageRepository;
            Groups = groupRepository;
            GroupTeams = groupTeamRepository;
            Zones = zoneRepository;
            Teams = teamRepository;
            TeamMembers = teamMemberRepository;
            Players = playerRepository;
            Leagues = leagueRepository;
            Fixtures = fixtureRepository;
            FixtureParticipants = fixtureParticipantRepository;
            FixtureEvents = fixtureEventRepository;
            OutOfFixturePlayers = outOfFixturePlayerRepository;
        }

        #region Public methods

        public void Execute(params System.Action<ISportUnitOfWork>[] commands)
        {
            if (commands == null || !commands.Any())
            {
                return;
            }
#if DEBUG
            var ts = TimeSpan.Zero;
#else
            var ts = TimeSpan.FromMinutes(5);
#endif
            var scope = new TransactionScope(TransactionScopeOption.Required, ts);
            try
            {
                foreach (var command in commands)
                {
                    command.Invoke(this);
                }
                scope.Complete();
            }
            catch (Exception e)
            {
                _logger.LogError(e);
                throw;
            }
            finally
            {
                scope.Dispose();
            }
        }

        public void Save()
        {
            if (Transaction.Current == null)
            {
                Execute(iow => SaveInternal());
            }
            else
            {
                SaveInternal();
            }
        }

        private void SaveInternal()
        {
            var previousValue = _db.UseAudit;
            try
            {
                //_db.UseAudit = UseAudit;
                _db.SaveChanges();
            }
            finally
            {
                //_db.UseAudit = previousValue;
            }
        }

        public ISportKindRepository SportKinds { get; private set; }
        public IChampTypeRepository ChampTypes { get; private set; }
        public IChampSeasonRepository ChampSeasons { get; private set; }
        public IChampRepository Champs { get; private set; }
        public IStageRepository Stages { get; private set; }
        public IGroupRepository Groups { get; private set; }
        public IGroupTeamRepository GroupTeams { get; private set; }
        public ILeagueRepository Leagues { get; private set; }
        public ITeamRepository Teams { get; private set; }
        public ITeamMemberRepository TeamMembers { get; private set; }
        public IPlayerRepository Players { get; private set; }
        public IZoneRepository Zones { get; private set; }
        public IFixtureRepository Fixtures { get; private set; }
        public IFixtureParticipantRepository FixtureParticipants { get; private set; }
        public IFixtureEventRepository FixtureEvents { get; private set; }
        public IOutOfFixturePlayerRepository OutOfFixturePlayers { get; private set; }
        public ISuspensionRepository Suspensions { get; private set; }
        public ISuspendedFixtureRepository SuspendedFixtures { get; private set; }

        #endregion

    }
}