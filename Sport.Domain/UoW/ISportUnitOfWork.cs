﻿#region

using System;
using Sport.Common.Core;
using Sport.Domain.Repositories;

#endregion

namespace Sport.Domain.UoW
{
    public interface ISportUnitOfWork : IUnitOfWork
    {
        ISportKindRepository SportKinds { get; }
        IChampTypeRepository ChampTypes { get; }
        IChampSeasonRepository ChampSeasons { get; }
        IChampRepository Champs { get; }
        IStageRepository Stages { get; }
        IGroupRepository Groups { get; }
        IGroupTeamRepository GroupTeams { get; }
        ILeagueRepository Leagues { get; }
        ITeamRepository Teams { get; }
        ITeamMemberRepository TeamMembers { get; }
        IPlayerRepository Players { get; }
        IZoneRepository Zones { get; }
        IFixtureRepository Fixtures { get; }
        IFixtureParticipantRepository FixtureParticipants { get; }
        IFixtureEventRepository FixtureEvents { get; }
        IOutOfFixturePlayerRepository OutOfFixturePlayers { get; }
        ISuspensionRepository Suspensions { get; }
        ISuspendedFixtureRepository SuspendedFixtures { get; }
        void Execute(params Action<ISportUnitOfWork>[] commands);
    }
}