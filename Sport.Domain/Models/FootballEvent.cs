﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sport.Common.Core;

namespace Sport.Domain.Models
{
    public abstract class FootballEvent : FixtureEvent
    {
        public bool IsGoal
        {
            get { return this is FootballGoal; }
        }

        public bool IsOwnGoal
        {
            get { return this is FootballOwnGoal; }
        }

        public bool IsYellowCard
        {
            get { return this is FootballYellowCard; }
        }

        public bool IsRedCard
        {
            get { return this is FootballRedCard; }
        }

        public bool IsInjury
        {
            get { return this is FootballInjury; }
        }

        public bool IsMissedPenalty
        {
            get { return this is FootballMissedPenalty; }
        }

        
    }
}