﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Sport.Common.Extensions;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    [MetadataType(typeof(UserMetaData))]
    public class User : IHasPrimaryKey, IHasTimestamp
    {
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Abbrev { get; set; }
        public bool Enabled { get; set; }
        public string Pwd { get; set; }
        public Guid? PwdToken { get; set; }
        public DateTime? PwdEmailDate { get; set; }
        public int? version { get; set; }

        /// <summary>
        /// Determines the date when the user was deleted, NULL if it's not
        /// </summary>
        public DateTime? Deleted { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        //[NotMapped]
        //public string EnabledText
        //{
        //    get
        //    {
        //        return Enabled ? "1" : "0";
        //    }

        //    set
        //    {
        //        Enabled = value == "1";
        //    }
        //}

        /// <summary>
        /// If the user comes from Active Directory.
        /// </summary>
        public bool IsActiveDirectoryUser { get; set; }

        /// <summary>
        /// Checks if UserName matches the provided value.
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public bool Is(string UserName)
        {
            return this.UserName.Equals(UserName, StringComparison.InvariantCultureIgnoreCase);
        }

        static User agop;

        //public static User AgibooOperator
        //{
        //    get
        //    {
        //        if (agop == null)
        //        {
        //            agop = new User
        //            {
        //                Id = -1,
        //                UserName = "Agiboo.Operator",
        //                FullName = "Agiboo Operator",
        //                Email = Config.AgibooOperatorEmail,
        //                Enabled = true,
        //                Abbrev = "AgOp",
        //            };
        //        }

        //        return agop;
        //    }
        //}

        /// <summary>
        /// Meta data class for add/edit user form
        /// </summary>
        public class UserMetaData
        {
            [Required, DisplayName("Full name")]
            public string FullName { get; set; }

            [Required, DisplayName("Username")]
            public string UserName { get; set; }

            [Required, RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                         @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                         @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Invalid email")]
            public string Email { get; set; }

            [DisplayName("Is enabled?")]
            public bool? Enabled { get; set; }

            [MaxLength(10)]
            public string Abbrev { get; set; }
        }

        public Guid Id { get; set; }
    }
}