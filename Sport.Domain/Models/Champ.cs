﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Sport.Common.Core;
using Sport.Common.Extensions;
using System.ComponentModel.DataAnnotations;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class Champ : Persistent, IHasMigrationId
    {
        public string Name { get; set; }

        //public Guid? ChampParentId { get; set; }

        //[Column("ChampSeason_Id")]
        public Guid? ChampSeasonId { get; set; }
        public virtual ChampSeason ChampSeason { get; set; }

        public Guid MigrationId { get; set; }

        public Guid? ZoneId { get; set; }
        public virtual Zone Zone { get; set; }

        public virtual ICollection<Stage> Stages { get; set; }

        [NotMapped]
        public Stage[] StagesOrdered
        {
            get
            {
                return Stages.OrderBy(x => x.StartDate).ToArray();
            }
        }

        [NotMapped]
        public Group[] Groups
        {
            get
            {
                return Stages.SelectMany(x => x.Groups).ToArray();
            }
        }

        [NotMapped]
        public IQueryable<Fixture> Fixtures
        {
            get { return Stages.SelectMany(x => x.Fixtures).OrderBy(x => x.Date).AsQueryable(); }
        }

        [NotMapped]
        public bool IsFriendly
        {
            get { return Name.StartsWith(Constants.Friendly); }
        }

        [NotMapped]
        public IQueryable<Team> Teams
        {
            get { return Stages.SelectMany(s => s.Teams).AsQueryable().Distinct(new TeamComparer()); }
        }

        [NotMapped]
        public DateTime? Start
        {
            get
            {
                var earliestFixture = Fixtures.OrderBy(x => x.Date).FirstOrDefault();
                return earliestFixture != null ? earliestFixture.Date : null;
            }
        }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [NotMapped]
        public DateTime StartDate
        {
            get
            {
                return Start == null ? DateTime.Now : (DateTime)Start;
            }
        }

        [NotMapped]
        public DateTime? End
        {
            get
            {
                var latestFixture = Fixtures.OrderByDescending(x => x.Date).FirstOrDefault();
                return latestFixture != null ? latestFixture.Date : null;
            }
        }

        [NotMapped]
        public DateRange Period
        {
            get
            {
                if (Start != null && End != null)
                    return new DateRange((DateTime)Start, (DateTime)End);
                return null;
            }
        }

        public int YellowsToMissNextMatch { get; set; }

        public override string ToString()
        {
            return Name + " " + Period;
        }
    }
}