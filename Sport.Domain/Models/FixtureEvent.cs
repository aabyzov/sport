﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Sport.Common.Core;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public abstract class FixtureEvent : Persistent, IValidatableObject
    {
        
        public int? EventOrder { get; set; }
        public EventTime EventTime { get; set; }

        public Guid PlayerId { get; set; }
        public virtual Player Player { get; set; }

        public Guid FixtureParticipantId { get; set; }
        public virtual FixtureParticipant FixtureParticipant { get; set; }


        //public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
        //    if (this.FixtureParticipant.OutOfFixturePlayers.Any(x => x.PlayerId == player.Id))
        //        yield return new ValidationResult();
        //}
        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (FixtureParticipant.OutOfFixturePlayers == null) throw new ArgumentNullException("FixtureParticipant.OutOfFixturePlayers");
            if (FixtureParticipant.OutOfFixturePlayers.Any(x => x.PlayerId == PlayerId))
                yield return new ValidationResult(String.Format(Constants.Errors.MissingMatchPlayerInEvents, Player.FullName, this));
        }
    }

    //public class FootballScore : Score
    //{
    //    public FootballScore()
    //    {
    //        Periods = new HashSet<Period>()
    //            {
    //                new FootballPeriod()
    //                    {
    //                        Name = "First Half",
    //                        StartMinute = 1,
    //                        EndMinute = 45
    //                    },
    //                    new FootballPeriod()
    //                        {
    //                            Name = "Second Half",
    //                            StartMinute = 46,
    //                            EndMinute = 90
    //                        },
    //                        new FootballPeriod()
    //                            {
    //                                Name = "Overtime First Half",
    //                                StartMinute = 91,
    //                                EndMinute = 105
    //                            },
    //                            new FootballPeriod()
    //                                {
    //                                    Name = "Overtime Second Half",
    //                                    StartMinute = 106,
    //                                    EndMinute = 120
    //                                }
    //            };
    //    }

    //    public virtual ICollection<Period> Periods { get; set; }
    //}

    //public abstract class Period : Persistent
    //{

    //}

    //public class FootballPeriod : Period
    //{
    //    public string Name { get; set; }
    //    public int StartMinute { get; set; }
    //    public int EndMinute { get; set; }
    //}

    //public abstract class Score : Persistent
    //{

    //}
}