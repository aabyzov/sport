﻿namespace Sport.Domain.Models
{
    public class FootballSquadPlayer : SquadPlayer
    {
        public bool IsSubstitution { get; set; }
    }
}