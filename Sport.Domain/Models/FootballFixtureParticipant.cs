﻿using System;

namespace Sport.Domain.Models
{
    public class FootballFixtureParticipant : FixtureParticipant
    {
        public int? HalfTimeScore { get; set; }
        public int? OverTimeHalfScore { get; set; }
        public int? OverTimeScore { get; set; }
        public int? PenaltyScore { get; set; }
        
    }
}