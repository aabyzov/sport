﻿using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public abstract class EventTime : Persistent
    {
        public int? Second { get; set; }
        //public int EventOrder { get; set; }
    }
}