﻿namespace Sport.Domain.Models.Enums
{
    public enum HowScored
    {
        FromPlay=1,
        FromPenalty=2,
        AfterGoalkeeperSaved=4,
        DirectlyFromFreeKick=3,
        AfterMissedPenalty=5,
        LoftedKick=6,
        BicycleKick=7,
    }
}