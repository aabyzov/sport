﻿namespace Sport.Domain.Models.Enums
{
    public enum MissedPenaltySituations
    {
        MissedLeftBar=0,
        OverTheBar,
        MissedRightBar,
        HitLeftPost,
        HitTheBar,
        HitRightPost,
        GkSavedInTheMiddle,
        GkSavedInTheLeftCorner,
        GkSavedInTheRightCorner,
        MissedLeftCrossBar,
        MissedRightCrossBar,
    }
}