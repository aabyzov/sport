﻿namespace Sport.Domain.Models.Enums
{
    public enum StageType
    {
        Group,
        Playoff
    }
}