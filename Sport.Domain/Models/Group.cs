﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using Sport.Common.Extensions;
using Sport.Common.Model;
using Sport.Domain.Services;
using Sport.Domain.Services.Standings;

namespace Sport.Domain.Models
{
    [DebuggerDisplay("{Stage.Champ.ToString()}")]
    public class Group : Persistent, IHasMigrationId
    {
        public Group()
        {
            GroupTeams = new Collection<GroupTeam>();
        }

        public string Name { get; set; }

        public Guid StageId { get; set; }
        public Stage Stage { get; set; }
        public Guid MigrationId { get; set; }

        public virtual ICollection<GroupTeam> GroupTeams { get; set; }

        public bool HasFixtures
        {
            get
            {
                return GroupTeams.All(x => x.FixtureParticipants.Any());
            }
        }

        [NotMapped]
        public int FixtureCountPerRound
        {
            get
            {
                var result = 0;
                for (var i = 0; i < GroupTeams.Count; i++ )
                {
                    result += i;
                }
                return result;
            }
        }

        /// <summary>
        /// Use it only for simple operations (like Count) coz it doesn't load all necessary data for example when filtering by group
        /// </summary>
        [NotMapped]
        public IQueryable<Fixture> Fixtures
        {
            get
            {
                //var homeParticipants = GroupTeams.SelectMany(x => x.FixtureParticipants.Where(y => y.IsHome)).Select(x => x.Fixture).AsQueryable();
                var fixtures = GroupTeams.SelectMany(x => x.FixtureParticipants.Select(y => y.Fixture)).Distinct().AsQueryable();
                return fixtures;
            }
        }

        [NotMapped]
        public Standings Standings { get; set; }

        [NotMapped]
        public List<FootballEvent> FootballEvents
        {
            get
            {
                return Fixtures.SelectMany(x => x.FootballEvents).ToList();
            }
        }
        
        [NotMapped]
        public IQueryable<Team> Teams
        {
            get
            {
                return GroupTeams.Select(gt => gt.Team).AsQueryable().Distinct(new TeamComparer());
            }
        }
    }
}