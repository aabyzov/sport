﻿using System;
using Sport.Common.Extensions;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class ChampType : Persistent, IHasMigrationId
    {
        public string Name { get; set; }

        public Guid SportKindId { get; set; }
        public virtual SportKind SportKind { get; set; }
        public Guid MigrationId { get; set; }
    }
}