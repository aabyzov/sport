using System;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class SuspendedFixture : Persistent
    {
        public Guid SuspensionId { get; set; }

        public Suspension Suspension { get; set; }

        public Guid FixtureId { get; set; }
        public Fixture Fixture { get; set; }
    }
}