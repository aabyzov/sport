﻿using System.Collections.Generic;

namespace Sport.Domain.Models
{
    public class EventTimeComparer : IEqualityComparer<FootballEventTime>
    {
        public bool Equals(FootballEventTime x, FootballEventTime y)
        {
            if (x == null && y == null) return true;
            if (x == null || y == null) return false;
            return x.Equals(y);
        }

        public int GetHashCode(FootballEventTime obj)
        {
            return obj.Minute.GetHashCode();
        }
    }
}