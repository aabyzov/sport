﻿using System;
using System.Collections.Generic;

namespace Sport.Domain.Models
{
    public class DateRange : IComparable<DateRange>
    {
        public DateRange(DateTime start, DateTime end)
        {
            Start = start;
            End = end;
        }
        public DateTime Start { get; set; }
        public DateTime End { get; set; } 

        public bool IsOverlap(DateRange toCompare)
        {
            if (toCompare.Start > Start)
            {
                return End >= toCompare.Start;
            }
            return toCompare.End >= Start;
        }

        public int CompareTo(DateRange other)
        {
            if (other == null) return 0;
            if (other.Start == Start && other.End == End) return 0;
            if (IsOverlap(other))
                throw new Exception("Ranges should not overlap in order to be compared");
            return Start.CompareTo(other.Start);
        }

        public override string ToString()
        {
            return Start.ToShortDateString() + " " + End.ToShortDateString();
        }
    }
}