﻿using System.Collections.Generic;

namespace Sport.Domain.Models
{
    public class TeamComparer : IEqualityComparer<Team>
    {
        public bool Equals(Team x, Team y)
        {
            if (x != null && y != null) return x.Id == y.Id;
            return x == null && y == null;
        }

        public int GetHashCode(Team obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}