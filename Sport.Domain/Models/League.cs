﻿using System;
using System.Collections.Generic;
using Sport.Common.Core;
using Sport.Common.Extensions;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class League : Persistent, IHasMigrationId, IUniqueEntity<League>
    {
        public string Name { get; set; }

        public Guid SportKindId { get; set; }
        public virtual SportKind SportKind { get; set; }

        public Guid MigrationId { get; set; }
        public List<EntityValidationHelper<League>> CheckForUnique()
        {
            return new List<EntityValidationHelper<League>>()
                {
                    new EntityValidationHelper<League>(x => x.Name == Name, "Name", "Name should be unique")
                };
        }
    }
}