﻿namespace Sport.Domain.Models
{
    public class FutsalFixtureParticiapnt : FixtureParticipant
    {
        public int? Fouls { get; set; }
        public int? TenMetresPenalty { get; set; }
    }
}