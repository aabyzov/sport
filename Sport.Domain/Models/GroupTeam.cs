﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Sport.Common.Extensions;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class GroupTeam : Persistent, IHasMigrationId
    {
        public GroupTeam()
        {
            FixtureParticipants = new HashSet<FixtureParticipant>();
        }
        public Guid TeamId { get; set; }
        public virtual Team Team { get; set; }

        public Guid GroupId { get; set; }
        public virtual Group Group { get; set; }
        public Guid MigrationId { get; set; }

        public virtual ICollection<FixtureParticipant> FixtureParticipants { get; set; }

        [NotMapped]
        public IQueryable<Fixture> Fixtures { get { return FixtureParticipants.Select(fp => fp.Fixture).Distinct().AsQueryable(); } }

        public override string ToString()
        {
            return string.Format("{0} in group '{1}' of '{2}'", Team.Name, Group.Name, Group.Stage.Champ.Name );
        }
    }
}