﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Sport.Common.Extensions;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class Team : Persistent, IHasMigrationId
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int? Chemistry { get; set; }
        public int? OverallRating { get; set; }

        public Guid LeagueId { get; set; }
        public virtual League League { get; set; }
        public Guid MigrationId { get; set; }
        public Guid ZoneId { get; set; }
        public virtual Zone Zone { get; set; }

        public virtual ICollection<TeamMember> TeamMembers { get; set; }
        public virtual ICollection<GroupTeam> GroupTeams { get; set; }

        [NotMapped]
        public IQueryable<Champ> Champs
        {
            get { return GroupTeams.Select(gt => gt.Group.Stage.Champ).AsQueryable(); }
        }

        [NotMapped]
        public IQueryable<Fixture> Fixtures
        {
            get { return GroupTeams.SelectMany(gt => gt.FixtureParticipants.Select(fp => fp.Fixture))
                .OrderBy(x => x.FixtureParticipants.FirstOrDefault(fp => fp.GroupTeam.TeamId == Id).GroupTeam.Group.Stage.Champ.Start).ThenBy(x => x.Date).Distinct().AsQueryable(); }
        }

        [NotMapped]
        public IQueryable<Fixture> OfficialFixtures {get { return Fixtures.Where(f => !f.IsFriendly); } }

        public override string ToString()
        {
            return Name;
        }
    }
}