﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sport.Domain.Models
{
    public class FootballEventTime : EventTime, IEquatable<FootballEventTime> //, IComparable<FootballEventTime>
    {
        public FootballEventTime()
        {

        }

        public FootballEventTime(int minute)
        {
            var minuteWithoutCompensation = minute;
            if (minuteWithoutCompensation > 0 && minuteWithoutCompensation <= 45)
            {
                FirstHalfMinute = minuteWithoutCompensation;
            }
            if (minuteWithoutCompensation > 45 && minuteWithoutCompensation <= 90)
            {
                SecondHalfMinute = minuteWithoutCompensation;
            }
            if (minuteWithoutCompensation > 90)
            {
                CompensatedMinute = minute - 90;
                SecondHalfMinute = 90;
            }
            Minute = minute;
            //EventOrder = eventOrder;
        }

        //public FootballEventTime(int minute, int compensatedMinute = 0, bool isFirstHalfOvertime = false, bool isSecondHalfOvertime = false)
        //{
        //    var minuteWithoutCompensation = minute - compensatedMinute;
        //    CompensatedMinute = compensatedMinute;
        //    if (minuteWithoutCompensation > 0 && minuteWithoutCompensation <= 45)
        //    {
        //        FirstHalfMinute = minuteWithoutCompensation;
        //    }
        //    if (minuteWithoutCompensation > 45 && minuteWithoutCompensation <= 90)
        //    {
        //        SecondHalfMinute = minuteWithoutCompensation;
        //    }
        //    if (minuteWithoutCompensation > 90 && minuteWithoutCompensation <= 105)
        //    {
        //        OverTimeFirstHalfMinute = minuteWithoutCompensation;
        //    }
        //    if (minuteWithoutCompensation > 105 && minuteWithoutCompensation <= 120)
        //    {
        //        OverTimeSecondHalfMinute = minuteWithoutCompensation;

        //    }
        //}

        //public int CompareTo(FootballEventTime other)
        //{
        //    return other == null ? 0 : other.Minute.CompareTo(Minute);
        //}

        // this is implemented to enhance performance
        public bool Equals(FootballEventTime obj)
        {
            if (obj == null) return false;
            return obj.Minute == Minute;
        }

        //public override bool Equals(object obj)
        //{
        //    if (obj == null) return false;
        //    var b = obj as FootballEventTime;
        //    if (b == null)
        //    {
        //        return false;
        //    }
        //    return b.Minute == Minute;
        //}
        //public override int GetHashCode()
        //{
        //    return Minute;
        //}

        public int? FirstHalfMinute { get; set; }

        public int? SecondHalfMinute { get; set; }

        public int? OverTimeFirstHalfMinute { get; set; }

        public int? OverTimeSecondHalfMinute { get; set; }

        public int? CompensatedMinute { get; set; }

        //[NotMapped]
        //public int Minute
        //{
        //    get
        //    {
        //        if (FirstHalfMinute.HasValue && FirstHalfMinute != 0)
        //            return FirstHalfMinute.Value;
        //        if (SecondHalfMinute.HasValue && SecondHalfMinute != 0)
        //            return SecondHalfMinute.Value + (CompensatedMinute.HasValue ? CompensatedMinute.Value : 0);
        //        if (OverTimeFirstHalfMinute.HasValue && OverTimeFirstHalfMinute != 0)
        //            return OverTimeFirstHalfMinute.Value;
        //        if (OverTimeSecondHalfMinute.HasValue && OverTimeSecondHalfMinute != 0)
        //            return OverTimeSecondHalfMinute.Value;
        //        return 0;
        //    }
        //}

        public int Minute { get; set; }

    }
}