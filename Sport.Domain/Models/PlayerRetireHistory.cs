﻿using System;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class PlayerRetireHistory : Persistent
    {
        public bool IsRetire { get; set; }
        public DateTime Date { get; set; }

        public Guid PlayerId { get; set; }
        public virtual Player Player { get; set; }
    }
}