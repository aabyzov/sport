﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class OutOfFixturePlayer : Persistent
    {
        public Guid PlayerId { get; set; }
        public virtual Player Player { get; set; }

        public Guid FixtureParticipantId { get; set; }
        public virtual FixtureParticipant FixtureParticipant { get; set; }

        public int MissedMatchCount { get; set; }

        public OutOfFixtureReasons Reason { get; set; }

        public override string ToString()
        {
            //return Player.FullName + " in " + FixtureParticipant.Fixture + " with reason " + "";
            return string.Format("{0} in {1} misses {2} with reason {3}", Player.FullName, FixtureParticipant.Fixture,
                                 MissedMatchCount, Reason);
        }

    }
}