﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public abstract class FixtureParticipant : Persistent
    {
        protected FixtureParticipant()
        {
            OutOfFixturePlayers = new HashSet<OutOfFixturePlayer>();
            Events = new HashSet<FixtureEvent>();
        }
        public int? Score { get; set; }
        [NotMapped]
        public bool IsPlayed { get { return Score != null; } }
        public bool IsHome { get; set; }

        public Guid GroupTeamId { get; set; }
        public virtual GroupTeam GroupTeam { get; set; }
        
        [NotMapped]
        public Team Team
        {
            get { return GroupTeam.Team; }
        }

        public Player[] Players
        {
            get { return Team.TeamMembers.Select(x => x.Player).ToArray(); }
        }

        public Guid FixtureId { get; set; }
        public virtual Fixture Fixture { get; set; }

        [NotMapped]
        public FixtureParticipant Opponent {get { return Fixture.FixtureParticipants.FirstOrDefault(x => x.Id != Id); }}

        public virtual ICollection<FixtureEvent> Events { get; set; }
        public virtual ICollection<SquadPlayer> SquadPlayers { get; set; }
        public virtual ICollection<OutOfFixturePlayer> OutOfFixturePlayers { get; set; }

        [NotMapped]
        public IQueryable<FootballEvent> FootballEvents
        {
            get { return Events.OfType<FootballEvent>().AsQueryable(); }
        }

        [NotMapped]
        public IQueryable<Fixture> PreviousFixtures
        {
            get
            {
                var team = GroupTeam.Team;
                var fixtures = team.Fixtures.ToList();
                return fixtures.Take(fixtures.IndexOf(fixtures.FirstOrDefault(x => x.Id == Fixture.Id)) - 1).AsQueryable();
            }
        }
        
        [NotMapped]
        public virtual FootballGoal[] Goals
        {
            get
            {
                return Events != null ? Events.OfType<FootballGoal>().ToArray() : new FootballGoal[0];
            }
        }

        [NotMapped]
        public virtual FootballOwnGoal[] OwnGoals
        {
            get
            {
                return Events != null ? Events.OfType<FootballOwnGoal>().ToArray() : new FootballOwnGoal[0];
            }
        }

        [NotMapped]
        public virtual FootballInjury[] Injuries
        {
            get
            {
                return Events != null ? Events.OfType<FootballInjury>().ToArray() : new FootballInjury[0];
            }
        }

        [NotMapped]
        public virtual FootballMissedPenalty[] MissedPenalties
        {
            get
            {
                return Events != null ? Events.OfType<FootballMissedPenalty>().ToArray() : new FootballMissedPenalty[0];
            }
        }

        [NotMapped]
        public virtual FootballYellowCard[] YellowCards
        {
            get
            {
                return Events != null ? Events.OfType<FootballYellowCard>().ToArray() : new FootballYellowCard[0];
            }
        }

        [NotMapped]
        public virtual FootballRedCard[] RedCards
        {
            get
            {
                return Events != null ? Events.OfType<FootballRedCard>().ToArray() : new FootballRedCard[0];
            }
        }

        public override string ToString()
        {
            return GroupTeam.Team.Name;
        }

    }
}