﻿using System;

namespace Sport.Domain.Models
{
    public class FootballInjury : FootballEvent
    {
        public int? MissedMatchCountInjury { get; set; }
        public DateTime? RecoveryDate { get; set; }
        public override string ToString()
        {
            return string.Format("{0} injured on {1} minute of '{2}' for {3} matches with recovery date {4}", Player.FullName, EventTime is FootballEventTime ? (EventTime as FootballEventTime).Minute : 0, FixtureParticipant.Fixture, MissedMatchCountInjury, RecoveryDate );
        }
    }
}