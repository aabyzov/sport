﻿namespace Sport.Domain.Models
{
    public class FutsalSquadPlayer : SquadPlayer
    {
        public int QuarterNumber { get; set; }
        public int PlayerInQuarterNumber { get; set; }
    }
}