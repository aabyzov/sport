﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class Suspension : Persistent
    {
        public Suspension()
        {
            SuspendedFixtures = new HashSet<SuspendedFixture>();
        }

        public int MissedMatchCount { get; set; }

        [NotMapped]
        public int AlreadyMissedMatches
        {
            get
            {
                return SuspendedFixtures.Count;
            }
        }

        public Guid FixtureEventId { get; set; }
        public FixtureEvent FixtureEvent { get; set; }

        public DateTime? Start { get; set; }

        public DateTime? End { get; set; }

        [NotMapped]
        public bool IsFinished
        {
            get
            {
                return MissedMatchCount == 0;
            }
        }

        public ICollection<SuspendedFixture> SuspendedFixtures { get; set; }

        public SuspensionReason Reason { get; set; }
    }
}