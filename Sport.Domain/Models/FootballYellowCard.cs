﻿namespace Sport.Domain.Models
{
    public class FootballYellowCard : FootballEvent
    {
        public override string ToString()
        {
            return string.Format("{0} got yellow card on {1} minute of '{2}'", Player.FullName, EventTime is FootballEventTime ? (EventTime as FootballEventTime).Minute : 0, FixtureParticipant.Fixture != null ? FixtureParticipant.Fixture.ToString() :  "(fixture name not loaded)");
        }
    }
}