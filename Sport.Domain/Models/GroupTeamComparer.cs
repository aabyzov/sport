﻿using System.Collections.Generic;

namespace Sport.Domain.Models
{
    public class GroupTeamComparer : IEqualityComparer<GroupTeam>
    {
        public bool Equals(GroupTeam x, GroupTeam y)
        {
            if (x != null && y != null && x.Team != null && y.Team != null) return x.Team.Id == y.Team.Id;
            return x == null && y == null;
        }

        public int GetHashCode(GroupTeam obj)
        {
            return obj.Team.Id.GetHashCode();
        }
    }
}