﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Sport.Common.Extensions;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class ChampSeason : Persistent, IHasMigrationId
    {
        /// <summary>
        /// Usually it's like World Cup 2006
        /// </summary>
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public Guid ChampTypeId { get; set; }
        public virtual ChampType ChampType { get; set; }

        public Guid? ChampSeasonId { get; set; }
        public virtual ChampSeason Parent { get; set; }

        public Guid MigrationId { get; set; }

        public virtual ICollection<Champ> Champs { get; set; }

        [NotMapped]
        public virtual IQueryable<Team> Teams { get { return Champs.SelectMany(c => c.Teams).Distinct(new TeamComparer()).AsQueryable(); } }
        
        /// <summary>
        /// That means Finals
        /// </summary>
        [NotMapped]
        public Champ Host
        {
            get
            {
                return Champs.FirstOrDefault(x => x.Zone.Name == "Host");
            }
        }

        [NotMapped]
        public Champ EuropeQualification
        {
            get
            {
                return Champs.FirstOrDefault(x => x.Zone.Name == "UEFA");
            }
        }

        [NotMapped]
        public Champ NorthCentralAmericaQualification
        {
            get
            {
                return Champs.FirstOrDefault(x => x.Zone.Name == "CONCACAF");
            }
        }

        [NotMapped]
        public Champ AsiaQualification
        {
            get
            {
                return Champs.FirstOrDefault(x => x.Zone.Name == "AFC");
            }
        }

        [NotMapped]
        public Champ SouthAmericaQualification
        {
            get
            {
                return Champs.FirstOrDefault(x => x.Zone.Name == "CONMEBOL");
            }
        }

        [NotMapped]
        public Champ AfricaQualification
        {
            get
            {
                return Champs.FirstOrDefault(x => x.Zone.Name == "CAF");
            }
        }

        [NotMapped]
        public Champ OceaniaQualification
        {
            get
            {
                return Champs.FirstOrDefault(x => x.Zone.Name == "OFC");
            }
        }
    }
}