﻿using Sport.Domain.Models.Enums;

namespace Sport.Domain.Models
{
    public class FootballMissedPenalty : FootballEvent
    {
        public MissedPenaltySituations Details { get; set; }

        public override string ToString()
        {
            return string.Format("{0} missed penalty on {1} minute with details {2}", Player.FullName, (EventTime as FootballEventTime).Minute, Details);
        }
    }
}