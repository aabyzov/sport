﻿using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class Position : Persistent
    {
        public string Name { get; set; }
    }
}