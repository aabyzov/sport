﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sport.Common.Core;

namespace Sport.Domain.Models
{
    public class FootballOwnGoal : FootballEvent
    {
        //public int? AssistantDetails { get; set; }

        public Guid? AssistantId { get; set; }
        public virtual Player Assistant { get; set; }

        public override string ToString()
        {
            return string.Format("{0} own goal on {1} minute with {2} ", Player.FullName, (EventTime as FootballEventTime).Minute, Assistant != null ? "assistant " + Assistant.FullName : "no assistant");
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            foreach (var brokenRule in base.Validate(validationContext))
            {
                yield return brokenRule;
            }
            var ownGoal = this;
            var goalAssistant = ownGoal.Assistant;
            if (goalAssistant != null)
            {
                if (ownGoal.FixtureParticipant.OutOfFixturePlayers.Any(x => x.PlayerId == goalAssistant.Id))
                    yield return new ValidationResult(String.Format(Constants.Errors.MissingMatchPlayerInEvents, Player.FullName, ownGoal));
            }
        }
    }
}