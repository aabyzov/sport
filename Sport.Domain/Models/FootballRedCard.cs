﻿namespace Sport.Domain.Models
{
    public class FootballRedCard : FootballEvent
    {
        public int MissedMatchCountRedCard { get; set; }
        public override string ToString()
        {
            return string.Format("{0} got red card on {1} minute of '{2}' for {3} matches", Player.FullName, EventTime is FootballEventTime ? (EventTime as FootballEventTime).Minute : 0, FixtureParticipant.Fixture, MissedMatchCountRedCard);
        }
    }
}