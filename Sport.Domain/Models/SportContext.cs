﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Infrastructure.MappingViews;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;
using System.Text;
using Sport.Common.Audit;
//using Sport.Common.Core;
using Sport.Common.Utility;
using Sport.Common.Model;
using Sport.Domain.Exceptions;
using Sport.Domain.Migrations;

namespace Sport.Domain.Models
{
    public class SportContext : DbContext
    {
        /// <summary>
        /// Max count of failed attempts to commit changes when there is concurrency error
        /// </summary>
        public int FailedAttemptsMaxCount { get; set; }

        private int _failedAttemptsCount = 0; 

        /// <summary>
        /// If true, then saved records will be audited and notification will be send to Notification service. By default, UseAudit = true.
        /// </summary>
        public bool UseAudit { get; set; }

        public IUserManager UserManager { get; set; }
        public SportContext()
            : this("name=SportContext")
        {
           
        }
        public SportContext(string connectionName) : base(connectionName.StartsWith("name=") ? connectionName : "name=" + connectionName)
        {
            FailedAttemptsMaxCount = 20;
            UseAudit = true;
        }

        public DbSet<AuditRecord> AuditRecords { get; set; }
        public DbSet<AuditRecordField> AuditRecordFields { get; set; }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SportContext, Configuration>());
            //base.Configuration.LazyLoadingEnabled = false;
            //modelBuilder.IncludeMetadataInDatabase = false;
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<FixtureParticipant>().HasRequired(x => x.Fixture).WithMany(x => x.FixtureParticipants).HasForeignKey(x => x.FixtureId);

            //modelBuilder.Entity<SportKind>().HasKey(x => x.Id)
            //.Property(x => x.Id)
            //.HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            //.HasKey(x => x.Id);

            //modelBuilder.Entity<OutOfFixturePlayer>().Property(x => x.ReasonEnum);
            //modelBuilder.IncludeMetadataInDatabase = false;
            //modelBuilder.Entity<Fixture>().HasRequired(m => m.Home);
            //modelBuilder.Entity<Fixture>().HasRequired(m => m.Away);
            //modelBuilder.Entity<FootballFixtureParticipant>().Map(m => m.Requires("Discriminator").HasValue("FOOTBALL"));
            //modelBuilder.Entity<FutsalFixtureParticiapnt>().Map(m => m.Requires("Discriminator").HasValue("FUTSAL"));
        }

        public override int SaveChanges()
        {
            try
            {
                var auditTrail = new AuditTrail(ChangeTracker.Entries(), this.GetRelationships());
                if (UseAudit)
                {
                    auditTrail.ProcessAuditChanges();
                }

                foreach (var entry in ChangeTracker.Entries().Where(x => x.State == EntityState.Modified).ToList())
                {
                    Set(ObjectContext.GetObjectType(entry.Entity.GetType())).Attach(entry.Entity);
                    Entry(entry.Entity).State = EntityState.Modified;
                }

                int numberOfUpdatedEntities = base.SaveChanges();

                if (UseAudit)
                {
                    auditTrail.ProcessCreatedEntities();

                    var auditRecords = auditTrail.AuditRecords;
                    if (auditRecords != null && auditRecords.Count > 0)
                    {
                        foreach (AuditRecord auditRecord in auditRecords) AuditRecords.Add(auditRecord);
                    }

                    base.SaveChanges();
                    //reset failed attempts counter if this try was successful
                    _failedAttemptsCount = 0;
                }

                return numberOfUpdatedEntities;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return ProcessConcurrencyException(ex);
            }
            catch (DbUpdateException ex)
            {
                throw new DbUpdateException("Unable to save changes. The following entity types are causing problems: " + string.Join(", ", ex.Entries.Select(e => e.Entity.GetType().Name)), ex);
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder();
                sb.AppendLine("Entity validation failed:");
                foreach (var item in ex.EntityValidationErrors)
                {
                    foreach (var error in item.ValidationErrors)
                    {
                        sb.AppendFormat("{0} -> {1}: {2}", item.Entry.Entity, error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                        Logger.Current.ErrorFormat("{0} -> {1}: {2}", item.Entry.Entity, error.PropertyName, error.ErrorMessage);
                        Console.WriteLine("{0} -> {1}: {2}", item.Entry.Entity, error.PropertyName, error.ErrorMessage);
                    }
                }

                throw new InvalidOperationException(sb.ToString(), ex);
            }
        }

        private int ProcessConcurrencyException(DbUpdateConcurrencyException ex)
        {
            //Logger.Current.WarnFormat("Concurrency error occured, {0} entries affected", ex.Entries.Count());

            bool hasForeignChanges = false;
            AuditRecord lastChange = null;
            ex.Entries.ToList().ForEach(
                x =>
                {
                    //AG [8/21/2012]. While synchronization, UserManager == null.
                    string userName = "Unknown user";
                    if (UserManager != null)
                        userName = UserManager.CurrentUsername;

                    Type entityType = ObjectContext.GetObjectType(x.Entity.GetType());

                    PropertyInfo idProperty = x.Entity.GetType().GetProperty("Id");
                    PropertyInfo timestampProperty = x.Entity.GetType().GetProperty("Timestamp");

                    Guid? id = idProperty != null && idProperty.PropertyType == typeof(Guid) ? idProperty.GetValue(x.Entity, null) as Guid? : null;
                    byte[] timestamp = timestampProperty != null ? timestampProperty.GetValue(x.Entity, null) as byte[] : null;

                    //log error
                    //Logger.Current.WarnFormat("Entity: {0}; Id: {1}; Timestamp: {2}",
                    //    entityType.FullName,
                    //    id,
                    //    timestamp != null ? string.Format("{0} : {1} : {2}",
                    //        timestamp.AsString(),
                    //        BitConverter.ToString(timestamp),
                    //        Convert.ToBase64String(timestamp)) : "Undefined");

                    AuditRecord auditRecord = id != null
                                                  ? AuditRecords.Where(a => a.EntityTable == entityType.Name
                                                        && (a.EntityNamespace == null || a.EntityNamespace == entityType.Namespace)
                                                        && a.EntityTableKey == id)
                                                        .OrderByDescending(a => a.AuditDate).FirstOrDefault()
                                                  : AuditRecords.Where(a => a.EntityTable == entityType.Name
                                                        && (a.EntityNamespace == null || a.EntityNamespace == entityType.Namespace))
                                                        .OrderByDescending(a => a.AuditDate).FirstOrDefault();

                    if (id != null && timestamp != null && auditRecord != null
                        && !string.IsNullOrEmpty(auditRecord.UserName) && !string.IsNullOrEmpty(userName)
                        && auditRecord.UserName.ToLowerInvariant().Trim() == userName.ToLowerInvariant().Trim())
                    {
                        //if the last person who edited the conflicted entity was current user then try to reload timestamp once
                        var dbValues = x.GetDatabaseValues();
                        if (dbValues != null)
                        {
                            byte[] newTimestamp = dbValues.GetValue<byte[]>("Timestamp");
                            timestampProperty.SetValue(x.Entity, newTimestamp, null);
                            Logger.Current.Debug("Timestamp updated");
                            return;
                        }
                    }
                    hasForeignChanges = true;
                    if (auditRecord != null && (lastChange == null || lastChange.AuditDate < auditRecord.AuditDate))
                    {
                        lastChange = auditRecord;
                    }
                });

            if (!hasForeignChanges && _failedAttemptsCount < FailedAttemptsMaxCount)
            {
                _failedAttemptsCount++;
                // re-try to save changes to db if all conflicts were resolved
                return SaveChanges();
            }
            _failedAttemptsCount = 0;

            User lastEditor = null;
            if (lastChange != null)
            {
                lastEditor = Users.FirstOrDefault(x => x.UserName.ToLower() == lastChange.UserName);
            }

            string userFullname = lastEditor != null ? lastEditor.FullName : "Unknown";

            throw new DataExpiredException(userFullname, ex);
        }

        public DbSet<SportKind> SportKinds { get; set; }
        public DbSet<League> Leagues { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<TeamMember> TeamMembers { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<TeamHistory> TeamHistories { get; set; }
        public DbSet<TeamMemberHistory> TeamMemberHistories { get; set; }
        public DbSet<PlayerRetireHistory> PlayerRetireHistories { get; set; }
        public DbSet<ChampType> ChampTypes { get; set; }
        public DbSet<ChampSeason> ChampSeasons { get; set; }
        public DbSet<Champ> Champs { get; set; }
        public DbSet<Stage> Stages { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupTeam> GroupTeams { get; set; }
        public DbSet<FixtureParticipant> FixtureParticipants { get; set; }
        public DbSet<Fixture> Fixtures { get; set; }
        public DbSet<FixtureEvent> FixtureEvents { get; set; }
        public DbSet<FootballGoal> FootballGoals { get; set; }
        public DbSet<FootballInjury> FootballInjuries { get; set; }
        public DbSet<FootballMissedPenalty> FootballMissedPenalties { get; set; }
        public DbSet<FootballOwnGoal> FootballOwnGoals { get; set; }
        public DbSet<FootballRedCard> FootballRedCards { get; set; }
        public DbSet<FootballYellowCard> FootballYellowCards { get; set; }

        public DbSet<OutOfFixturePlayer> OutOfFixturePlayers { get; set; }

        public DbSet<Suspension> Suspensions { get; set; }

        public DbSet<SuspendedFixture> SuspendedFixtures { get; set; }

        public DbSet<Zone> Zones { get; set; }

        

    }
}