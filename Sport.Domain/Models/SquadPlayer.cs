﻿using System;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public abstract class SquadPlayer : Persistent
    {
        public Guid PlayerId { get; set; }
        public virtual Player Player { get; set; }

        public Guid FixtureParticipantId { get; set; }
        public virtual FixtureParticipant FixtureParticipant { get; set; }

        public Guid PositionId { get; set; }
        public virtual Position Position { get; set; }
    }
}