﻿namespace Sport.Domain.Models
{
    public enum SuspensionReason
    {
        RedCard = 1,
        Injury = 2,
        YellowCard = 3
    }
}