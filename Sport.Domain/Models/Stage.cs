﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using Sport.Common.Extensions;
using Sport.Common.Model;
using Sport.Domain.Models.Enums;

namespace Sport.Domain.Models
{
    public class Stage : Persistent, IHasMigrationId
    {
        public string Name { get; set; }
        public int RoundCount { get; set; }

        public Guid ChampId { get; set; }
        public virtual Champ Champ { get; set; }

        public Guid? StageId { get; set; }
        public virtual Stage Parent { get; set; }

        public Guid MigrationId { get; set; }

        public virtual ICollection<Group> Groups { get; set; }

        [NotMapped]
        public virtual bool IsPlayed
        {
            get
            {
                var fixtures = Groups.SelectMany(y => y.GroupTeams.SelectMany(gt => gt.FixtureParticipants.Select(fp => fp.Fixture))).AsQueryable();
                //var participants = groupTeams.SelectMany(x => x.FixtureParticipants);
                //var fixtures = participants.Select(x => x.Fixture);
                return fixtures.Count(x => x.IsPlayed) == fixtures.Count();
            }
        }

        [NotMapped]
        public IQueryable<GroupTeam> GroupTeams { get { return Groups.SelectMany(x => x.GroupTeams).AsQueryable(); } }

        [NotMapped]
        public IQueryable<Team> Teams
        {
            get
            {
                return Groups.SelectMany(g => g.Teams).AsQueryable().Distinct(new TeamComparer());
            }
        }

        [NotMapped]
        public IQueryable<Fixture> Fixtures
        {
            get { return Groups.SelectMany(x => x.Fixtures).OrderBy(x => x.Date).AsQueryable(); }
        }

        [NotMapped]
        public StageType StageType
        {
            get
            {
                if (Name.ToLower().Contains("group"))
                    return StageType.Group;
                if (Name.ToLower().Contains("playoff"))
                    return StageType.Playoff;
                return StageType.Group;
            }
        }

        [NotMapped]
        public DateTime? StartDate
        {
            get
            {
                var earliestFixture = Fixtures.OrderBy(x => x.Date).FirstOrDefault();
                return earliestFixture != null ? earliestFixture.Date : null;
            }
        }

        [NotMapped]
        public DateTime? EndDate
        {
            get
            {
                var latestFixture = Fixtures.OrderByDescending(x => x.Date).FirstOrDefault();
                return latestFixture != null ? latestFixture.Date : null;
            }
        }
        
        public override string ToString()
        {
            return Name + " of " + Champ.Name + " of " + Champ.ChampSeason.Name;
        }
    }
}