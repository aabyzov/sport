﻿namespace Sport.Domain.Models
{
    public enum OutOfFixtureReasons
    {
        LowForm=1,
        LowMiddleForm=2,
        YellowCard=3,
        RedCard=4,
        Injury=5
    }
}