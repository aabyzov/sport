﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Sport.Common.Core;
using Sport.Domain.Models.Enums;

namespace Sport.Domain.Models
{
    public class FootballGoal : FootballEvent
    {
        public Guid? AssistantId { get; set; }
        public virtual Player Assistant { get; set; }
        public HowScored HowScored { get; set; }
        public BodyPart BodyPart { get; set; }
        public int? GoalDistance { get; set; }
        //public int? AssistantDetails { get; set; }

        public override string ToString()
        {
            return string.Format("{0} goal on {1} minute with {2} ", Player.FullName, (EventTime as FootballEventTime).Minute, Assistant != null ? "assistant " + Assistant.FullName : "no assistant" );
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            foreach (var brokenRule in base.Validate(validationContext))
            {
                yield return brokenRule;
            }
            var goal = this;
            var goalAssistant = goal.Assistant;
            if (goalAssistant != null)
            {
                if (goal.FixtureParticipant.OutOfFixturePlayers.Any(x => x.PlayerId == goalAssistant.Id))
                    yield return new ValidationResult(String.Format(Constants.Errors.MissingMatchPlayerInEvents, Player.FullName, goal));
            }
        }
    }
}