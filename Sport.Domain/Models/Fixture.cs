﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Sport.Common.Extensions;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class Fixture : Persistent, IHasMigrationId
    {
        // Should be nullable in case we generate it and don't know the date exactly
        public DateTime? Date { get; set; }

        //public DateTime DateOfFixture
        //{
        //    get
        //    {
        //        return Date != null ? (DateTime)Date : DateTime.Now;
        //    }
        //}
        public string CoachComment { get; set; }

        public string Location { get; set; }
        public bool IsFriendly { get; set; }

        public virtual ICollection<FixtureParticipant> FixtureParticipants { get; set; }

        public bool IsPlayed { get; set; }

        public bool HasOutOfFixtureProcessed { get; set; }

        public int MatchDay { get; set; }

        [NotMapped]
        public FixtureParticipant Home
        {
            get { return FixtureParticipants.FirstOrDefault(x => x.IsHome); }
        }

        [NotMapped]
        public Team HomeTeam
        {
            get { return Home.Team; }
        }

        [NotMapped]
        public Team AwayTeam
        {
            get { return Away.Team; }
        }

        [NotMapped]
        public IQueryable<FootballEvent> FootballEvents
        {
            get { return FixtureParticipants.SelectMany(x => x.Events).OfType<FootballEvent>().OrderBy(x => ((FootballEventTime)x.EventTime).Minute).ThenBy(x => x.EventOrder).AsQueryable(); }
        }

        [NotMapped]
        public virtual FixtureParticipant Away { get { return FixtureParticipants.FirstOrDefault(x => !x.IsHome); } }


        public Guid MigrationId { get; set; }

        [NotMapped]
        public string HomeTeamName { get { return Home.GroupTeam.Team.Name; } }

        [NotMapped]
        public string AwayTeamName { get { return Away.GroupTeam.Team.Name; } }

        [NotMapped]
        public int? HomeScore { get { return Home.Score; } }

        [NotMapped]
        public int? HomeScoreBasedOnEvents
        {
            get
            {
                if (!FootballEvents.Any()) return 0;
                int homeGoals = 0;
                int awayGoals = 0;
                foreach (var fixtureEvent in FootballEvents)
                {
                    IncreaseHomeOrAwayGoal(fixtureEvent, ref homeGoals, ref awayGoals);
                }
                return homeGoals;
            }
        }

        [NotMapped]
        public int? AwayScore { get { return Away.Score; } }

        [NotMapped]
        public int? AwayScoreBasedOnEvents
        {
            get
            {
                if (!FootballEvents.Any()) return 0;
                var homeGoals = 0;
                var awayGoals = 0;
                foreach (var fixtureEvent in FootballEvents)
                {
                    IncreaseHomeOrAwayGoal(fixtureEvent, ref homeGoals, ref awayGoals);
                }
                return awayGoals;
            }
        }

        public void IncreaseHomeOrAwayGoal(FootballEvent fixtureEvent, ref int homeGoals, ref int awayGoals)
        {
            if (fixtureEvent.FixtureParticipant.IsHome)
            {
                if (fixtureEvent is FootballGoal || fixtureEvent is FootballOwnGoal)
                {
                    homeGoals++;
                }
                //if (fixtureEvent is FootballOwnGoal)
                //{
                //    awayGoals++;
                //}
            }
            else
            {
                if (fixtureEvent is FootballGoal || fixtureEvent is FootballOwnGoal)
                {
                    awayGoals++;
                }
                //if (fixtureEvent is FootballOwnGoal)
                //{
                //    homeGoals++;
                //}
            }
        }

        [NotMapped]
        public Champ Champ
        {
            get { return FixtureParticipants.FirstOrDefault().GroupTeam.Group.Stage.Champ; }
        }

        [NotMapped]
        public Group Group
        {
            get
            {
                return FixtureParticipants.FirstOrDefault().GroupTeam.Group;
            }
        }

        public override string ToString()
        {
            return string.Format("{0} {1}:{2} {3} ({4} Matchday {5})", HomeTeamName, HomeScore, AwayScore, AwayTeamName, Date.Value.ToString("g"), MatchDay);
        }

    }
}