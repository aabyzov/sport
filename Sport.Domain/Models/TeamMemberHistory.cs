﻿using System;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class TeamMemberHistory : Persistent
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public Guid TeamId { get; set; }
        public virtual Team Team { get; set; }

        public Guid PlayerId { get; set; }
        public virtual Player Player { get; set; }
    }
}