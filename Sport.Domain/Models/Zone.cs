﻿using System;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class Zone : Persistent
    {
        public string Name { get; set; }
        public int MigrationEnumId { get; set; }
    }
}