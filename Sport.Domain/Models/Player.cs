﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using Sport.Common.Extensions;
using Sport.Common.Model;

namespace Sport.Domain.Models
{
    public class Player : Persistent, IHasMigrationId
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [NotMapped]
        public string FullName { get
        {
            if (!String.IsNullOrEmpty(FirstName))
                return FirstName + " " + LastName;
            return LastName;
        } }

        [NotMapped]
        public TeamMember FirstTeamMember
        {
            get { return TeamMembers.FirstOrDefault(); }
        }

        public TeamMember TeamMember(Team team)
        {
            return TeamMembers.FirstOrDefault(x => x.TeamId == team.Id);
        }

        public virtual ICollection<TeamMember> TeamMembers { get; set; }

        public virtual ICollection<FixtureEvent> FixtureEvents { get; set; } 
        public Guid MigrationId { get; set; }

        [NotMapped]
        public IQueryable<Fixture> Fixtures
        {
            get
            {
                return
                    TeamMembers.SelectMany(
                        tm => tm.Team.GroupTeams.SelectMany(gt => gt.FixtureParticipants.Select(fp => fp.Fixture)))
                               .AsQueryable().OrderBy(x => x.FixtureParticipants.FirstOrDefault().GroupTeam.Group.Stage.Champ.Start).ThenBy(x => x.Date).Distinct().AsQueryable();
            }
        }

        [NotMapped]
        public IQueryable<Fixture> OfficialFixtures
        {
            get
            {
                return
                    Fixtures.Where(f => !f.IsFriendly);
            }
        }

        [NotMapped]
        public IQueryable<FootballEvent> FootballEvents
        {
            get
            {
                return TeamMembers.SelectMany(
                        tm => tm.Team.GroupTeams.SelectMany(gt => gt.FixtureParticipants.SelectMany(fp => fp.Events))).OfType<FootballEvent>()
                               .AsQueryable();
            }
        }
        
        [NotMapped]
        public IQueryable<FootballGoal> FootballGoals
        {
            get
            {
                return
                    Fixtures.SelectMany(f => f.FixtureParticipants.SelectMany(fp => fp.Events))
                            .Where(x => x is FootballGoal && x.PlayerId == Id)
                            .OfType<FootballGoal>()
                            .OrderBy(x => (x.EventTime as FootballEventTime).Minute)
                            .ThenBy(x => x.EventOrder);
            }
        }

        [NotMapped]
        public IQueryable<FootballGoal> FootballGoalAssists
        {
            get
            {
                return
                    Fixtures.SelectMany(f => f.FixtureParticipants.SelectMany(fp => fp.Events))
                            .Where(x => x is FootballGoal && (x as FootballGoal).AssistantId == Id)
                            .OfType<FootballGoal>()
                            .OrderBy(x => (x.EventTime as FootballEventTime).Minute)
                            .ThenBy(x => x.EventOrder);
            }
        }

        [NotMapped]
        public IQueryable<FootballOwnGoal> FootballOwnGoals
        {
            get
            {
                return
                    Fixtures.SelectMany(f => f.FixtureParticipants.SelectMany(fp => fp.Events))
                            .Where(x => x is FootballOwnGoal && x.PlayerId == Id)
                            .OfType<FootballOwnGoal>()
                            .OrderBy(x => (x.EventTime as FootballEventTime).Minute)
                            .ThenBy(x => x.EventOrder);
            }
        }

        [NotMapped]
        public IQueryable<FootballOwnGoal> FootballOwnGoalAssists
        {
            get
            {
                return
                    Fixtures.SelectMany(f => f.FixtureParticipants.SelectMany(fp => fp.Events))
                            .Where(x => x is FootballGoal && (x as FootballOwnGoal).AssistantId == Id)
                            .OfType<FootballOwnGoal>()
                            .OrderBy(x => (x.EventTime as FootballEventTime).Minute)
                            .ThenBy(x => x.EventOrder);
            }
        }

        [NotMapped]
        public IQueryable<FootballEvent> FootballAssistsAll
        {
            get { return FootballOwnGoalAssists.OfType<FootballEvent>().Union(FootballGoalAssists); }
        }

        public IQueryable<FootballYellowCard> FootballYellowCardsBeforeFixture(Fixture fixture)
        {
                //return
                //    Fixtures.SelectMany(f => f.FixtureParticipants.SelectMany(fp => fp.Events))
                //            .Where(x => x is FootballYellowCard && x.PlayerId == Id)
                //            .OfType<FootballYellowCard>()
                //            .OrderBy(x => x.FixtureParticipant.Fixture.Date)
                //            .ThenBy(x => (x.EventTime as FootballEventTime).Minute)
                //            .ThenBy(x => x.EventOrder);
                if (fixture== null)
                    return FixtureEvents.OfType<FootballYellowCard>().OrderBy(x => x.FixtureParticipant.Fixture.Date).AsQueryable().Include(x => x.FixtureParticipant.GroupTeam.Group.Stage.Champ);
                return FixtureEvents.OfType<FootballYellowCard>().OrderBy(x => x.FixtureParticipant.Fixture.Date).AsQueryable().Where(x => x.FixtureParticipant.Fixture.Date < fixture.Date).AsQueryable();
        }

        
        public IQueryable<FootballYellowCard> FootballYellowCardsInOfficialMatchesBeforeFixture(Fixture fixture)
        {
            
                return
                    //Fixtures.SelectMany(f => f.FixtureParticipants.SelectMany(fp => fp.Events))
                    //        .Where(x => x is FootballYellowCard && x.PlayerId == Id && !x.FixtureParticipant.Fixture.IsFriendly)
                    //        .OfType<FootballYellowCard>()
                    //        .OrderBy(x => x.FixtureParticipant.Fixture.Date)
                    //        .ThenBy(x => (x.EventTime as FootballEventTime).Minute)
                    //        .ThenBy(x => x.EventOrder);
                    FootballYellowCardsBeforeFixture(fixture).Where(x => !x.FixtureParticipant.Fixture.IsFriendly);
            
        }

        public override string ToString()
        {
            return FullName;
        }
    }
}