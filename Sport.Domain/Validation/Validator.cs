﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.DataAnnotations;
//using System.Linq;
//using Sport.Common.Core;
//using Sport.Domain.Exceptions;
//using Sport.Domain.Models;

//namespace Sport.Domain.Validators
//{
//    public abstract class Validator
//    {
//        public bool IsValid { get { return BrokenRules == null || !BrokenRules.Any(); } }
//        public abstract IEnumerable<string> BrokenRules { get; }
//    }

//    public class FootballGoalValidator : FixtureEventValidator
//    {
//        public FootballGoalValidator(FootballGoal fixtureEvent, Player player) : base(fixtureEvent, player)
//        {
            
//        }

//        public override IEnumerable<string> BrokenRules
//        {
//            get
//            {
//                foreach (var brokenRule in base.BrokenRules)
//                {
//                    yield return brokenRule;
//                }
                
//                var goal = FixtureEvent as FootballGoal;
//                var goalAssistant = goal.Assistant;
//                if (goalAssistant != null)
//                {

//                    if (goal.FixtureParticipant.OutOfFixturePlayers.Any(x => x.PlayerId == goalAssistant.Id))
//                        yield return Constants.Errors.MissingMatchPlayerInEvents;
//                }
//            }
//        }
//    }

//    public class FootballOwnGoalValidator : FixtureEventValidator
//    {
//        public FootballOwnGoalValidator(FootballGoal fixtureEvent, Player player)
//            : base(fixtureEvent, player)
//        {

//        }

//        public override IEnumerable<string> BrokenRules
//        {
//            get
//            {
//                foreach (var brokenRule in base.BrokenRules)
//                {
//                    yield return brokenRule;
//                }

//                var goal = FixtureEvent as FootballOwnGoal;
//                var goalAssistant = goal.Assistant;
//                if (goalAssistant != null)
//                {

//                    if (goal.FixtureParticipant.OutOfFixturePlayers.Any(x => x.PlayerId == goalAssistant.Id))
//                        yield return String.Format(Constants.Errors.MissingMatchPlayerInEvents, Player.FullName,
//                            FixtureEvent);
//                }
//            }
//        }
//    }

//    public class FixtureEventValidator : Validator
//    {
//        protected readonly FixtureEvent FixtureEvent;
//        protected readonly Player Player;
//        public FixtureEventValidator(FixtureEvent fixtureEvent, Player player)
//        {
//            Player = player;
//            FixtureEvent = fixtureEvent;
//        }

//        public override IEnumerable<string> BrokenRules
//        {
//            get
//            {
//                var player = FixtureEvent.Player;
//                if (FixtureEvent.FixtureParticipant.OutOfFixturePlayers.Any(x => x.PlayerId == player.Id))
//                    yield return
//                        String.Format(Constants.Errors.MissingMatchPlayerInEvents, player.FullName,
//                            FixtureEvent);
//            }
//        }
//    }

//}