﻿using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class SportKindRepository : BaseRepository<SportKind, SportContext>, ISportKindRepository
    {
         public SportKindRepository(SportContext db)
         {
             DbContext = db;
         }
    }
}