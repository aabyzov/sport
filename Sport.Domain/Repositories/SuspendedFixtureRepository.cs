﻿using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class SuspendedFixtureRepository : BaseRepository<SuspendedFixture, SportContext>, ISuspendedFixtureRepository
    {
        public SuspendedFixtureRepository(SportContext db)
        {
            DbContext = db;
        }
    }
}