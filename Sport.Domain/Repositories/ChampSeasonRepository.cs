﻿using System;
using System.Data.Entity;
using System.Linq;
using Sport.Common.Core;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class ChampSeasonRepository : BaseRepository<ChampSeason, SportContext>, IChampSeasonRepository
    {
        public ChampSeasonRepository(SportContext db)
        {
            DbContext = db;
        }

        public override IQueryable<ChampSeason> GetQueryable()
        {
            return base.GetQueryable().Include(x => x.ChampType).Include(x => x.Champs.Select(y => y.Zone));
        }

        public ChampSeason WorldCup2006()
        {
            return base.GetQueryable().FirstOrDefault(x => x.ChampType.Name.StartsWith(Constants.WorldCup) && x.End.Year == Constants.Year2006);
        }

        public ChampSeason WorldCup2006WithTeams()
        {
            return base.GetQueryable().Include(x => x.Champs.Select(c => c.Stages.Select(s => s.Groups.Select( g => g.GroupTeams.Select(gt => gt.Team))))).FirstOrDefault(x => x.ChampType.Name.StartsWith(Constants.WorldCup) && x.End.Year == Constants.Year2006);
        }

        //public ChampSeason WorldCup2006WithFixtures()
        //{

        //}

        public Champ Host(Guid id)
        {
            return DbContext.Champs.FirstOrDefault(x => x.ChampSeasonId == id && x.Zone.Name == Constants.Host);
        }
    }
}