﻿using System;
using System.Data.Entity;
using System.Linq;
using Sport.Domain.Models;
using Sport.Domain.Repositories.IQueryable;

namespace Sport.Domain.Repositories
{
    public class PlayerRepository : BaseRepository<Player, SportContext>, IPlayerRepository
    {
        public PlayerRepository(SportContext db)
        {
            DbContext = db;
        }

        public IQueryable<Player> WithEvents()
        {
            return GetQueryable()
                .Include(
                    x =>
                    x.TeamMembers.Select(
                        tm => tm.Team.GroupTeams.Select(gt => gt.FixtureParticipants.Select(fp => fp.Fixture.FixtureParticipants.Select(fp1 => fp1.GroupTeam.Team)))))
                .Include(
                    x =>
                    x.TeamMembers.Select(
                        tm => tm.Team.GroupTeams.Select(gt => gt.FixtureParticipants.Select(fp => fp.Fixture.FixtureParticipants.Select(fp1 => fp1.Events.Select(fp1e => fp1e.EventTime))))))
                .Include(
                    x =>
                    x.TeamMembers.Select(
                        tm => tm.Team.GroupTeams.Select(gt => gt.FixtureParticipants.Select(fp => fp.Events.Select(fe => fe.EventTime)))));
        }

        public IQueryable<Player> WithAnyEventsAndTeamOutOfFixturePlayers()
        {
            return WithEvents()
                .Include(
                    x => x.TeamMembers.Select(tm => tm.Team.GroupTeams.Select(gt => gt.Group.Stage.Champ.ChampSeason)))
                .Include(
                    x =>
                    x.TeamMembers.Select(
                        tm =>
                        tm.Team.GroupTeams.Select(gt => gt.FixtureParticipants.Select(fp => fp.OutOfFixturePlayers))));
        }

        public IQueryable<Player> WithAnyYellowCardEvents()
        {
            //return WithAnyEventsAndTeamOutOfFixturePlayers().Where(p => p.TeamMembers.SelectMany(
            //    tm => tm.Team.GroupTeams.SelectMany(gt => gt.FixtureParticipants.SelectMany(fp => fp.Events)))
            //                                                             //.OfType<FootballEvent>() Do we really need this?
            //                                                             .OfType<FootballYellowCard>()
            //                                                             .Any());
            return WithAnyEventsAndTeamOutOfFixturePlayers().Where(p => DbContext.FixtureEvents.OfType<FootballYellowCard>().FirstOrDefault(fe => fe.Player == p) != null );
            //return DbContext.Players.Where(p => DbContext.FixtureEvents.OfType<FootballYellowCard>().FirstOrDefault(fe => fe.Player == p) != null);
        }

        public IQueryable<Player> FindAllByFixtureWithAnyYellowCard(Fixture fixture)
        {
            return GetQueryable().Where(p => DbContext.FixtureEvents.OfType<FootballYellowCard>().Any(fe => fe.Player == p)).ByFixture(fixture);
        }
    }
}