﻿using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface ISuspensionRepository : IRepository<Suspension>
    {
        IQueryable<Suspension> FindAllActiveBy(Fixture fixture, Player player);
    }
}