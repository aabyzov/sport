﻿using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface IFixtureEventRepository : IRepository<FixtureEvent>
    {
        IQueryable<FootballEvent> FootballEvents();
        IQueryable<FootballGoal> FootballGoals();
        IQueryable<FootballOwnGoal> FootballOwnGoals();
        IQueryable<FootballEvent> FootballEventsWithOutOfFixture();
        IQueryable<FootballEvent> FootballEventsShort();
    }
}