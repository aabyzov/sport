﻿using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface ILeagueRepository : IRepository<League>
    {
        League International();
    }
}