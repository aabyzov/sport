﻿using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface IGroupRepository : IRepository<Group>
    {

    }
}