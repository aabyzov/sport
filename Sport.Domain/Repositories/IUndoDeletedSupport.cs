﻿namespace Sport.Domain.Repositories
{
    public interface IUndoDeletedSupport
    {
        /// <summary>
        /// If true, than entity was removed.
        /// </summary>
        bool Unused { get; set; }
    }
}