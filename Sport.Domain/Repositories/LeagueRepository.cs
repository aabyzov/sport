﻿using System.Linq;
using Sport.Common.Core;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class LeagueRepository : BaseRepository<League, SportContext>, ILeagueRepository
    {
         public LeagueRepository(SportContext db)
         {
             DbContext = db;
         }

        public League International()
        {
            return DbContext.Leagues.FirstOrDefault(x => x.Name.Contains(Constants.International));
        }
    }
}