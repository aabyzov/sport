﻿using System.Data.Entity;
using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class FixtureEventRepository : BaseRepository<FixtureEvent, SportContext>, IFixtureEventRepository
    {
        public FixtureEventRepository(SportContext db)
        {
            DbContext = db;
        }

        public IQueryable<FootballEvent> FootballEventsShort()
        {
            return DbContext.FixtureEvents.OfType<FootballEvent>().
                Include(x => x.EventTime).Include(x => x.Player)
                .Include(x => x.FixtureParticipant.Fixture);
        } 

        public IQueryable<FootballEvent> FootballEvents()
        {
            return GetQueryable().OfType<FootballEvent>()
                .Include(x => x.FixtureParticipant.GroupTeam.Team)
                .Include(x => x.FixtureParticipant.GroupTeam.Group.Stage.Champ)
                .Include(x => x.FixtureParticipant.Fixture)
                .Include(x => x.EventTime).Include(x => x.Player.TeamMembers);
        }

        public IQueryable<FootballEvent> FootballEventsWithOutOfFixture()
        {
            return FootballEvents().Include(x => x.FixtureParticipant.OutOfFixturePlayers)
                .Include(x => x.FixtureParticipant.Fixture.FixtureParticipants.Select(fp => fp.GroupTeam.Team))
                .Include(x => x.FixtureParticipant.Fixture.FixtureParticipants.Select(fp => fp.GroupTeam.Group.Stage.Champ))
                .Include(x => x.FixtureParticipant.Fixture.FixtureParticipants.Select(fp => fp.Events));
        }

        public IQueryable<FootballGoal> FootballGoals()
        {
            return DbContext.FootballGoals.Include(x => x.Assistant).Include(x => x.Assistant.TeamMembers).Include(x => x.FixtureParticipant.GroupTeam.Team);
        }

        public IQueryable<FootballOwnGoal> FootballOwnGoals()
        {
            return DbContext.FootballOwnGoals.Include(x => x.Assistant.TeamMembers).Include(x => x.FixtureParticipant.GroupTeam.Team).Include(x => x.FixtureParticipant.Fixture.FixtureParticipants);
        } 
    }
}