﻿using System;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface IChampSeasonRepository : IRepository<ChampSeason>
    {
        ChampSeason WorldCup2006();
        Champ Host(Guid id);
        ChampSeason WorldCup2006WithTeams();
    }
}