﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Sport.Domain.Repositories
{
    public interface IRepository<T>
    {
        #region Public Properties

        bool Lazy { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Helper method to include child objects.
        /// </summary>
        IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties);

        /// <summary>
        /// 	Delete a given instance of T.
        /// </summary>
        /// <param name = "item"></param>
        void Delete(T item);

        /// <summary>
        /// 	Delete all instances of T.
        /// </summary>
        void DeleteAll();

        /// <summary>
        /// 	Find all instances of T and return it as List.
        /// </summary>
        /// <returns>List of entities.</returns>
        List<T> FindAll();

        /// <summary>
        /// 	Find all instances of T and return it as List, sorted by unique name.
        /// </summary>
        /// <returns>List of entities.</returns>
        List<T> FindAllSortedByUniqueField();

        /// <summary>
        /// 	Find all instances of T and return it as List.
        /// 	<param name = "pageIndex">Default value is 0, meaning take first page.</param>
        /// 	<param name = "pageRecords">Default value is 30, meaning take only 30 records.</param>
        /// </summary>
        /// <returns></returns>
        List<T> FindAll(int pageIndex = 0, int pageRecords = 30);

        /// <summary>
        /// 	Find instances of T for the given expression and return it as List.
        /// </summary>
        /// <param name = "filter">a Where clause</param>
        /// <param name="orderBy">Sort order</param>
        /// <param name = "pageIndex">Default value is 0, meaning take first page.</param>
        /// <param name = "pageRecords">By default returns all records.</param>
        /// <param name="includeProperties"></param>
        /// <returns></returns>
        List<T> FindAllBy(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            int pageIndex = 0,
            int pageRecords = int.MaxValue,
            string includeProperties = "");

        /// <summary>
        /// 	Find instances of T for the given expression and return it as List.
        /// </summary>
        /// <param name="filtersKeyValuePairs"></param>
        /// <param name="orderDirection">asc or des</param>
        /// <param name="orderByColumnName">The column name</param>
        /// <param name = "pageIndex">Default value is 0, meaning take first page.</param>
        /// <param name = "pageRecords">By default returns all records.</param>
        /// <returns></returns>
        List<T> FindAllBy(
            List<KeyValuePair<string, string>> filtersKeyValuePairs,
            string orderDirection = "asc",
            string orderByColumnName = "Id",
            int pageIndex = 0,
            int pageRecords = int.MaxValue);


        List<T> FindAllIncluding(Expression<Func<T, object>>[] includes = null, Expression<Func<T, bool>> filter = null);

        /// <summary>
        /// 	Find a single instance of T by primary key. 
        /// 	Note: we assume that primary keys are one column integers.
        /// </summary>
        /// <param name = "id"></param>
        /// <returns></returns>
        T FindById(Guid id);

        /// <summary>
        /// 	Find a single instance of T by the given expression. 
        /// </summary>
        /// <param name="filter">The filter expression, must not be null.</param>
        /// <returns>The first item that matches the filter or <c>null</c>.</returns>
        T FindOneBy(Expression<Func<T, bool>> filter);

        /// <summary>
        /// Returns the number of items of T type.
        /// </summary>
        /// <returns></returns>
        int GetItemsCount();

        /// <summary>
        /// 	Saves a new instance of <see cref="T"/>.
        /// </summary>
        /// <param name = "item"></param>
        void Save(T item);

        /// <summary>
        /// Check entity to be unique
        /// </summary>
        Dictionary<string, string> CheckOnUnique(T item);

        /// <summary>
        /// Check entity to be unique from List
        /// </summary>
        Dictionary<string, string> CheckOnUnique(List<T> items, T item);

        /// <summary>
        /// Update an existing entity with the provided value.
        /// </summary>
        /// <param name="toUpdate"></param>
        /// <param name="current"></param>
        void Update(T toUpdate, T current);

        //T GetByExternalId(string externalId);

        bool Any(Expression<Func<T, bool>> Filter);
        bool Any();

        #endregion

        T FindByMigrationId(Guid migrationId);
        //IQueryable<T> GetQueryable();

        
    }
}