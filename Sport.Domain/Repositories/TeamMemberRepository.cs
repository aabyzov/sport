﻿using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class TeamMemberRepository : BaseRepository<TeamMember, SportContext>, ITeamMemberRepository
    {
        public TeamMemberRepository(SportContext db)
        {
            DbContext = db;
        }
    }

    public interface ITeamMemberRepository : IRepository<TeamMember>
    {
    }
}