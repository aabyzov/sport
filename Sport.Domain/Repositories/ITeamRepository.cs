﻿using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface ITeamRepository : IRepository<Team>
    {
        IQueryable<Team> GetByChamp(Champ champ);
        IQueryable<Team> WithLeague();
        IQueryable<Team> WithFixtures();
        IQueryable<Team> WithFixturesAndChamp();
        IQueryable<Team> WithFixturesAndTeams();
        IQueryable<Team> WithEventsAndTeams();
    }
}