﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Sport.Common.Core;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class ChampRepository : BaseRepository<Champ, SportContext>, IChampRepository
    {
        private readonly Expression<Func<Champ, bool>> _wcFinalsSelector = 
            x => x.Zone.Name == Constants.Host && x.ChampSeason.Name.StartsWith(Constants.WorldCup) && x.ChampSeason.End.Year == Constants.Year2006;
        private readonly Expression<Func<Champ, bool>> _wcWithQualificationSelector =
            x => x.ChampSeason.Name.StartsWith(Constants.WorldCup) && x.ChampSeason.End.Year == Constants.Year2006;
        public ChampRepository(SportContext db)
        {
            DbContext = db;
        }

        public override IQueryable<Champ> GetQueryable()
        {
            return base.GetQueryable().Include(x => x.Zone);
        }

        private Champ ApplySelector(IQueryable<Champ> champs, Expression<Func<Champ, bool>> selector)
        {
            return champs.FirstOrDefault(selector);
        }
       
        public Champ WorldCup2006FinalsWithFixtures()
        {
            var queryable = WithFixturesAndTeams();
            return ApplySelector(queryable, _wcFinalsSelector);
        }

        public Champ[] FindAllOrderedByStartDate()
        {
            return WithFixtures().ToArray().OrderBy(x => x.StartDate).ToArray();
        }

        public IQueryable<Champ> WithFixtures()
        {
            return GetQueryable().Include(x => x.Stages.Select(s => s.Groups.Select(g => g.GroupTeams.Select(gt => gt.FixtureParticipants.Select(fp => fp.Fixture)))));
        }

        public IQueryable<Champ> WithFixturesAndTeams()
        {
            var queryable =
                GetQueryable()
                    .Include(
                        x =>
                        x.Stages.Select(
                            y => y.Groups.Select(g => g.GroupTeams.Select(z => z.FixtureParticipants.Select(a => a.Fixture)))))
                            .Include(x => x.Stages.Select(y => y.Groups.Select(g => g.GroupTeams.Select(gt => gt.Team.Zone))));
            return queryable;
        }

        public Champ WorldCup2006FinalsWithTeams()
        {
            var queryable = GetQueryable().Include(x => x.Stages.Select(y => y.Groups.Select(g => g.GroupTeams.Select(gt => gt.Team.Zone))));
            return ApplySelector(queryable, _wcFinalsSelector);
        }

        public IQueryable<Champ> WorldCup2006WithQualificationWithTeams()
        {
            return GetQueryable().Include(x => x.Stages.Select(y => y.Groups.Select(g => g.GroupTeams.Select(gt => gt.Team.Zone)))).Where(_wcWithQualificationSelector);
            
        }

        public IQueryable<Team> GetWorldCup2006Teams()
        {
            return WorldCup2006WithQualificationWithTeams().SelectMany(x => x.Stages.SelectMany(s => s.Groups.SelectMany(g => g.GroupTeams.Select(gt => gt.Team)))).AsQueryable().GroupBy(t => t.Id).Select(team => team.FirstOrDefault());
        }
    }


}