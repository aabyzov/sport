﻿using System.Data.Entity;
using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class GroupRepository : BaseRepository<Group, SportContext>, IGroupRepository
    {
        public GroupRepository(SportContext db)
        {
            DbContext = db;
        }

        public override IQueryable<Group> GetQueryable()
        {
            return base.GetQueryable().AsQueryable().Include(x => x.Stage).Include(x => x.GroupTeams);
        }
        
    }
}