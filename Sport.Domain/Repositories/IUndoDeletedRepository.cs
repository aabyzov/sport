﻿namespace Sport.Domain.Repositories
{
    public interface IUndoDeletedRepository<T> : IRepository<T>
    {
        bool ShowRemoved { get; set; }

        /// <summary>
        /// If set, ShowRemoved property is ignored
        /// </summary>
        bool ShowAll { get; set; }

        /// <summary>
        /// Restores the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void Restore(T item);
    }
}