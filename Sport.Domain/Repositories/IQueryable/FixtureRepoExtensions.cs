﻿using System;
using Sport.Domain.Models;
using System.Linq;

namespace Sport.Domain.Repositories.IQueryable
{
    public static class FixtureRepoExtensions
    {
        public static IQueryable<Fixture> ByChampId(this IQueryable<Fixture> query, Guid champId)
        {
            return query.Where(x => x.FixtureParticipants.FirstOrDefault().GroupTeam.Group.Stage.ChampId == champId);
        }

        public static IQueryable<Fixture> ByGroupId(this IQueryable<Fixture> query, Guid groupId)
        {
            return query.Where(x => x.FixtureParticipants.FirstOrDefault().GroupTeam.Group.Id == groupId);
        }
    }
}