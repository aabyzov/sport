﻿using System;
using Sport.Domain.Models;
using System.Linq;

namespace Sport.Domain.Repositories.IQueryable
{
    public static class PlayerRepoExtensions
    {
        public static IQueryable<Player> ByFixture(this IQueryable<Player> query, Fixture fixture)
        {
            return query.Where(x =>
                            x.TeamMembers.FirstOrDefault().TeamId == fixture.Home.GroupTeam.TeamId ||
                            x.TeamMembers.FirstOrDefault().TeamId == fixture.Away.GroupTeam.TeamId);
        }
    }
}