﻿using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface IGroupTeamRepository : IRepository<GroupTeam>
    {
    }
}