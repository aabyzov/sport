﻿using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface IPlayerRepository : IRepository<Player>
    {
        IQueryable<Player> WithEvents();
        IQueryable<Player> WithAnyEventsAndTeamOutOfFixturePlayers();
        IQueryable<Player> WithAnyYellowCardEvents();
        IQueryable<Player> FindAllByFixtureWithAnyYellowCard(Fixture fixture);
    }
}