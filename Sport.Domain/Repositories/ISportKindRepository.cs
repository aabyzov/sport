﻿using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface ISportKindRepository : IRepository<SportKind>
    {
    }
}