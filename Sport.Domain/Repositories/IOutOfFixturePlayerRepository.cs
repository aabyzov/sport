﻿using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface IOutOfFixturePlayerRepository : IRepository<OutOfFixturePlayer>
    {
    }
}