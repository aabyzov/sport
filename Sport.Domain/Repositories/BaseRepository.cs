﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Sport.Common.Core;
using Sport.Common.Extensions;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    /// <summary>
    /// 	Common behavior for repositories
    /// </summary>
    /// <typeparam name = "T"></typeparam>
    public abstract class BaseRepository<T, TContext> : IRepository<T>
        where T : class, IHasPrimaryKey
        where TContext : SportContext
    {
        #region Variables
        protected TContext DbContext { get; set; }
        #endregion

        protected BaseRepository() : this(null) { }

        protected BaseRepository(TContext context)
        {
            DbContext = context;
        }
        #region IRepository<T> Members

        /// <summary>
        /// 	Gets the enumerator of an object by type
        /// </summary>
        /// <param name = "type">The object type</param>
        /// <returns>The object enumerator</returns>
        public IEnumerator Get(Type type)
        {
            return DbContext.Database.SqlQuery(type, string.Empty, null).GetEnumerator();
        }

        public bool Lazy
        {
            get { return DbContext.Configuration.LazyLoadingEnabled; }
            set { DbContext.Configuration.LazyLoadingEnabled = value; }
        }

        public bool ProxyCreationEnabled
        {
            get { return DbContext.Configuration.ProxyCreationEnabled; }
            set { DbContext.Configuration.ProxyCreationEnabled = value; }
        }

        public int GetItemsCount()
        {
            return GetQueryable().Count();
        }

        public virtual T FindById(Guid id)
        {
            return GetQueryable().FirstOrDefault(t => t.Id == id);
        }

        public T FindOneBy(Expression<Func<T, bool>> filter)
        {
            return GetQueryable().Where(filter).OrderBy(t => t.Id).FirstOrDefault();
        }

        public void Add<TObject>(TObject obj) where TObject : class
        {
            DbContext.Set<TObject>().Add(obj);
        }

        public List<TObject> GetAllWhere<TObject>(Func<TObject, bool> func) where TObject : class
        {
            return DbContext.Set<TObject>().Where(func).ToList();
        }

        /// <summary>
        /// Saves the changes to the DB
        /// </summary>
        [Obsolete("Use UnitOfWork to commit changes to Db")]
        public void Save()
        {
            DbContext.SaveChanges();
        }

        public List<T> FindAllIncluding( Expression<Func<T, object>>[] includes = null, Expression<Func<T, bool>> filter = null)
        {
            IQueryable<T> query = GetQueryable();

            if (filter != null)
                query = query.Where(filter);

            if (includes != null && includes.Any())
            {
                foreach (var include in includes)
                {
                    query.Include(include);
                }
            }
                

            //query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
            //                         .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            //if (orderBy != null)
            //    return orderBy(query).Skip(pageIndex).Take(pageRecords).ToList();

            return query.OrderBy(t => t.Id).
                //Skip(pageIndex).Take(pageRecords).
                ToList();
        }

        public List<T> FindAllBy(Expression<Func<T, bool>> filter = null,
                                 Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                 int pageIndex = 0, int pageRecords = int.MaxValue, string includeProperties = "")
        {
            IQueryable<T> query = GetQueryable();

            if (filter != null)
                query = query.Where(filter);

            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                                     .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            if (orderBy != null)
                return orderBy(query).Skip(pageIndex).Take(pageRecords).ToList();

            return query.OrderBy(t => t.Id).Skip(pageIndex).Take(pageRecords).ToList();
        }

        public List<T> FindAllBy(List<KeyValuePair<string, string>> filtersKeyValuePairs, string orderDirection = "asc",
                                 string orderByColumnName = "Id", int pageIndex = 0, int pageRecords = int.MaxValue)
        {
            IQueryable<T> query = GetQueryable();

            int totalCount;

            return ProcessItemsList(query, filtersKeyValuePairs, out totalCount, pageRecords, pageIndex, orderDirection,
                                    orderByColumnName);
        }

        public void DeleteAll()
        {
            var dbSet = DbContext.Set<T>();
            foreach (var element in dbSet)
            {
                dbSet.Remove(element);
            }
        }

        public List<T> FindAll()
        {
            return GetQueryable().OrderBy(t => t.Id).ToList();
        }

        public List<T> FindAllSortedByUniqueField()
        {
            return GetSortOrder(GetQueryable()).ToList();
        }

        public List<T> FindAll(int pageIndex = 0, int pageRecords = 30)
        {
            return GetQueryable().OrderBy(t => t.Id).Skip(pageIndex).Take(pageRecords).ToList();
        }

        public virtual void Save(T item)
        {
            if (item.Id == Guid.Empty)
            {
                //item.Id = Guid.NewGuid();
                DbContext.Set<T>().Add(item);
            }
            else
            {
                DbContext.Set<T>().Attach(item);
                DbContext.Entry(item).State = EntityState.Modified;
            }

            var errors = CheckOnUnique(item);
            if (errors != null && errors.Count > 0)
            {
                //In ideal, this exception should never fired, because client code should call CheckOnUnique before to save.
                throw new ApplicationException(typeof(T).Name + " should be unique.");
            }
        }

        public virtual void Delete(T item)
        {
            DbContext.Set<T>().Remove(item);
        }

        public IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = GetQueryable();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public Dictionary<string, string> CheckOnUnique(T item)
        {
            if (SuppressUniqueValidation)
                return null;

            var entity = item as IUniqueEntity<T>;
            if (entity != null)
            {
                var result = new Dictionary<string, string>();
                foreach (EntityValidationHelper<T> entityValidation in entity.CheckForUnique())
                {
                    IQueryable<T> query = DbContext.Set<T>().Where(entityValidation.ValidationExpression);
                    if (item.Id != Guid.Empty)
                        query = query.Where(m => m.Id != item.Id);
                    var count = query.Count();

                    if (count > 0)
                    {
                        foreach (var key in entityValidation.Errors)
                        {
                            result.Add(key.Key, key.Value);
                        }
                    }
                }
                return result;
            }
            return null;
        }

        public Dictionary<string, string> CheckOnUnique(List<T> items, T item)
        {
            if (SuppressUniqueValidation)
                return null;

            var entity = item as IUniqueEntity<T>;
            if (entity != null)
            {
                var result = new Dictionary<string, string>();
                foreach (EntityValidationHelper<T> entityValidation in entity.CheckForUnique())
                {
                    IEnumerable<T> query = items.Where(entityValidation.ValidationExpression.Compile());
                    if (item.Id != Guid.Empty)
                        query = query.Where(m => m.Id != item.Id);
                    var count = query.Count();

                    if (count > 0)
                    {
                        foreach (var key in entityValidation.Errors)
                        {
                            result.Add(key.Key, key.Value);
                        }
                    }
                }
                return result;
            }
            return null;
        }

        public void Update(T toUpdate, T current)
        {
            DbContext.Entry(toUpdate).CurrentValues.SetValues(current);
        }

        public T FindByMigrationId(Guid migrationId)
        {
            if (typeof(T).GetInterface("IHasMigrationId") != null)
            {
                var result = (GetQueryable() as IQueryable<IHasMigrationId>).FirstOrDefault(m => m.MigrationId == migrationId);
                return result as T;
            }
            throw new NotImplementedException(typeof(T).Name + " should implement IHasExternalId.");
        }

        public bool Any(Expression<Func<T, bool>> Filter)
        {
            return GetQueryable().Any(Filter);
        }

        public bool Any()
        {
            return GetQueryable().Any();
        }

        #endregion

        #region Helper methods

        public virtual IQueryable<T> GetQueryable()
        {
            return DbContext.Set<T>().AsQueryable();
        }

        protected virtual IQueryable<T> GetSortOrder(IQueryable<T> queryable)
        {
            return queryable.OrderBy(m => m.Id);
        }

        protected bool SuppressUniqueValidation { get; set; }
        #endregion

        #region Generic methods to retrieve all or single items from tables.

        /// <summary>
        /// 	Gets all the modified members info of an entity
        /// </summary>
        /// <typeparam name = "T">The table type</typeparam>
        /// <param name = "items">The item list to be processed, as IQueryable</param>
        /// <param name = "filtersKeyValuePairs">The filters</param>
        /// <param name = "totalCount">The total nr of items</param>
        /// <param name = "count">The nr of elements to retrieve</param>
        /// <param name = "position">The start position</param>
        /// <param name = "orderDirection">The order by direction</param>
        /// <param name = "orderByColumnName">The order by column name</param>
        /// <returns>All the modified members info of an entity</returns>
        /// <summary>
        /// 	Adds an entity to the DB
        /// </summary>
        /// <typeparam name = "T">The table type</typeparam>
        /// <summary>
        /// 	Processes the items list (filtering, ordering)
        /// </summary>
        /// <typeparam name = "T">The type of the items in the list</typeparam>
        /// <returns>Processed items list</returns>
        internal static List<T> ProcessItemsList(IQueryable<T> items,
                                                 List<KeyValuePair<string, string>> filtersKeyValuePairs,
                                                 out int totalCount,
                                                 int count = 30, int position = 0, string orderDirection = "asc",
                                                 string orderByColumnName = "Id")
        {
            items = Filter(filtersKeyValuePairs, items);
            totalCount = items.Count();

            return
                items.OrderBy(orderByColumnName + "|" + orderDirection + ",Id|desc").Skip(position).Take(count).ToList();
        }

        /// <summary>
        /// 	Processes the items list (filtering, ordering)
        /// </summary>
        /// <typeparam name = "T">The type of the items in the list</typeparam>
        /// <param name = "items">The item list to be processed as IEnumberable</param>
        /// <param name = "position">The start position</param>
        /// <param name = "count">The nr of elements to retrieve</param>
        /// <param name = "orderDirection">The order by direction</param>
        /// <param name = "orderByColumnName">The order by column name</param>
        /// <param name = "filtersKeyValuePairs">The filters</param>
        /// <param name = "totalCount">The total nr of items</param>
        /// <returns>Processed items list</returns>
        internal List<T> ProcessItemsList(IEnumerable<T> items, List<KeyValuePair<string, string>> filtersKeyValuePairs,
                                          out int totalCount,
                                          int count = 30, int position = 0, string orderDirection = "asc",
                                          string orderByColumnName = "Id")
        {
            items = Filter(filtersKeyValuePairs, items);
            totalCount = items.Count();

            return items.OrderBy(orderByColumnName + "|" + orderDirection + ",Id|desc").Skip(position).Take(count).ToList();
        }

        #endregion

        #region Sorting/Filtering

        /// <summary>
        /// 	Filters a collection after the given filters
        /// </summary>
        /// <typeparam name = "T">The type of the elements in the collection</typeparam>
        /// <param name = "filtersKeyValuePairs">a collection of filterColumn - filterValue pair</param>
        /// <param name = "collection">The collection to be filtered</param>
        /// <returns>The filtered collection</returns>
        public static IQueryable<T> Filter(List<KeyValuePair<string, string>> filtersKeyValuePairs,
                                           IQueryable<T> collection)
        {
            //filters were applied
            if (filtersKeyValuePairs.Count > 0)
            {
                foreach (var filter in filtersKeyValuePairs)
                {
                    collection = collection.Where(Sport.Common.Extensions.Filter.From<T>(filter.Key, filter.Value));
                }
            }

            return collection;
        }

        public static IEnumerable<T> Filter(List<KeyValuePair<string, string>> filtersKeyValuePairs,
                                            IEnumerable<T> collection)
        {
            //filters were applied
            if (filtersKeyValuePairs.Count > 0)
            {
                foreach (var filter in filtersKeyValuePairs)
                {
                    collection = collection.Where(Sport.Common.Extensions.Filter.From<T>(filter.Key, filter.Value).Compile());
                }
            }

            return collection;
        }

        #endregion
    }
}