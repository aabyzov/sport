﻿using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class TeamRepository : BaseRepository<Team, SportContext>, ITeamRepository
    {
        public TeamRepository(SportContext db)
        {
            DbContext = db;
        }

        public IQueryable<Team> WithLeague()
        {
            return GetQueryable().Include(x => x.League);
        }

        public IQueryable<Team> WithFixtures()
        {
            return
                GetQueryable().Include(x => x.GroupTeams.Select(gt => gt.FixtureParticipants.Select(fp => fp.Fixture)));
        }

        public IQueryable<Team> WithFixturesAndTeams()
        {
            return
                GetQueryable().Include(x => x.GroupTeams.Select(gt => gt.FixtureParticipants.Select(fp => fp.Fixture)))

                .Include(x => x.GroupTeams.Select(gt => gt.Team))
                .Include(x => x.GroupTeams.Select(gt => gt.Group.Stage.Champ));
        }

        public IQueryable<Team> WithEventsAndTeams()
        {
            return
                GetQueryable().Include(x => x.GroupTeams.Select(gt => gt.FixtureParticipants.Select(fp => fp.Fixture)))
                .Include(x => x.GroupTeams.Select(gt => gt.FixtureParticipants.Select(fp => fp.Events)))
                .Include(x => x.GroupTeams.Select(gt => gt.FixtureParticipants.Select(fp => fp.OutOfFixturePlayers)))
                .Include(x => x.GroupTeams.Select(gt => gt.Team))
                .Include(x => x.GroupTeams.Select(gt => gt.Group.Stage.Champ));
        }

        public IQueryable<Team> WithFixturesAndChamp()
        {
            return
                WithFixtures().Include(x => x.GroupTeams.Select(gt => gt.Group.Stage.Champ));
        } 


        public IQueryable<Team> GetByChamp(Champ champ)
        {
            var stages = DbContext.Stages.Where(x => x.ChampId == champ.Id);

            var groups = stages.SelectMany(x => x.Groups);
            var groupTeams = groups.SelectMany(x => x.GroupTeams);
            var teams = groupTeams.Select(x => x.Team);
            return teams.AsQueryable();
        }

        public override void Save(Team item)
        {
            var existingTeam = DbContext.Teams.FirstOrDefault(x => x.Name == item.Name);
            if (existingTeam == null)
            {
                base.Save(item);
                return;
            }
            throw new ValidationException(string.Format("There is already a team with the name \"{0}\"", item.Name));
        }
    }
}