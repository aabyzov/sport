﻿using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class OutOfFixturePlayerRepository : BaseRepository<OutOfFixturePlayer, SportContext>, IOutOfFixturePlayerRepository
    {
        public OutOfFixturePlayerRepository(SportContext db)
        {
            DbContext = db;
        }


    }
}