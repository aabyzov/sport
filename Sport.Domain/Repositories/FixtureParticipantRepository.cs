﻿using System.Data.Entity;
using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class FixtureParticipantRepository : BaseRepository<FixtureParticipant, SportContext>, IFixtureParticipantRepository
    {
        public FixtureParticipantRepository(SportContext db)
        {
            DbContext = db;
        }

        public override IQueryable<FixtureParticipant> GetQueryable()
        {
            return DbContext.FixtureParticipants.AsQueryable().Include(x => x.Fixture.FixtureParticipants).Include(x => x.GroupTeam);
        }
    }
}