﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface IStageRepository : IRepository<Stage>
    {
        bool IsPlayed(Stage stage);
        IQueryable<Stage> FindBy(Champ champ);
        IQueryable<Fixture> FindFixturesBy(Stage stage);
    }
}