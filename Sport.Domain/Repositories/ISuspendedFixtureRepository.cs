﻿using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface ISuspendedFixtureRepository : IRepository<SuspendedFixture>
    {
    }
}