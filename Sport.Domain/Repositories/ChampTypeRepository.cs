﻿using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class ChampTypeRepository : BaseRepository<ChampType, SportContext>, IChampTypeRepository
    {
        public ChampTypeRepository(SportContext db)
        {
            DbContext = db;
        }
    }

    public interface IChampTypeRepository : IRepository<ChampType>
    {
    }
}