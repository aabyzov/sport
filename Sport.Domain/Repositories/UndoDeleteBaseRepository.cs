﻿using System.Linq;
using Sport.Common.Extensions;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
	public abstract class UndoDeleteBaseRepository<T, TContext> : BaseRepository<T, TContext>, IUndoDeletedRepository<T> 
		where T : class, IHasPrimaryKey, IUndoDeletedSupport
		where TContext : SportContext
	{
		#region IUndoDeletedRepository Members
		public bool ShowRemoved { get; set; } 

		public bool ShowAll { get; set; } 

		public void Restore(T item)
		{
            bool originalValue = SuppressUniqueValidation;
			try
			{
				SuppressUniqueValidation = true;
				item.Unused = false;
				Save(item);
			}
			finally
			{
                SuppressUniqueValidation = originalValue;
			}
		}
		#endregion

		#region Override methods
		/// <summary>
		/// Sets the Unused flag to true.
		/// </summary>
		/// <param name="item"></param>
		public override void Delete(T item)
		{
		    bool originalValue = SuppressUniqueValidation;
			try
			{
				SuppressUniqueValidation = true;
				item.Unused = true;
				Save(item);
			}
			finally
			{
                SuppressUniqueValidation = originalValue;
			}
		}

	    public override IQueryable<T> GetQueryable()
		{
			var result = DbContext.Set<T>().AsQueryable();
            if (!ShowAll)
			    result = result.Where(r => r.Unused == ShowRemoved);
			return result;
		} 
		#endregion
	}
}
