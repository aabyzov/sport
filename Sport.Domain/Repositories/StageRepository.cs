﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class StageRepository : BaseRepository<Stage, SportContext>, IStageRepository
    {
        public StageRepository(SportContext db)
        {
            DbContext = db;
        }

        public override IQueryable<Stage> GetQueryable()
        {
            return base.GetQueryable().Include(x => x.Champ);
        }

        public IQueryable<Stage> FindBy(Champ champ)
        {
            return GetQueryable().Where(x => x.ChampId == champ.Id);
        }

        public IQueryable<Fixture> FindFixturesBy(Stage stage)
        {
            var groups = DbContext.Groups.Where(x => x.StageId == stage.Id);
            var fixtures = groups.SelectMany(group1 => DbContext.GroupTeams.Where(x => x.GroupId == group1.Id)).
                SelectMany(team => DbContext.FixtureParticipants.Where(x => x.GroupTeamId == team.Id && x.IsHome), (team, participant) => participant.Fixture);
            return fixtures.AsQueryable();
        }

        public bool IsPlayed(Stage stage)
        {
            var groups = DbContext.Groups.Where(x => x.StageId == stage.Id);
            var fixtures = groups.SelectMany(group => DbContext.GroupTeams.Where(x => x.GroupId == group.Id).
                                                 SelectMany(
                                                     team => DbContext.FixtureParticipants.Where(
                                                         x => x.GroupTeamId == team.Id && x.IsHome)).
                                                 Select(participant => participant.Fixture)).ToList();
            return fixtures.Count() == fixtures.Count(x => x.IsPlayed);
        }
    }
}