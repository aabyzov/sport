﻿using System;
using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface IFixtureRepository : IRepository<Fixture>
    {
        IQueryable<Fixture> FindAllWhereChampNameStartWith(string champName);
        IQueryable<Fixture> FindAllByGroupId(Guid groupId);
        IQueryable<Fixture> WithEvents();
        IQueryable<Fixture> WithEventsAndTeams();
        IQueryable<Fixture> FindAllByGroupIdWithEventsAndTeams(Guid groupId);
        IQueryable<Fixture> FindAllByChampIdWithEventsAndTeams(Guid champId);
        IQueryable<Fixture> FindAllByChampId(Guid champId);
    }
}