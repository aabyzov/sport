﻿using System.Data.Entity;
using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class SuspensionRepository : BaseRepository<Suspension, SportContext>, ISuspensionRepository
    {
        public SuspensionRepository(SportContext db)
        {
            DbContext = db;
        }

        public override IQueryable<Suspension> GetQueryable()
        {
            return base.GetQueryable().Include(x => x.FixtureEvent.Player);
        }

        public IQueryable<Suspension> FindAllActiveBy(Fixture fixture, Player player)
        {
            return GetQueryable().Where(x => x.FixtureEvent.PlayerId == player.Id && x.MissedMatchCount > 0 && x.SuspendedFixtures.All(suspendedFixture => suspendedFixture.FixtureId != fixture.Id));
        } 
    }
}