﻿using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface IFixtureParticipantRepository : IRepository<FixtureParticipant>
    {
    }
}