﻿using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public interface IChampRepository : IRepository<Champ>
    {
        Champ WorldCup2006FinalsWithFixtures();
        Champ WorldCup2006FinalsWithTeams();
        IQueryable<Champ> WorldCup2006WithQualificationWithTeams();
        IQueryable<Team> GetWorldCup2006Teams();
        IQueryable<Champ> WithFixturesAndTeams();
        Champ[] FindAllOrderedByStartDate();
    }
}