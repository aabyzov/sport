﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Sport.Domain.Models;
using Sport.Domain.Repositories.IQueryable;

namespace Sport.Domain.Repositories
{
    public class FixtureRepository : BaseRepository<Fixture, SportContext>, IFixtureRepository
    {
        //private readonly Func<Fixture, Guid, bool> _byChampId = (x, champId) => x.FixtureParticipants.FirstOrDefault().GroupTeam.Group.Stage.ChampId == champId;
        //private readonly Func<Fixture, Guid, bool> _byGroupId = (x, groupId) => x.FixtureParticipants.FirstOrDefault().GroupTeam.Group.Id == groupId;

        public FixtureRepository(SportContext db)
        {
            DbContext = db;
        }

        public override IQueryable<Fixture> GetQueryable()
        {
            return DbContext.Fixtures.AsQueryable().Include(x => x.FixtureParticipants).OrderBy(x => x.Date);
        }

        public IQueryable<Fixture> WithEvents()
        {
            return DbContext.Fixtures.Include(x => x.FixtureParticipants.Select(fp => fp.Events));
        }

        public IQueryable<Fixture> WithEventsAndTeams()
        {
            return DbContext.Fixtures.Include(x => x.FixtureParticipants.Select(fp => fp.Events.Select(fev => fev.EventTime)))
                .Include(x => x.FixtureParticipants.Select(fp => fp.Events.Select(fev => fev.FixtureParticipant.OutOfFixturePlayers)))
                .Include(x => x.FixtureParticipants.Select(fp => fp.Events.Select(fpe => fpe.Player)))
                .Include(x => x.FixtureParticipants.Select(fp => fp.GroupTeam.Group.Stage.Champ.ChampSeason))
                .Include(x => x.FixtureParticipants.Select(fp => fp.GroupTeam.Team))
                .Include(x => x.FixtureParticipants.Select(fp => fp.OutOfFixturePlayers.Select(ofp => ofp.Player)));
        }

        public override Fixture FindById(System.Guid id)
        {
            return base.FindById(id);
        }

        public IQueryable<Fixture> FindAllByGroupId(Guid groupId)
        {
            return GetQueryable().ByGroupId(groupId);
        }

        public IQueryable<Fixture> FindAllByChampId(Guid champId)
        {
            // Can't refactor it with simplier Func (like field func<Fixture,int,bool> coz of "Linq not supported" exception
            return GetQueryable().ByChampId(champId);
        }

        public IQueryable<Fixture> FindAllByGroupIdWithEventsAndTeams(Guid groupId)
        {
            return WithEventsAndTeams().ByGroupId(groupId);
        }

        public IQueryable<Fixture> FindAllByChampIdWithEventsAndTeams(Guid champId)
        {
            return WithEventsAndTeams().ByChampId(champId);
        }

        public IQueryable<Fixture> FindAllWhereChampNameStartWith(string champName)
        {
            return
                GetQueryable()
                    .Where(
                        x =>
                        x.FixtureParticipants.FirstOrDefault().GroupTeam.Group.Stage.Champ.Name.StartsWith(champName));
        }
    }
}