﻿using System.Data.Entity;
using System.Linq;
using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class GroupTeamRepository : BaseRepository<GroupTeam, SportContext>, IGroupTeamRepository
    {
        public GroupTeamRepository(SportContext db)
        {
            DbContext = db;
        }

        public override IQueryable<GroupTeam> GetQueryable()
        {
            return base.GetQueryable().Include(x => x.Team);
        }

        //public IQueryable<GroupTeam> IncludeFixtures()
        //{
            
        //}
    }
}