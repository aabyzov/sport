﻿using Sport.Domain.Models;

namespace Sport.Domain.Repositories
{
    public class ZoneRepository : BaseRepository<Zone, SportContext>, IZoneRepository
    {
        public ZoneRepository(SportContext db)
        {
            DbContext = db;
        }
    }

    public interface IZoneRepository : IRepository<Zone>
    {
    }
}