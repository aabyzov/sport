﻿using Sport.Common.Core;
using Sport.Domain.Models;
using Sport.Domain.Repositories;
using Sport.Domain.UoW;
using StructureMap;

namespace Sport.Domain.IoC
{
    public static class CompositionRoot
    {
        public static void Initialize()
        {
            ObjectFactory.Initialize(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.AssemblyContainingType(typeof(SportKindRepository));
                    scan.AddAllTypesOf(typeof(IRepository<>));
                    scan.ConnectImplementationsToTypesClosing(typeof(IRepository<>));
                });
                x.For<SportContext>().HybridHttpOrThreadLocalScoped().Use(new SportContext());
                x.For<ISportUnitOfWork>().HybridHttpOrThreadLocalScoped().Use<SportUnitOfWork>();
                x.For<ILogger>().Singleton().Use(new Logger("test", "pref"));
            });
            //return ObjectFactory.Container;
        }
    }
}