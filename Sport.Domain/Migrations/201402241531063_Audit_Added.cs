namespace Sport.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Audit_Added : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AuditRecordFields",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        AuditRecordId = c.Int(nullable: false),
                        MemberName = c.String(),
                        OldValue = c.String(),
                        NewValue = c.String(),
                        AuditRecord_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AuditRecords", t => t.AuditRecord_Id)
                .Index(t => t.AuditRecord_Id);
            
            CreateTable(
                "dbo.AuditRecords",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Action = c.Byte(nullable: false),
                        EntityTable = c.String(),
                        EntityNamespace = c.String(),
                        EntityTableKey = c.Guid(nullable: false),
                        AssociationTable = c.String(),
                        AssociationTableKey = c.Guid(),
                        AuditDate = c.DateTime(nullable: false),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FullName = c.String(nullable: false),
                        UserName = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Abbrev = c.String(maxLength: 10),
                        Enabled = c.Boolean(nullable: false),
                        Pwd = c.String(),
                        PwdToken = c.Guid(),
                        PwdEmailDate = c.DateTime(),
                        version = c.Int(),
                        Deleted = c.DateTime(),
                        Timestamp = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        IsActiveDirectoryUser = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AuditRecordFields", "AuditRecord_Id", "dbo.AuditRecords");
            DropIndex("dbo.AuditRecordFields", new[] { "AuditRecord_Id" });
            DropTable("dbo.Users");
            DropTable("dbo.AuditRecords");
            DropTable("dbo.AuditRecordFields");
        }
    }
}
