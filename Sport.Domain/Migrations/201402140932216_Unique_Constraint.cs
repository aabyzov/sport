namespace Sport.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Unique_Constraint : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SportKinds", "UniqueId", c => c.Int(nullable: false));
            CreateIndex("dbo.SportKinds", "UniqueId", unique:true, name:"IX_UniqueId");
        }
        
        public override void Down()
        {
            DropColumn("dbo.SportKinds", "UniqueId");
            DropIndex("dbo.SportKinds", "IX_UniqueId");
        }
    }
}
