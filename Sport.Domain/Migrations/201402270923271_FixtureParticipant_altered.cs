namespace Sport.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixtureParticipant_altered : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FixtureParticipants", "Fixture_Id", "dbo.Fixtures");
            DropIndex("dbo.FixtureParticipants", new[] { "Fixture_Id" });
            AddColumn("dbo.FixtureParticipants", "FixtureId", c => c.Guid(nullable: false));
            CreateIndex("dbo.FixtureParticipants", "FixtureId");
            AddForeignKey("dbo.FixtureParticipants", "FixtureId", "dbo.Fixtures", "Id", cascadeDelete: false);
            DropColumn("dbo.FixtureParticipants", "Fixture_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FixtureParticipants", "Fixture_Id", c => c.Guid());
            DropForeignKey("dbo.FixtureParticipants", "FixtureId", "dbo.Fixtures");
            DropIndex("dbo.FixtureParticipants", new[] { "FixtureId" });
            DropColumn("dbo.FixtureParticipants", "FixtureId");
            CreateIndex("dbo.FixtureParticipants", "Fixture_Id");
            AddForeignKey("dbo.FixtureParticipants", "Fixture_Id", "dbo.Fixtures", "Id");
        }
    }
}
