namespace Sport.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SuspensionFieldsUpdated : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Suspensions", "AlreadyMissedMatches");
            DropColumn("dbo.Suspensions", "IsFinished");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Suspensions", "IsFinished", c => c.Boolean(nullable: false));
            AddColumn("dbo.Suspensions", "AlreadyMissedMatches", c => c.Int(nullable: false));
        }
    }
}
