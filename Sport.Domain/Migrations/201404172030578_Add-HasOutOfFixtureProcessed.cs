namespace Sport.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHasOutOfFixtureProcessed : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Fixtures", "HasOutOfFixtureProcessed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Fixtures", "HasOutOfFixtureProcessed");
        }
    }
}
