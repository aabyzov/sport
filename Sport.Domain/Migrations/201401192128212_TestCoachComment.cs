namespace Sport.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestCoachComment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Fixtures", "CoachComment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Fixtures", "CoachComment");
        }
    }
}
