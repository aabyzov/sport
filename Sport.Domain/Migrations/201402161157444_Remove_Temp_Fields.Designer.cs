// <auto-generated />
namespace Sport.Domain.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.1-21010")]
    public sealed partial class Remove_Temp_Fields : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Remove_Temp_Fields));
        
        string IMigrationMetadata.Id
        {
            get { return "201402161157444_Remove_Temp_Fields"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
