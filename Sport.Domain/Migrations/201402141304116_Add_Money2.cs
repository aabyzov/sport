namespace Sport.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Money2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SportKinds", "Money_Value", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SportKinds", "Money_CurrencyId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SportKinds", "Money_CurrencyId");
            DropColumn("dbo.SportKinds", "Money_Value");
        }
    }
}
