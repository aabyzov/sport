namespace Sport.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMatchDay : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Fixtures", "MatchDay", c => c.Int(nullable: false, defaultValue:0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Fixtures", "MatchDay");
        }
    }
}
