namespace Sport.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PreparingForRelease1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SuspendedFixtures",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        SuspensionId = c.Guid(nullable: false),
                        FixtureId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fixtures", t => t.FixtureId, cascadeDelete: true)
                .ForeignKey("dbo.Suspensions", t => t.SuspensionId, cascadeDelete: true)
                .Index(t => t.FixtureId)
                .Index(t => t.SuspensionId);
            
            CreateTable(
                "dbo.Suspensions",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        MissedMatchCount = c.Int(nullable: false),
                        AlreadyMissedMatches = c.Int(nullable: false),
                        FixtureEventId = c.Guid(nullable: false),
                        Start = c.DateTime(),
                        End = c.DateTime(),
                        IsFinished = c.Boolean(nullable: false),
                        Reason = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FixtureEvents", t => t.FixtureEventId, cascadeDelete: true)
                .Index(t => t.FixtureEventId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SuspendedFixtures", "SuspensionId", "dbo.Suspensions");
            DropForeignKey("dbo.Suspensions", "FixtureEventId", "dbo.FixtureEvents");
            DropForeignKey("dbo.SuspendedFixtures", "FixtureId", "dbo.Fixtures");
            DropIndex("dbo.SuspendedFixtures", new[] { "SuspensionId" });
            DropIndex("dbo.Suspensions", new[] { "FixtureEventId" });
            DropIndex("dbo.SuspendedFixtures", new[] { "FixtureId" });
            DropTable("dbo.Suspensions");
            DropTable("dbo.SuspendedFixtures");
        }
    }
}
