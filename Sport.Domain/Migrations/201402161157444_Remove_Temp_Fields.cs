namespace Sport.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_Temp_Fields : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.SportKinds", "IX_UniqueId");
            DropColumn("dbo.SportKinds", "UniqueId");
            DropColumn("dbo.SportKinds", "Money_Value");
            DropColumn("dbo.SportKinds", "Money_CurrencyId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SportKinds", "Money_CurrencyId", c => c.String());
            AddColumn("dbo.SportKinds", "Money_Value", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SportKinds", "UniqueId", c => c.Int(nullable: false));
            CreateIndex("dbo.SportKinds", "UniqueId", unique: true, name: "IX_UniqueId");
        }
    }
}
