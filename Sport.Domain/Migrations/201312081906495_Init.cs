namespace Sport.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Champs",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        ChampSeasonId = c.Guid(),
                        MigrationId = c.Guid(nullable: false),
                        ZoneId = c.Guid(),
                        YellowsToMissNextMatch = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ChampSeasons", t => t.ChampSeasonId)
                .ForeignKey("dbo.Zones", t => t.ZoneId)
                .Index(t => t.ChampSeasonId)
                .Index(t => t.ZoneId);
            
            CreateTable(
                "dbo.ChampSeasons",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        ChampTypeId = c.Guid(nullable: false),
                        ChampSeasonId = c.Guid(),
                        MigrationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ChampTypes", t => t.ChampTypeId, cascadeDelete: true)
                .ForeignKey("dbo.ChampSeasons", t => t.ChampSeasonId)
                .Index(t => t.ChampTypeId)
                .Index(t => t.ChampSeasonId);
            
            CreateTable(
                "dbo.ChampTypes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        SportKindId = c.Guid(nullable: false),
                        MigrationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SportKinds", t => t.SportKindId, cascadeDelete: true)
                .Index(t => t.SportKindId);
            
            CreateTable(
                "dbo.SportKinds",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Stages",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        RoundCount = c.Int(nullable: false),
                        ChampId = c.Guid(nullable: false),
                        StageId = c.Guid(),
                        MigrationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Champs", t => t.ChampId, cascadeDelete: true)
                .ForeignKey("dbo.Stages", t => t.StageId)
                .Index(t => t.ChampId)
                .Index(t => t.StageId);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        StageId = c.Guid(nullable: false),
                        MigrationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Stages", t => t.StageId, cascadeDelete: true)
                .Index(t => t.StageId);
            
            CreateTable(
                "dbo.GroupTeams",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        TeamId = c.Guid(nullable: false),
                        GroupId = c.Guid(nullable: false),
                        MigrationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Teams", t => t.TeamId, cascadeDelete: true)
                .ForeignKey("dbo.Groups", t => t.GroupId, cascadeDelete: true)
                .Index(t => t.TeamId)
                .Index(t => t.GroupId);
            
            CreateTable(
                "dbo.EventTimes",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Second = c.Int(),
                        FirstHalfMinute = c.Int(),
                        SecondHalfMinute = c.Int(),
                        OverTimeFirstHalfMinute = c.Int(),
                        OverTimeSecondHalfMinute = c.Int(),
                        CompensatedMinute = c.Int(),
                        Minute = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Players",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        MigrationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TeamMembers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        TeamId = c.Guid(nullable: false),
                        PositionNumber = c.Int(),
                        JerseyNumber = c.Int(),
                        YearContractEnds = c.Int(),
                        PlayerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("dbo.Teams", t => t.TeamId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.TeamId);
            
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        Chemistry = c.Int(),
                        OverallRating = c.Int(),
                        LeagueId = c.Guid(nullable: false),
                        MigrationId = c.Guid(nullable: false),
                        ZoneId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Leagues", t => t.LeagueId, cascadeDelete: true)
                .ForeignKey("dbo.Zones", t => t.ZoneId, cascadeDelete: true)
                .Index(t => t.LeagueId)
                .Index(t => t.ZoneId);
            
            CreateTable(
                "dbo.Leagues",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        SportKindId = c.Guid(nullable: false),
                        MigrationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SportKinds", t => t.SportKindId, cascadeDelete: true)
                .Index(t => t.SportKindId);
            
            CreateTable(
                "dbo.Zones",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                        MigrationEnumId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FixtureEvents",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        EventOrder = c.Int(),
                        PlayerId = c.Guid(nullable: false),
                        FixtureParticipantId = c.Guid(nullable: false),
                        AssistantId = c.Guid(),
                        HowScored = c.Int(),
                        BodyPart = c.Int(),
                        GoalDistance = c.Int(),
                        MissedMatchCountInjury = c.Int(),
                        RecoveryDate = c.DateTime(),
                        Details = c.Int(),
                        AssistantId1 = c.Guid(),
                        MissedMatchCountRedCard = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        EventTime_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EventTimes", t => t.EventTime_Id)
                .ForeignKey("dbo.FixtureParticipants", t => t.FixtureParticipantId, cascadeDelete: true)
                .ForeignKey("dbo.Players", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("dbo.Players", t => t.AssistantId)
                .ForeignKey("dbo.Players", t => t.AssistantId1)
                .Index(t => t.EventTime_Id)
                .Index(t => t.FixtureParticipantId)
                .Index(t => t.PlayerId)
                .Index(t => t.AssistantId)
                .Index(t => t.AssistantId1);
            
            CreateTable(
                "dbo.Fixtures",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Date = c.DateTime(),
                        Location = c.String(),
                        IsFriendly = c.Boolean(nullable: false),
                        IsPlayed = c.Boolean(nullable: false),
                        MigrationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OutOfFixturePlayers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        PlayerId = c.Guid(nullable: false),
                        FixtureParticipantId = c.Guid(nullable: false),
                        MissedMatchCount = c.Int(nullable: false),
                        Reason = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FixtureParticipants", t => t.FixtureParticipantId, cascadeDelete: true)
                .ForeignKey("dbo.Players", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.FixtureParticipantId)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "dbo.Positions",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SquadPlayers",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        PlayerId = c.Guid(nullable: false),
                        FixtureParticipantId = c.Guid(nullable: false),
                        PositionId = c.Guid(nullable: false),
                        IsSubstitution = c.Boolean(),
                        QuarterNumber = c.Int(),
                        PlayerInQuarterNumber = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FixtureParticipants", t => t.FixtureParticipantId, cascadeDelete: true)
                .ForeignKey("dbo.Players", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("dbo.Positions", t => t.PositionId, cascadeDelete: true)
                .Index(t => t.FixtureParticipantId)
                .Index(t => t.PlayerId)
                .Index(t => t.PositionId);
            
            CreateTable(
                "dbo.FixtureParticipants",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Score = c.Int(),
                        IsHome = c.Boolean(nullable: false),
                        GroupTeamId = c.Guid(nullable: false),
                        HalfTimeScore = c.Int(),
                        OverTimeHalfScore = c.Int(),
                        OverTimeScore = c.Int(),
                        PenaltyScore = c.Int(),
                        Fouls = c.Int(),
                        TenMetresPenalty = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Fixture_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Fixtures", t => t.Fixture_Id)
                .ForeignKey("dbo.GroupTeams", t => t.GroupTeamId, cascadeDelete: true)
                .Index(t => t.Fixture_Id)
                .Index(t => t.GroupTeamId);
            
            CreateTable(
                "dbo.PlayerRetireHistories",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        IsRetire = c.Boolean(nullable: false),
                        Date = c.DateTime(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.PlayerId, cascadeDelete: true)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "dbo.TeamHistories",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TeamMemberHistories",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        TeamId = c.Guid(nullable: false),
                        PlayerId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Players", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("dbo.Teams", t => t.TeamId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.TeamId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeamMemberHistories", "TeamId", "dbo.Teams");
            DropForeignKey("dbo.TeamMemberHistories", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.PlayerRetireHistories", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.Champs", "ZoneId", "dbo.Zones");
            DropForeignKey("dbo.Stages", "StageId", "dbo.Stages");
            DropForeignKey("dbo.Groups", "StageId", "dbo.Stages");
            DropForeignKey("dbo.GroupTeams", "GroupId", "dbo.Groups");
            DropForeignKey("dbo.SquadPlayers", "PositionId", "dbo.Positions");
            DropForeignKey("dbo.SquadPlayers", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.SquadPlayers", "FixtureParticipantId", "dbo.FixtureParticipants");
            DropForeignKey("dbo.OutOfFixturePlayers", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.OutOfFixturePlayers", "FixtureParticipantId", "dbo.FixtureParticipants");
            DropForeignKey("dbo.FixtureParticipants", "GroupTeamId", "dbo.GroupTeams");
            DropForeignKey("dbo.FixtureParticipants", "Fixture_Id", "dbo.Fixtures");
            DropForeignKey("dbo.FixtureEvents", "AssistantId1", "dbo.Players");
            DropForeignKey("dbo.FixtureEvents", "AssistantId", "dbo.Players");
            DropForeignKey("dbo.FixtureEvents", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.Teams", "ZoneId", "dbo.Zones");
            DropForeignKey("dbo.TeamMembers", "TeamId", "dbo.Teams");
            DropForeignKey("dbo.Teams", "LeagueId", "dbo.Leagues");
            DropForeignKey("dbo.Leagues", "SportKindId", "dbo.SportKinds");
            DropForeignKey("dbo.GroupTeams", "TeamId", "dbo.Teams");
            DropForeignKey("dbo.TeamMembers", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.FixtureEvents", "FixtureParticipantId", "dbo.FixtureParticipants");
            DropForeignKey("dbo.FixtureEvents", "EventTime_Id", "dbo.EventTimes");
            DropForeignKey("dbo.Stages", "ChampId", "dbo.Champs");
            DropForeignKey("dbo.ChampSeasons", "ChampSeasonId", "dbo.ChampSeasons");
            DropForeignKey("dbo.ChampSeasons", "ChampTypeId", "dbo.ChampTypes");
            DropForeignKey("dbo.ChampTypes", "SportKindId", "dbo.SportKinds");
            DropForeignKey("dbo.Champs", "ChampSeasonId", "dbo.ChampSeasons");
            DropIndex("dbo.TeamMemberHistories", new[] { "TeamId" });
            DropIndex("dbo.TeamMemberHistories", new[] { "PlayerId" });
            DropIndex("dbo.PlayerRetireHistories", new[] { "PlayerId" });
            DropIndex("dbo.Champs", new[] { "ZoneId" });
            DropIndex("dbo.Stages", new[] { "StageId" });
            DropIndex("dbo.Groups", new[] { "StageId" });
            DropIndex("dbo.GroupTeams", new[] { "GroupId" });
            DropIndex("dbo.SquadPlayers", new[] { "PositionId" });
            DropIndex("dbo.SquadPlayers", new[] { "PlayerId" });
            DropIndex("dbo.SquadPlayers", new[] { "FixtureParticipantId" });
            DropIndex("dbo.OutOfFixturePlayers", new[] { "PlayerId" });
            DropIndex("dbo.OutOfFixturePlayers", new[] { "FixtureParticipantId" });
            DropIndex("dbo.FixtureParticipants", new[] { "GroupTeamId" });
            DropIndex("dbo.FixtureParticipants", new[] { "Fixture_Id" });
            DropIndex("dbo.FixtureEvents", new[] { "AssistantId1" });
            DropIndex("dbo.FixtureEvents", new[] { "AssistantId" });
            DropIndex("dbo.FixtureEvents", new[] { "PlayerId" });
            DropIndex("dbo.Teams", new[] { "ZoneId" });
            DropIndex("dbo.TeamMembers", new[] { "TeamId" });
            DropIndex("dbo.Teams", new[] { "LeagueId" });
            DropIndex("dbo.Leagues", new[] { "SportKindId" });
            DropIndex("dbo.GroupTeams", new[] { "TeamId" });
            DropIndex("dbo.TeamMembers", new[] { "PlayerId" });
            DropIndex("dbo.FixtureEvents", new[] { "FixtureParticipantId" });
            DropIndex("dbo.FixtureEvents", new[] { "EventTime_Id" });
            DropIndex("dbo.Stages", new[] { "ChampId" });
            DropIndex("dbo.ChampSeasons", new[] { "ChampSeasonId" });
            DropIndex("dbo.ChampSeasons", new[] { "ChampTypeId" });
            DropIndex("dbo.ChampTypes", new[] { "SportKindId" });
            DropIndex("dbo.Champs", new[] { "ChampSeasonId" });
            DropTable("dbo.TeamMemberHistories");
            DropTable("dbo.TeamHistories");
            DropTable("dbo.PlayerRetireHistories");
            DropTable("dbo.FixtureParticipants");
            DropTable("dbo.SquadPlayers");
            DropTable("dbo.Positions");
            DropTable("dbo.OutOfFixturePlayers");
            DropTable("dbo.Fixtures");
            DropTable("dbo.FixtureEvents");
            DropTable("dbo.Zones");
            DropTable("dbo.Leagues");
            DropTable("dbo.Teams");
            DropTable("dbo.TeamMembers");
            DropTable("dbo.Players");
            DropTable("dbo.EventTimes");
            DropTable("dbo.GroupTeams");
            DropTable("dbo.Groups");
            DropTable("dbo.Stages");
            DropTable("dbo.SportKinds");
            DropTable("dbo.ChampTypes");
            DropTable("dbo.ChampSeasons");
            DropTable("dbo.Champs");
        }
    }
}
