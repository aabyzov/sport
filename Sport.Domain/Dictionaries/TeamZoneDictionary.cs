﻿using System.Collections.Generic;

namespace Sport.Domain.Dictionaries
{
    public class TeamZoneDictionary
    {
        public static Dictionary<string, string> Get()
        {
            return new Dictionary<string, string>()
                {
            {"Belgium", "UEFA"},
            {"Croatia", "UEFA"},
            {"Serbia", "UEFA"},
            {"Switzerland", "UEFA"},
            {"Poland", "UEFA"},
            {"Romania", "UEFA"},
            {"Austria", "UEFA"},
            {"Israel", "UEFA"},
            {"France", "UEFA"},
            {"Slovakia", "UEFA"},
            {"Wales", "UEFA"},
            {"Slovenia", "UEFA"},
            {"Germany", "UEFA"},
            {"Netherlands", "UEFA"},
            {"Finland", "UEFA"},
            {"Scotland", "UEFA"},
            {"Italy", "UEFA"},
            {"Czech Rep.", "UEFA"},
            {"Republic of Ireland", "UEFA"},
            {"Latvia", "UEFA"},
            {"Greece", "UEFA"},
            {"Northern Ireland", "UEFA"},
            {"Spain", "UEFA"},
            {"Ukraine", "UEFA"},
            {"Hungary", "UEFA"},
            {"England", "UEFA"},
            {"Sweden", "UEFA"},
            {"Portugal", "UEFA"},
            {"Denmark", "UEFA"},
            {"Bulgaria", "UEFA"},
            {"Turkey", "UEFA"},
            {"Russia", "UEFA"},
            {"Norway", "UEFA"},

            {"Iran", "AFC"},
            {"Japan", "AFC"},
            {"Korea Republic", "AFC"},
            {"China PR", "AFC"},
            {"Australia", "AFC"},
            {"Saudi Arabia", "AFC"},

            {"Canada", "CONCACAF"},
            {"USA", "CONCACAF"},
            {"Costa Rica", "CONCACAF"},
            {"Mexico", "CONCACAF"},

            {"Egypt", "CAF"},
            {"Cote D'Ivoire", "CAF"},
            {"Cameroon", "CAF"},
            {"Senegal", "CAF"},
            {"Nigeria", "CAF"},
            {"South Africa", "CAF"},
            {"Ghana", "CAF"},
            {"Tunisia", "CAF"},

            {"Peru", "CONMEBOL"},
            {"Ecuador", "CONMEBOL"},
            {"Argentina", "CONMEBOL"},
            {"Uruguay", "CONMEBOL"},
            {"Brazil", "CONMEBOL"},
            {"Colombia", "CONMEBOL"},
            {"Paraguay", "CONMEBOL"},
            {"Chile", "CONMEBOL"}
                };
        } 
    }
}