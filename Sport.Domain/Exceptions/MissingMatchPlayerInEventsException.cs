﻿using System;
using Sport.Common.Core;
using Sport.Domain.Models;

namespace Sport.Domain.Exceptions
{
    public class MissingMatchPlayerInEventsException : Exception
    {
        public MissingMatchPlayerInEventsException(FixtureEvent fixtureEvent) : base(String.Format(Constants.Errors.MissingMatchPlayerInEvents, fixtureEvent.Player.FullName, fixtureEvent))
        {
            
        }
    }
}