﻿using System;

namespace Sport.Domain.Exceptions
{
    [Serializable]
    public class DataExpiredException : Exception
    {
        public string LastEditorFullName { get; set; }

        public DataExpiredException()
            : this(null)
        {
        }

        public DataExpiredException(Exception innerException)
            : this(null, innerException)
        {
        }

        public DataExpiredException(string lastEditorFullName, Exception innerException)
            : base("The data has expired", innerException)
        {
            LastEditorFullName = lastEditorFullName;
        }
    }
}