﻿using System;
using System.Collections;
using System.Linq;
using System.Linq.Expressions;
using Should;
using Sport.Common.Core;
using Sport.Domain.Models;
using Sport.Tests.Common;
using StructureMap;
using Xunit;

namespace Sport.Migration.IntegrationTests
{
    public class When_using_migration_manager : IntegrationBase
    {
        private MigrationManager migrationManager;
        public When_using_migration_manager()
        {
            migrationManager = ObjectFactory.GetInstance<MigrationManager>();
        }

        [Fact]
        public void Will_adjust_correct_date_on_Wc_Finals()
        {
            //Arrange
            var champSeason = SetupBasics();
            var europeQualif = SetupAllStarsQualifChamp(champSeason);
            var champ = SetupAllStarsFinalChamp();
            SetupYellowsInTwoFixturesOfDifferentChamps(CreateGameCenter(), europeQualif, champ);

            //Act
            champ = _sportUnitOfWork.Champs.WithFixturesAndTeams().First(x => x.Id == champ.Id);
            var champStages = champ.Stages.ToArray();

            Array.ForEach(champStages[0].Fixtures.ToArray(), Write);

            migrationManager.AdjustFixtureDates(champ);

            Console.WriteLine(Environment.NewLine + "Updated dates");
            Array.ForEach(champStages[0].Fixtures.ToArray(), Write);

            //Assert
            var champToAssert = _sportUnitOfWork.Champs.WithFixturesAndTeams().First(x => x.Id == champ.Id);
            var fixtures = champToAssert.Fixtures.ToList();
            const int year = 2014;
            var firstFixtureDate = new DateTime(year, 6, 12);
            fixtures[0].Date.Value.Date.ShouldEqual(firstFixtureDate);
            fixtures[2].Date.Value.Date.ShouldEqual(new DateTime(year, 6, 17));
            fixtures[4].Date.Value.Date.ShouldEqual(new DateTime(year, 6, 22));
            _db.Fixtures.ToList().FirstOrDefault(x => x.Id == fixtures[0].Id).Date.Value.Date.ShouldEqual(firstFixtureDate);
        }

        [Fact]
        public void Will_adjust_matchdays()
        {
            //Arrange
            var champSeason = SetupBasics();
            var champ = SetupAllStarsQualifChamp(champSeason);
            champ.Groups.Count().ShouldEqual(1);
            var fixtures = champ.Fixtures.Where(x => x.Group.Name == Constants.GroupA).ToArray();
            foreach (var item in fixtures)
            {
                item.MatchDay = 1;
            }

            //Act
            migrationManager.AdjustMatchDays(champ);

            //Assert
            var dbFixtures = _db.Fixtures.ToArray();
            dbFixtures[0].MatchDay.ShouldEqual(1);
            dbFixtures[1].MatchDay.ShouldEqual(1);
            dbFixtures[2].MatchDay.ShouldEqual(2);
            dbFixtures[3].MatchDay.ShouldEqual(2);
            dbFixtures[4].MatchDay.ShouldEqual(3);
            dbFixtures[5].MatchDay.ShouldEqual(3);
        }

        [Fact]
        public void Will_adjust_matchdays_with_five_teams_per_group()
        {
            //Arrange
            var champSeason = SetupBasics();
            var champ = SetupFiveTeamPerGroupChamp(champSeason);
            champ.Groups.Count().ShouldEqual(1);
            var fixtures = champ.Fixtures.Where(x => x.Group.Name == Constants.GroupA).ToArray();
            foreach (var item in fixtures)
            {
                item.MatchDay = 1;
            }

            //Act
            migrationManager.AdjustMatchDays(champ);

            //Assert
            var dbFixtures = _db.Fixtures.OrderBy(x => x.MatchDay).ToArray();
            dbFixtures[0].MatchDay.ShouldEqual(1);
            dbFixtures[1].MatchDay.ShouldEqual(1);
            dbFixtures[2].MatchDay.ShouldEqual(2);
            dbFixtures[3].MatchDay.ShouldEqual(2);
            dbFixtures[4].MatchDay.ShouldEqual(3);
            dbFixtures[5].MatchDay.ShouldEqual(3);
            dbFixtures[6].MatchDay.ShouldEqual(4);
            dbFixtures[7].MatchDay.ShouldEqual(4);
            dbFixtures[8].MatchDay.ShouldEqual(5);
            dbFixtures[9].MatchDay.ShouldEqual(5);
        }

        [Fact]
        public void Will_adjust_matchdays_with_three_teams_in_two_rounds_per_group()
        {
            //Arrange
            var champSeason = SetupBasics();
            var champ = SetupThreeTeamPerGroupChamp(champSeason);
            champ.Groups.Count().ShouldEqual(1);
            var fixtures = champ.Fixtures.Where(x => x.Group.Name == Constants.GroupA).ToArray();
            foreach (var item in fixtures)
            {
                item.MatchDay = 1;
            }

            //Act
            migrationManager.AdjustMatchDays(champ);

            //Assert
            var dbFixtures = _db.Fixtures.OrderBy(x => x.MatchDay).ToArray();
            dbFixtures[0].MatchDay.ShouldEqual(1);
            dbFixtures[1].MatchDay.ShouldEqual(2);
            dbFixtures[2].MatchDay.ShouldEqual(3);
            dbFixtures[3].MatchDay.ShouldEqual(4);
            dbFixtures[4].MatchDay.ShouldEqual(5);
            dbFixtures[5].MatchDay.ShouldEqual(6);
        }

        [Fact]
        public void Will_adjust_matchdays_with_two_rounds()
        {
            //Arrange
            var champSeason = SetupBasics();
            var champ = SetupAllStarsQualifChampTwoRounds(champSeason);
            champ.Groups.Count().ShouldEqual(1);
            var fixtures = champ.Fixtures.Where(x => x.Group.Name == Constants.GroupA).ToArray();
            foreach (var item in fixtures)
            {
                item.MatchDay = 1;
            }

            //Act
            migrationManager.AdjustMatchDays(champ);

            //Assert
            var dbFixtures = _db.Fixtures.ToArray();
            dbFixtures[0].MatchDay.ShouldEqual(1);
            dbFixtures[2].MatchDay.ShouldEqual(2);
            dbFixtures[4].MatchDay.ShouldEqual(3);
            dbFixtures[5].MatchDay.ShouldEqual(3);
            dbFixtures[6].MatchDay.ShouldEqual(4);
            dbFixtures[8].MatchDay.ShouldEqual(5);
            dbFixtures[10].MatchDay.ShouldEqual(6);

        }

    }
}