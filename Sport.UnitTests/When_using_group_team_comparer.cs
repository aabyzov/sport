﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sport.Domain.Models;
using Xunit;

namespace Sport.UnitTests
{
    public class When_using_group_team_comparer
    {
        [Fact]
        public void Group_team_comparer_should_work()
        {
		    var comparer = new GroupTeamComparer();
            var team = new Team()
                {
                    Id = Guid.NewGuid(),
                    Name = "Test"
                };
            var groupTeam1 = new GroupTeam()
                {
                    Team = team
                };
            var groupTeam2 = new GroupTeam()
                {
                    Team = team
                };

            Assert.True( new List<GroupTeam>(){groupTeam1, groupTeam2 }.Distinct(comparer).Count() == 1);
        }
    }
}