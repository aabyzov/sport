"# To run project from scratch:"

1. Install Azure SDK for VS 2013 (https://azure.microsoft.com/en-us/downloads/)
2. Attach Champs db from folder ChampsDB
3. Enable MSDTC in Services (Distributed Transaction Coordinator)
4. Open up MigrationTests class in Sport.Migration project, run this test - Will_migrate_data_with_committing_to_db
5. Run Will_migrate_out_of_fixture_players_with_committing_to_db test
6. Set Sport.Web.ResultTracking project as default
7. Launch in browser and see All Fixtures

To run integration tests:
1. Run UpdateIntegrationTestsDb.bat in folder "!BatchFiles"
2. Run tests from Sport.Migration.IntegrationTests

Validate:
1. Run all tests from Sport.Migration.ValidationTests

 
