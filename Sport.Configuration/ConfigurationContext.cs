﻿using System.Data.Entity;
using Sport.Configuration.Models;

namespace Sport.Configuration
{
    public class ConfigurationContext : DbContext
    {
        public ConfigurationContext()
            : base("name=ConfigurationContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<ConfigurationContext>(null);
            //base.Configuration.LazyLoadingEnabled = false;
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<SportKind> SportKinds { get; set; }
        public DbSet<League> Leagues { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<TeamMember> TeamMembers { get; set; }
        public DbSet<Player> Players { get; set; }

        public System.Data.Entity.DbSet<Sport.Configuration.Models.Zone> Zones { get; set; }

    }
}