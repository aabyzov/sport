﻿namespace Sport.Configuration.Models
{
    public class SportKind : Persistent
    {
        public string Name { get; set; }
    }
}