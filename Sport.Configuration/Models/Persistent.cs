﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sport.Configuration.Models
{
    //[DebuggerDisplay("Id={Id}")]
    public abstract class Persistent
    {
        protected Persistent()
        {
            Id = Guid.Empty;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
    }
}