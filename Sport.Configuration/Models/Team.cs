﻿using System;

namespace Sport.Configuration.Models
{
    public class Team : Persistent
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int? Chemistry { get; set; }
        public int? OverallRating { get; set; }

        public Guid LeagueId { get; set; }
        public virtual League League { get; set; }
        public Guid MigrationId { get; set; }
        public Guid ZoneId { get; set; }
        public virtual Zone Zone { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}