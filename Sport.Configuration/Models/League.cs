﻿using System;

namespace Sport.Configuration.Models
{
    public class League : Persistent
    {
        public string Name { get; set; }

        public Guid SportKindId { get; set; }
        public virtual SportKind SportKind { get; set; }

        public Guid MigrationId { get; set; }
       
    }
}