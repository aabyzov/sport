﻿using System;

namespace Sport.Configuration.Models
{
    public class TeamMember : Persistent
    {
        public Guid TeamId { get; set; }
        public virtual Team Team { get; set; }
        public int? PositionNumber { get; set; }
        public int? JerseyNumber { get; set; }
        public int? YearContractEnds { get; set; }

        public Guid PlayerId { get; set; }
        public virtual Player Player { get; set; }
    }
}