﻿namespace Sport.Configuration.Models
{
    public class Zone : Persistent
    {
        public string Name { get; set; }
        public int MigrationEnumId { get; set; }
    }
}