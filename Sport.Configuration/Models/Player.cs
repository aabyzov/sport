﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Sport.Configuration.Models
{
    public class Player : Persistent
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [NotMapped]
        public string FullName { get
        {
            if (!String.IsNullOrEmpty(FirstName))
                return FirstName + " " + LastName;
            return LastName;
        } }

        public virtual ICollection<TeamMember> TeamMembers { get; set; }
        public Guid MigrationId { get; set; }

        public override string ToString()
        {
            return FullName;
        }
    }
}