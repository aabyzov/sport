﻿#region

using System.Linq;
using Should;
using Sport.Common.Core;
using Sport.Domain.Models;
using Sport.Tests.Common;
using Xunit;

#endregion

namespace Sport.IntegrationTests.Services
{
    public class When_missing_matches : IntegrationBase
    {
        [Fact]
        public void It_should_suspend_player_for_2_yellows_in_sum()
        {
            //Arrange
            var gameCenter = CreateGameCenter();
            var missingManager = CreateMissingMatchManager();
            var uow = CreateUnitOfWork();
            var champSeason = SetupBasics();
            var champ = SetupAllStarsQualifChamp(champSeason);

            //Act
            AddNesta2YellowsInDiffFixturesOfEuropeQualif(gameCenter, champ);

            //Assert
            var italyFrance = GetFixture(champ, Constants.Teams.Italy, Constants.Teams.France);
            var outOfFixturePlayers = missingManager.BeforeMatch(italyFrance);
            outOfFixturePlayers.First().Player.LastName.ShouldEqual("Nesta");
            _db.FixtureEvents.OfType<FootballYellowCard>().Count().ShouldEqual(2);
            var footballYellowCard = _db.FixtureEvents.OfType<FootballYellowCard>().FirstOrDefault();
            footballYellowCard.ShouldNotBeNull();
            footballYellowCard.Player.LastName.ShouldEqual(Constants.Players.Nesta);
            _db.Suspensions.Count().ShouldEqual(1);
            var suspesion = _db.Suspensions.FirstOrDefault();
            suspesion.ShouldNotBeNull();
            var susp = uow.Suspensions.FindOneBy(x => x.Id == suspesion.Id);
            susp.FixtureEvent.Player.LastName.ShouldEqual(Constants.Players.Nesta);
        }

        [Fact]
        public void It_should_reset_yellows_count_after_champ_ends()
        {
            //Arrange
            var gameCenter = CreateGameCenter();
            var champSeason = SetupBasics();
            var europeQualif = SetupAllStarsQualifChamp(champSeason);
            var finals = SetupAllStarsFinalChamp();

            //Act
            SetupYellowsInTwoFixturesOfDifferentChamps(gameCenter, europeQualif, finals);

            //Assert
            _db.FixtureEvents.OfType<FootballYellowCard>().Count().ShouldEqual(2);
            var footballYellowCard = _db.FixtureEvents.OfType<FootballYellowCard>().FirstOrDefault();
            footballYellowCard.ShouldNotBeNull();
            footballYellowCard.Player.LastName.ShouldEqual(Constants.Players.Nesta);
            _db.Suspensions.Count().ShouldEqual(0);
        }
    }
}