﻿using System.Linq;
using Should;
using Sport.Common.Core;
using Sport.Tests.Common;
using Xunit;

namespace Sport.IntegrationTests.Services
{
    public class When_using_datetime_provider : IntegrationBase
    {
        [Fact]
        public void Will_get_current_date_of_first_not_started_match()
        {
            //Arrange
            var gameCenter = CreateGameCenter();
            var champSeason = SetupBasics();
            var europeQualif = SetupAllStarsQualifChamp(champSeason);
            var dateTimeProvider = CreateDateTimeProvider();
            Assert.Equal(0, europeQualif.Fixtures.GroupBy(x => x.Date).Count(x => x.Count() > 1));
            var firstMatchDate = dateTimeProvider.CurrentMatchDate;
            var englandItaly = GetFixture(europeQualif, Constants.Teams.England, Constants.Teams.Italy);
            const int homeScore = 0, awayScore = 0;
            
            //Act
            SaveResult(gameCenter, englandItaly, homeScore, awayScore);
            var secondMatchDate = dateTimeProvider.CurrentMatchDate;

            //Assert
            firstMatchDate.ShouldNotBeSameAs(secondMatchDate);
        }
 
    }
}