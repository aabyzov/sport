﻿using System;
using System.Linq;
using Should;
using Sport.Common.Core;
using Sport.Domain.Models;
using Sport.Domain.Services;
using Sport.Tests.Common;
using Xunit;

namespace Sport.IntegrationTests.Services
{
    public class When_using_schedule_manager : IntegrationBase
    {
        [Fact]
        public void Will_create_African_schedule()
        {
            //Arrange
            var champSeason = SetupBasics();
            SetupAllStarsQualifChamp(champSeason);
            var scheduleManager = CreateScheduleManager();
            AddAllZones();
            var champType = _db.ChampTypes.FirstOrDefault(x => x.Name == ChampTypeName);
            var africa = _db.Zones.FirstOrDefault(x => x.Name == Constants.Zones.Africa);

            //Act
            var schedule = scheduleManager.GetScheduleTable(africa, champType);
            var stages = schedule.GetStages(2006);

            //Assert
            stages.Count().ShouldEqual(1);
            var stage = stages.First();
            stage.Groups.Count().ShouldEqual(1);
            var group = stage.Groups.First();
            var startDate = new DateTime(2005, 01, 15, 18, 0, 0);
            startDate.DayOfWeek.ShouldEqual(DayOfWeek.Saturday);
            group.Fixtures[0].Date.ShouldEqual(startDate);
            var secondDay = new DateTime(2005, 01, 17, 18, 0, 0);
            secondDay.DayOfWeek.ShouldEqual(DayOfWeek.Monday);
            group.Fixtures[4].Date.ShouldEqual(secondDay);
            group.Fixtures[8].Date.DayOfWeek.ShouldEqual(DayOfWeek.Wednesday);
            group.Fixtures.Count().ShouldEqual(28);
            var last = @group.Fixtures.Last();
            last.ShouldEqual(group.Fixtures[27]);
            last.Date.DayOfWeek.ShouldEqual(DayOfWeek.Thursday);
        }

        [Fact]
        public void Will_create_South_America_schedule()
        {
            //Arrange
            var champSeason = SetupBasics();
            SetupAllStarsQualifChamp(champSeason);
            var scheduleManager = CreateScheduleManager();
            AddAllZones();
            var champType = _db.ChampTypes.FirstOrDefault(x => x.Name == ChampTypeName);
            var america = _db.Zones.FirstOrDefault(x => x.Name == Constants.Zones.SouthAmerica);

            //Act
            var schedule = scheduleManager.GetScheduleTable(america, champType);
            var stages = schedule.GetStages(2006);

            //Assert
            stages.Count().ShouldEqual(1);
            var stage = stages.First();
            var startDate = new DateTime(2003, 10, 3, 18, 0, 0);
            stage.MatchDays.First().DayOfWeek.ShouldEqual(DayOfWeek.Friday);
            stage.MatchDays.First().ShouldEqual(startDate);
        }

        [Fact]
        public void Will_create_North_America_schedule()
        {
            //Arrange
            var champSeason = SetupBasics();
            SetupAllStarsQualifChamp(champSeason);
            var scheduleManager = CreateScheduleManager();
            AddAllZones();
            var champType = _db.ChampTypes.FirstOrDefault(x => x.Name == ChampTypeName);
            var america = _db.Zones.FirstOrDefault(x => x.Name == Constants.Zones.NorthAmerica);

            //Act
            var schedule = scheduleManager.GetScheduleTable(america, champType);
            var stages = schedule.GetStages(2006);

            //Assert
            stages.Count().ShouldEqual(1);
            var stage = stages.First();
            var startDate = new DateTime(2004, 02, 4, 18, 0, 0);
            stage.MatchDays.First().DayOfWeek.ShouldEqual(DayOfWeek.Wednesday);
            stage.MatchDays.First().ShouldEqual(startDate);
        }

        [Fact]
        public void Will_create_Europe_schedule()
        {
            //Arrange
            var champSeason = SetupBasics();
            SetupAllStarsQualifChamp(champSeason);
            var scheduleManager = CreateScheduleManager();
            AddAllZones();
            var champType = _db.ChampTypes.FirstOrDefault(x => x.Name == ChampTypeName);
            var europe = _db.Zones.FirstOrDefault(x => x.Name == Constants.Zones.Europe);

            //Act
            var schedule = scheduleManager.GetScheduleTable(europe, champType);
            var stages = schedule.GetStages(2006);

            //Assert
            stages.Count().ShouldEqual(1);
            var stage = stages.First();
            var startDate = new DateTime(2004, 09, 3, 19, 0, 0);
            stage.MatchDays.First().DayOfWeek.ShouldEqual(DayOfWeek.Friday);
            stage.MatchDays.First().ShouldEqual(startDate);
        }

        [Fact]
        public void Will_create_Friendlies_Wc_schedule()
        {
            //Arrange
            var champSeason = SetupBasics();
            SetupAllStarsQualifChamp(champSeason);
            var scheduleManager = CreateScheduleManager();
            AddAllZones();
            var champType = _db.ChampTypes.FirstOrDefault(x => x.Name == ChampTypeName);
            var friendlies = _db.Zones.FirstOrDefault(x => x.Name == Constants.Zones.Friendly);

            //Act
            var schedule = scheduleManager.GetScheduleTable(friendlies, champType);
            var stages = schedule.GetStages(2006);

            //Assert
            stages.Count().ShouldEqual(1);
            var stage = stages.First();
            var startDate = new DateTime(2004, 09, 7, 19, 0, 0);
            stage.MatchDays.First().DayOfWeek.ShouldEqual(DayOfWeek.Tuesday);
            stage.MatchDays.First().ShouldEqual(startDate);
        }

        [Fact]
        public void Will_create_Wc_Finals_schedule()
        {
            //Arrange
            var champSeason = SetupBasics();
            SetupAllStarsQualifChamp(champSeason);
            var scheduleManager = CreateScheduleManager();
            AddAllZones();
            var champType = _db.ChampTypes.FirstOrDefault(x => x.Name == ChampTypeName);
            var finals = _db.Zones.FirstOrDefault(x => x.Name == Constants.Zones.Host);

            //Act
            var schedule = scheduleManager.GetScheduleTable(finals, champType);
            var stages = schedule.GetStages(2006);

            //Assert
            stages.Count().ShouldEqual(5);
            var stage = stages.First();
            var startDate = new DateTime(2006, 06, 8, 19, 0, 0);
            stage.MatchDays.First().DayOfWeek.ShouldEqual(DayOfWeek.Thursday);
            stage.MatchDays.First().ShouldEqual(startDate);
        }
    } 
}