﻿using System;
using System.Linq;
using Sport.Domain.Services;
using Sport.Domain.Services.Templates;
using Sport.Tests.Common;
using Xunit;

namespace Sport.IntegrationTests.Services
{
    public class When_using_standings_manager : IntegrationBase
    {
        [Fact]
        public void It_should_build_table()
        {
            //Arrange
            var gameCenter = CreateGameCenter();
            var standingsManager = CreateStandingsManager();
            var champSeason = SetupBasics();
            var champ = SetupAllStarsQualifChamp(champSeason);
            AddNesta2YellowsInDiffFixturesOfEuropeQualif(gameCenter, champ);

            //Act
            var standings = standingsManager.BuildStandings(champ.Stages.FirstOrDefault().Groups.FirstOrDefault(), new WorldCupTemplate());

            Write(standings);
            //Assert

        }

        
 
    }
}