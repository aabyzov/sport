﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sport.Web.Public.Startup))]
namespace Sport.Web.Public
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
