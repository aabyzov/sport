﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Transactions;
using Sport.Common.Core;
using Sport.Domain.Models;
using Sport.Domain.Models.Enums;
using Sport.Domain.Services;
using Sport.Domain.Services.Standings;
using Sport.Domain.UoW;
using Sport.Tests.Common.Setup;
using StructureMap;

namespace Sport.Tests.Common
{
    public abstract class IntegrationBase : SetupBasicData, IDisposable
    {
        protected TransactionScope _tran;

        protected ILogger _logger;
        protected ISportUnitOfWork _sportUnitOfWork;

        protected IntegrationBase()
        {
            _logger = new Logger("sport.web", "Integration");
            Init();
        }

        public virtual void Init()
        {
            //_db.Database.Log = Console.WriteLine;
            IntegrationTests.IoC.CompositionRootTests.Initialize();

            _db = CreateSportContext();
            _db.UseAudit = false;
            _sportUnitOfWork = CreateUnitOfWork();
            _tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.Zero);
        }

        public virtual void Dispose()
        {
            _tran.Dispose();
            Cleanup();
        }

        public virtual void Cleanup()
        {

        }

        public SportContext CreateSportContext()
        {
            return ObjectFactory.GetInstance<SportContext>();
        }

        public IMissingMatchManager CreateMissingMatchManager()
        {
            return ObjectFactory.GetInstance<IMissingMatchManager>();
        }

        public ISportUnitOfWork CreateUnitOfWork()
        {
            return ObjectFactory.GetInstance<ISportUnitOfWork>();
        }

        public IDateTimeProvider CreateDateTimeProvider()
        {
            return ObjectFactory.GetInstance<IDateTimeProvider>();
        }

        public IGameCenter CreateGameCenter()
        {
            return ObjectFactory.GetInstance<IGameCenter>();
        }

        public IScheduleManager CreateScheduleManager()
        {
            return ObjectFactory.GetInstance<IScheduleManager>();
        }

        public IStandingsManager CreateStandingsManager()
        {
            return ObjectFactory.GetInstance<IStandingsManager>();
        }

        #region
        public void Write(Standings standings)
        {
            // Table head
            const int defaultSpace = 35;
            const string template = "{0}{1}{2} {3} {4} {5} {6} {7} {8} {9} {10} {11}";
            Console.WriteLine(template, GetSpaces(defaultSpace), string.Empty, "P ", "W ", "D ", "L ", "F ", "A ", "PP ", "PD ", "PA ", "D ");
            //
            if (standings != null)
                foreach (var team in standings.Teams)
                {
                    var spacesCount = defaultSpace - team.Team.Team.Name.Length;
                    var spaceSeparator = GetSpaces(spacesCount);

                    Console.WriteLine(template, team.Team.Team.Name,
                                      spaceSeparator,
                                      GetValueForString(team.Points),
                                      GetValueForString(team.Wins),
                                      GetValueForString(team.Draws),
                                      GetValueForString(team.Loses),
                                      GetValueForString(team.Forward),
                                      GetValueForString(team.Against),
                                      GetValueForString(team.PartialPoints),
                                      GetValueForString(team.PartialForward - team.PartialAgainst),
                                      GetValueForString(team.PartialAwayGoals),
                                      GetValueForString(team.PartialForward - team.PartialAgainst)
                        );
                }
        }

        public void Write(Stage stage)
        {
            Console.WriteLine();
            Console.WriteLine("-------{0}--------", stage.Name);
            Console.WriteLine();
            Write(stage.Groups);
        }

        public void Write(IEnumerable<Group> groups)
        {

            foreach (var @group in groups)
            {
                Write(@group);
            }
        }

        public void Write(Group @group)
        {
            Console.WriteLine();
            if (group.Stage.StageType == StageType.Group)
            {
                Console.WriteLine(group.Name);
                //Write(group.Teams);
                //Console.WriteLine();
                Write(group.Standings);
            }
            Console.WriteLine();
            Write(group.Fixtures);
            Console.WriteLine();

            if (group.Stage.StageType == StageType.Playoff)
            {
                if (group.Standings != null)
                {
                    var team = group.Standings.Teams.FirstOrDefault();
                    if (team != null)
                    {
                        if (group.Stage.Champ.Stages.Last() != group.Stage)
                            Console.WriteLine("Team to proceed {0}", team.Team.Team.Name);
                        else
                        {
                            Console.WriteLine("Champion is {0}", team.Team.Team.Name);
                        }
                    }
                }
            }
        }

        public void Write(IEnumerable<Fixture> fixtures)
        {
            foreach (var fixture in fixtures)
            {
                Write(fixture);
            }
        }

        public void Write(Fixture fixture)
        {
            Console.WriteLine(fixture.Date.Value.ToShortDateString() + " MatchDay" + fixture.MatchDay + " " + fixture);
            if (fixture.FootballEvents == null || !fixture.FootballEvents.Any()) return;
            foreach (var fact in fixture.FootballEvents)
            {
                Write(fact, fact.FixtureParticipant.IsHome ? string.Empty : new string(' ', fact.FixtureParticipant.Fixture.Home.Team.Name.Count() + 10));
            }
        }

        public void Write(FootballEvent fact, string space = "")
        {
            Console.WriteLine(space + fact);
        }

        private string GetSpaces(int count)
        {
            string result = string.Empty;
            for (var i = 0; i < count; i++)
            {
                result += " ";
            }
            return result;
        }

        private static string GetValueForString(int value)
        {
            const string separator = " ";
            if (value > 9 || value < 0)
                return value.ToString(CultureInfo.InvariantCulture);
            return value + separator;
        }
        #endregion
    }
}