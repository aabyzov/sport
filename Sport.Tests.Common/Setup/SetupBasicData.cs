﻿#region

using System;
using System.Linq;
using Sport.Common.Core;
using Sport.Domain.Models;
using Sport.Domain.Services;

#endregion

namespace Sport.Tests.Common.Setup
{
    public abstract partial class SetupBasicData
    {
        public const int YellowsToMissNextMatch = 2;
        public const string SportKindName = "FootballTest";
        public const string ChampTypeName = "AllStars World Cup";
        public const string ChampSeasonName = "AllStars World Cup Qualification";
        public const string QualifChampName = "All Europe";
        public const string AllStarsFinalChampName = "All Starts Finals";
        public const string LocationName = "Minsk";
        protected SportContext _db;

        public ChampSeason SetupBasics()
        {
            var sportKind = new SportKind() {Name = SportKindName};
            _db.SportKinds.Add(sportKind);
            _db.SaveChanges();

            AddAllZones();

            SetupLeagueTeamPlayerData(sportKind);

            var champType = new ChampType() {Name = ChampTypeName, SportKind = sportKind};

            var champSeason = new ChampSeason()
            {
                Name = ChampSeasonName,
                Start = new DateTime(2014, 1, 1),
                End = new DateTime(2014, 1, 30),
                ChampType = champType
            };
            return champSeason;
        }

        public Champ SetupAllStarsQualifChamp(ChampSeason champSeason)
        {
            _db.ChampSeasons.Add(champSeason);
            _db.SaveChanges();
            var champ = new Champ()
            {
                Name = QualifChampName,
                ChampSeason = champSeason,
                YellowsToMissNextMatch = YellowsToMissNextMatch,
                Zone = new Zone() {Name = Constants.Zones.Europe}
            };
            var stage = new Stage() {Name = Constants.GroupStage, Champ = champ};
            var group = new Group()
            {
                Name = Constants.GroupA,
                Stage = stage
            };

            _db.Groups.Add(group);
            _db.SaveChanges();

            var england = GetTeam(Constants.Teams.England);
            var italy = GetTeam(Constants.Teams.Italy);
            var france = GetTeam(Constants.Teams.France);
            var germany = GetTeam(Constants.Teams.Germany);

            AddFixture(england, italy, group, 1);
            AddFixture(france, germany, group, 1);
            AddFixture(england, france, group, 2);
            AddFixture(germany, italy, group, 2);
            AddFixture(germany, england, group, 3);
            AddFixture(italy, france, group, 3);
            return champ;
        }

        public Champ SetupAllStarsQualifChampTwoRounds(ChampSeason champSeason)
        {
            _db.ChampSeasons.Add(champSeason);
            _db.SaveChanges();
            var champ = new Champ()
            {
                Name = QualifChampName,
                ChampSeason = champSeason,
                YellowsToMissNextMatch = YellowsToMissNextMatch,
                Zone = new Zone() {Name = Constants.Zones.Europe}
            };
            var stage = new Stage() {Name = Constants.GroupStage, Champ = champ, RoundCount = 2};
            var group = new Group()
            {
                Name = Constants.GroupA,
                Stage = stage
            };

            _db.Groups.Add(group);
            _db.SaveChanges();

            var england = GetTeam(Constants.Teams.England);
            var italy = GetTeam(Constants.Teams.Italy);
            var france = GetTeam(Constants.Teams.France);
            var germany = GetTeam(Constants.Teams.Germany);

            AddFixture(england, italy, group, 1);
            AddFixture(france, germany, group, 1);
            AddFixture(england, france, group, 2);
            AddFixture(germany, italy, group, 2);
            AddFixture(germany, england, group, 3);
            AddFixture(italy, france, group, 3);
            AddFixture(england, italy, group, 4);
            AddFixture(france, germany, group, 4);
            AddFixture(england, france, group, 5);
            AddFixture(germany, italy, group, 5);
            AddFixture(germany, england, group, 6);
            AddFixture(italy, france, group, 6);
            return champ;
        }

        public Champ SetupFiveTeamPerGroupChamp(ChampSeason champSeason)
        {
            _db.ChampSeasons.Add(champSeason);
            _db.SaveChanges();
            var champ = new Champ()
            {
                Name = QualifChampName,
                ChampSeason = champSeason,
                YellowsToMissNextMatch = YellowsToMissNextMatch,
                Zone = new Zone() {Name = Constants.Zones.Europe}
            };
            var stage = new Stage() {Name = Constants.GroupStage, Champ = champ};
            var group = new Group()
            {
                Name = Constants.GroupA,
                Stage = stage
            };

            _db.Groups.Add(group);
            _db.SaveChanges();

            var england = GetTeam(Constants.Teams.England);
            var italy = GetTeam(Constants.Teams.Italy);
            var france = GetTeam(Constants.Teams.France);
            var germany = GetTeam(Constants.Teams.Germany);
            var spain = GetTeam(Constants.Teams.Spain);

            AddFixture(england, italy, group, 1);
            AddFixture(spain, england, group, 1);
            AddFixture(spain, italy, group, 1);
            AddFixture(spain, france, group, 1);
            AddFixture(spain, germany, group, 1);
            AddFixture(france, germany, group, 1);
            AddFixture(england, france, group, 2);
            AddFixture(germany, italy, group, 2);
            AddFixture(germany, england, group, 3);
            AddFixture(italy, france, group, 3);
            return champ;
        }

        public Champ SetupThreeTeamPerGroupChamp(ChampSeason champSeason)
        {
            _db.ChampSeasons.Add(champSeason);
            _db.SaveChanges();
            var champ = new Champ()
            {
                Name = QualifChampName,
                ChampSeason = champSeason,
                YellowsToMissNextMatch = YellowsToMissNextMatch,
                Zone = new Zone() {Name = Constants.Zones.Europe}
            };
            var stage = new Stage() {Name = Constants.GroupStage, Champ = champ};
            var group = new Group()
            {
                Name = Constants.GroupA,
                Stage = stage
            };

            _db.Groups.Add(group);
            _db.SaveChanges();

            var england = GetTeam(Constants.Teams.England);
            var italy = GetTeam(Constants.Teams.Italy);
            var france = GetTeam(Constants.Teams.France);

            AddFixture(england, italy, group, 1);
            AddFixture(england, france, group, 2);
            AddFixture(italy, france, group, 3);
            AddFixture(england, italy, group, 1);
            AddFixture(england, france, group, 2);
            AddFixture(italy, france, group, 3);
            return champ;
        }

        public Champ SetupAllStarsFinalChamp()
        {
            var champSeason = _db.ChampSeasons.FirstOrDefault(x => x.Name == ChampSeasonName);
            var champ = new Champ()
            {
                Name = AllStarsFinalChampName,
                ChampSeason = champSeason,
                YellowsToMissNextMatch = YellowsToMissNextMatch,
                Zone = new Zone() {Name = Constants.Zones.Host}
            };
            var stage = new Stage() {Name = Constants.GroupStage, Champ = champ};
            var group = new Group()
            {
                Name = Constants.GroupA,
                Stage = stage
            };
            _db.Groups.Add(group);
            _db.SaveChanges();

            var italy = GetTeam(Constants.Teams.Italy);
            var france = GetTeam(Constants.Teams.France);
            var brazil = GetTeam(Constants.Teams.Brazil);
            var argentina = GetTeam(Constants.Teams.Argentina);

            AddFixture(italy, brazil, group, 1);
            AddFixture(france, argentina, group, 1);
            AddFixture(italy, france, group, 2);
            AddFixture(brazil, argentina, group, 2);
            AddFixture(argentina, italy, group, 3);
            AddFixture(brazil, france, group, 3);
            return champ;
        }

        protected void AddNesta2YellowsInDiffFixturesOfEuropeQualif(IGameCenter gameCenter, Champ champ)
        {
            var nesta = _db.Players.FirstOrDefault(x => x.LastName == Constants.Players.Nesta);
            var englandItaly = GetFixture(champ, Constants.Teams.England, Constants.Teams.Italy);
            const int homeScore = 0, awayScore = 0;
            AddYellowCard(englandItaly.Away, nesta, 7);
            gameCenter.SaveResult(englandItaly, homeScore, awayScore);

            var germanyItaly = GetFixture(champ, Constants.Teams.Germany, Constants.Teams.Italy);
            AddYellowCard(germanyItaly.Away, nesta, 77);
            gameCenter.SaveResult(germanyItaly, homeScore, awayScore);
        }

        protected void SetupYellowsInTwoFixturesOfDifferentChamps(IGameCenter gameCenter, Champ firstChamp,
            Champ secondChamp)
        {
            var nesta = _db.Players.FirstOrDefault(x => x.LastName == Constants.Players.Nesta);
            var englandItaly = GetFixture(firstChamp, Constants.Teams.England, Constants.Teams.Italy);
            const int homeScore = 0, awayScore = 0;
            AddYellowCard(englandItaly.Away, nesta, 5);
            SaveResult(gameCenter, englandItaly, homeScore, awayScore);

            var italyBrazil = GetFixture(secondChamp, Constants.Teams.Brazil, Constants.Teams.Italy);
            AddYellowCard(italyBrazil.Home, nesta, 55);
            SaveResult(gameCenter, italyBrazil, homeScore, awayScore);
        }
    }
}