﻿using Sport.Common.Core;
using Sport.Domain.Models;

namespace Sport.Tests.Common.Setup
{
    public abstract partial class SetupBasicData
    {
        private void SetupLeagueTeamPlayerData(SportKind sportKind)
        {
            var league = new League()
            {
                Name = Constants.International,
                SportKind = sportKind
            };
            _db.Leagues.Add(league);
            _db.SaveChanges();
            var europe = new Zone() { Name = Constants.Zones.Europe };
            var conmebol = new Zone() { Name = Constants.Zones.SouthAmerica };
            AddTeam(league, Constants.Teams.Italy, europe);
            AddTeam(league, Constants.Teams.England, europe);
            AddTeam(league, Constants.Teams.France, europe);
            AddTeam(league, Constants.Teams.Germany, europe);
            AddTeam(league, Constants.Teams.Spain, europe);
            AddTeam(league, Constants.Teams.Brazil, conmebol);
            AddTeam(league, Constants.Teams.Argentina, conmebol);
            _db.SaveChanges();
            SetupPlayers();
            _db.SaveChanges();
        }

        public void SetupPlayers()
        {
            var england = GetTeam(Constants.Teams.England);
            var italy = GetTeam(Constants.Teams.Italy);
            var germany = GetTeam(Constants.Teams.Germany);
            var france = GetTeam(Constants.Teams.France);
            var brazil = GetTeam(Constants.Teams.Brazil);
            var argentina = GetTeam(Constants.Teams.Argentina);

            AddPlayer(england, Constants.Players.Gerrard);
            AddPlayer(england, Constants.Players.Rooney);
            AddPlayer(england, Constants.Players.Lampard);
            AddPlayer(england, Constants.Players.Walcott);
            AddPlayer(england, Constants.Players.Welbeck);
            AddPlayer(italy, Constants.Players.Totti);
            AddPlayer(italy, Constants.Players.Balotelli);
            AddPlayer(italy, Constants.Players.Cassano);
            AddPlayer(italy, Constants.Players.Pirlo);
            AddPlayer(italy, Constants.Players.Nesta);
            AddPlayer(germany, Constants.Players.Schweinsteiger);
            AddPlayer(germany, Constants.Players.Ballack);
            AddPlayer(germany, Constants.Players.Klose);
            AddPlayer(germany, Constants.Players.Muller);
            AddPlayer(germany, Constants.Players.Ozil);
            AddPlayer(france, Constants.Players.Zidane);
            AddPlayer(france, Constants.Players.Ribery);
            AddPlayer(france, Constants.Players.Henry);
            AddPlayer(france, Constants.Players.Trezeguet);
            AddPlayer(france, Constants.Players.Nasri);
            AddPlayer(brazil, Constants.Players.Ronaldo);
            AddPlayer(brazil, Constants.Players.Oscar);
            AddPlayer(brazil, Constants.Players.Neymar);
            AddPlayer(brazil, Constants.Players.RobertoCarlos);
            AddPlayer(brazil, Constants.Players.Hulk);
            AddPlayer(argentina, Constants.Players.Messi);
            AddPlayer(argentina, Constants.Players.Tevez);
            AddPlayer(argentina, Constants.Players.Batistuta);
            AddPlayer(argentina, Constants.Players.Zanetti);
            AddPlayer(argentina, Constants.Players.Cambiasso);
        }
        
        
    }
}