﻿using System;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using Sport.Common.Core;
using Sport.Domain.Models;
using Sport.Domain.Services;
using Group = Sport.Domain.Models.Group;
using GroupTeam = Sport.Domain.Models.GroupTeam;
using Team = Sport.Domain.Models.Team;

namespace Sport.Tests.Common.Setup
{
    public abstract partial class SetupBasicData
    {
        public void AddPlayer(Team team, string lastName)
        {
            var player = new Player()
            {
                LastName = lastName,
            };
            _db.Players.Add(player);
            _db.TeamMembers.Add(new TeamMember()
            {
                Team = team,
                Player = player
            });
        }

        private void AddTeam(League league, string teamName, Zone zone)
        {
            var newTeam = new Team()
            {
                League = league,
                Name = teamName,
                Zone = zone
            };
            _db.Teams.Add(newTeam);
        }

        /// <summary>
        /// Creates a group team and adds it to the group if there is no team with the same name already in the group
        /// </summary>
        /// <param name="group"></param>
        /// <param name="team"></param>
        /// <returns></returns>
        private GroupTeam AddOrGetGroupTeam(Group group, Team team)
        {
            var existingGroupTeam = @group.GroupTeams.FirstOrDefault(x => x.Team.Name == team.Name);
            if (existingGroupTeam != null) return existingGroupTeam;
            var groupTeam = new GroupTeam()
            {
                Group = group,
                Team = team
            };
            _db.GroupTeams.Add(groupTeam);
            _db.SaveChanges();
            return groupTeam;
        }
        private void AddFixture(Team home, Team away, Group group, int matchDay = 0)
        {
            var homeGroupTeam = AddOrGetGroupTeam(group, home);
            var awayGroupTeam = AddOrGetGroupTeam(group, away);
            var homeParticipant = new FootballFixtureParticipant()
            {
                GroupTeam = homeGroupTeam,
                IsHome = true,
            };
            var awayParticipant = new FootballFixtureParticipant()
            {
                GroupTeam = awayGroupTeam,
                IsHome = false
            };
            var fixture = new Fixture()
            {
                Date = DateTime.Now,
                Location = LocationName,
                FixtureParticipants = new Collection<FixtureParticipant>()
                    {
                        homeParticipant, awayParticipant
                    },
                    MatchDay = matchDay
            };
            _db.Fixtures.Add(fixture);
            _db.SaveChanges();
        }
        private void AddYellowCard(FixtureParticipant homeOrAway, Player player, int minute)
        {
            var yellowCard = new FootballYellowCard()
            {
                Player = player,
                //FixtureParticipant = homeOrAway,
                FixtureParticipantId = homeOrAway.Id,
                EventTime = new FootballEventTime(minute),

            };
            homeOrAway.Events.Add(yellowCard);
            _db.FixtureEvents.Add(yellowCard);
            //_db.SaveChanges();
        }

        public void AddAllZones()
        {
            //That means Finals
            _db.Zones.Add(new Zone() { Name = Constants.Zones.Host });
            _db.Zones.Add(new Zone() { Name = Constants.Zones.Africa });
            _db.Zones.Add(new Zone() { Name = Constants.Zones.Asia });
            _db.Zones.Add(new Zone() { Name = Constants.Zones.Europe });
            _db.Zones.Add(new Zone() { Name = Constants.Zones.Friendly });
            _db.Zones.Add(new Zone() { Name = Constants.Zones.NorthAmerica });
            _db.Zones.Add(new Zone() { Name = Constants.Zones.Oceania });
            _db.Zones.Add(new Zone() { Name = Constants.Zones.SouthAmerica });
            _db.SaveChanges();
        }

        private Team GetTeam(string teamName)
        {
            //return _db.Teams.FirstOrDefault(x => String.Equals(x.Name, teamName, StringComparison.CurrentCultureIgnoreCase));
            return _db.Teams.FirstOrDefault(x => x.Name == teamName);
        }

        /// <summary>
        /// Gets first fixture checking home and away team in order of parameters passed. If not found swaps these 2 parameters
        /// </summary>
        /// <param name="champ"></param>
        /// <param name="homeTeam"></param>
        /// <param name="awayTeam"></param>
        /// <returns></returns>
        public Fixture GetFixture(Champ champ, string homeTeam, string awayTeam)
        {
            return GetFixtureInner(champ, homeTeam, awayTeam) ?? GetFixtureInner(champ, awayTeam, homeTeam);
        }

        private Fixture GetFixtureInner(Champ champ, string homeTeam, string awayTeam)
        {
            var fixture = _db.Fixtures.OrderBy(x => x.Date).Include(x => x.FixtureParticipants.Select(fp => fp.Events.Select(fev => fev.EventTime)))
                .Include(x => x.FixtureParticipants.Select(fp => fp.Events.Select(fpe => fpe.Player)))
                .Include(x => x.FixtureParticipants.Select(fp => fp.GroupTeam.Group.Stage.Champ.ChampSeason))
                .Include(x => x.FixtureParticipants.Select(fp => fp.GroupTeam.Team))
                .Include(x => x.FixtureParticipants.Select(fp => fp.OutOfFixturePlayers.Select(ofp => ofp.Player)))
                .FirstOrDefault(
                    x =>
                        x.FixtureParticipants.FirstOrDefault(
                            fp => fp.GroupTeam.Group.Stage.ChampId == champ.Id && fp.GroupTeam.Team.Name == homeTeam) != null
                        && x.FixtureParticipants.FirstOrDefault(fp => fp.GroupTeam.Team.Name == awayTeam) != null);
            return fixture;
        }

        public void SaveResult(IGameCenter gameCenter, Fixture fixture, int homeScore, int awayScore)
        {
            gameCenter.SaveResult(fixture, homeScore, awayScore);
           
        }
    }
}