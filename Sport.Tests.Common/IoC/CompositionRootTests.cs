﻿using Sport.Common.Core;
using Sport.Domain.IoC;
using Sport.Domain.Models;
using Sport.Domain.Repositories;
using Sport.Domain.UoW;
using StructureMap;
using StructureMap.Graph;

namespace Sport.IntegrationTests.IoC
{
    public static class CompositionRootTests
    {
        public static void Initialize()
        {
            CompositionRoot.Initialize();
            
            //ObjectFactory.Initialize(x =>
            //{
            //    x.For<ISportUnitOfWork>().Singleton().Use<SportUnitOfWork>();
            //});
            ObjectFactory.Configure(x =>
            {
                x.For<ISportUnitOfWork>().Singleton().Use<SportUnitOfWork>();
            });
            //ObjectFactory.Container. For<ISportUnitOfWork>().HybridHttpOrThreadLocalScoped().Use<SportUnitOfWork>();
            //ObjectFactory.Initialize(x =>
            //{
            //    x.SetAllProperties(y => y.OfType<MigrationManager>());
            //});
            //container.; .SetAllProperties(x => x.OfType<IService>());
            //return container;
        }
    }
}