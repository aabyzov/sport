﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sport.Web.Configuration.Startup))]
namespace Sport.Web.Configuration
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
