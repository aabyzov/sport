﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Sport.Configuration;
using Sport.Configuration.Models;

namespace Sport.Web.Configuration.Controllers
{
    public class TeamMemberController : Controller
    {
        private ConfigurationContext db = new ConfigurationContext();

        // GET: /TeamMember/
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.TeamSortParm = sortOrder == "team" ? "team_desc" : "team";
            
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var teamMembers = from s in db.TeamMembers
                        select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                teamMembers = teamMembers.Where(s => s.Team.Name.ToUpper().Equals(searchString));

                teamMembers = !teamMembers.Any() ? db.TeamMembers.Where(s =>  s.Team.Name.ToUpper().Contains(searchString) || s.Player.FirstName.ToUpper().Contains(searchString.ToUpper()) ||
                    s.Player.LastName.ToUpper().Contains(searchString.ToUpper()) ) : teamMembers;
            }
            switch (sortOrder)
            {
                case "name_desc":
                    teamMembers = teamMembers.OrderByDescending(s => s.Player.LastName);
                    break;
                case "team_desc":
                    teamMembers = teamMembers.OrderByDescending(s => s.Team.Name);
                    break;
                case "team":
                    teamMembers = teamMembers.OrderBy(s => s.Team.Name);
                    break;
                default:  // Name ascending 
                    teamMembers = teamMembers.OrderBy(s => s.Player.LastName);
                    break;
            }

            const int pageSize = 35;
            int pageNumber = (page ?? 1);
            return View(teamMembers.ToPagedList(pageNumber, pageSize));
        }

        // GET: /TeamMember/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TeamMember teammember = db.TeamMembers.Find(id);
            if (teammember == null)
            {
                return HttpNotFound();
            }
            return View(teammember);
        }

        // GET: /TeamMember/Create
        public ActionResult Create()
        {
            ViewBag.PlayerId = GetPlayersSelectList();
            ViewBag.TeamId = new SelectList(db.Teams, "Id", "Name");
            return View();
        }

        // POST: /TeamMember/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,TeamId,PositionNumber,JerseyNumber,YearContractEnds,PlayerId,MigrationId")] TeamMember teammember)
        {
            if (ModelState.IsValid)
            {
                teammember.Id = Guid.NewGuid();
                db.TeamMembers.Add(teammember);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PlayerId = GetPlayersSelectList(teammember);
            ViewBag.TeamId = new SelectList(db.Teams, "Id", "Name", teammember.TeamId);
            return View(teammember);
        }

        // GET: /TeamMember/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TeamMember teammember = db.TeamMembers.Find(id);
            if (teammember == null)
            {
                return HttpNotFound();
            }
            ViewBag.PlayerId = GetPlayersSelectList(teammember);
            ViewBag.TeamId = new SelectList(db.Teams, "Id", "Name", teammember.TeamId);
            return View(teammember);
        }

        private SelectList GetPlayersSelectList(TeamMember teammember = null)
        {
            return teammember != null ? new SelectList(db.Players, "Id", "LastName", teammember.PlayerId) : new SelectList(db.Players, "Id", "LastName");
        }

        // POST: /TeamMember/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,TeamId,PositionNumber,JerseyNumber,YearContractEnds,PlayerId,MigrationId")] TeamMember teammember)
        {
            if (ModelState.IsValid)
            {
                db.Entry(teammember).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PlayerId = GetPlayersSelectList(teammember);
            ViewBag.TeamId = new SelectList(db.Teams, "Id", "Name", teammember.TeamId);
            return View(teammember);
        }

        // GET: /TeamMember/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TeamMember teammember = db.TeamMembers.Find(id);
            if (teammember == null)
            {
                return HttpNotFound();
            }
            return View(teammember);
        }

        // POST: /TeamMember/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            TeamMember teammember = db.TeamMembers.Find(id);
            db.TeamMembers.Remove(teammember);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
