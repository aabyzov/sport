﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sport.Configuration;
using Sport.Configuration.Models;

namespace Sport.Web.Configuration.Controllers
{
    public class LeagueController : Controller
    {
        private ConfigurationContext db = new ConfigurationContext();

        // GET: /League/
        public ActionResult Index()
        {
            var leagues = db.Leagues.Include(l => l.SportKind);
            return View(leagues.ToList());
        }

        // GET: /League/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            League league = db.Leagues.Find(id);
            if (league == null)
            {
                return HttpNotFound();
            }
            return View(league);
        }

        // GET: /League/Create
        public ActionResult Create()
        {
            ViewBag.SportKindId = new SelectList(db.SportKinds, "Id", "Name");
            return View();
        }

        // POST: /League/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name,SportKindId,MigrationId")] League league)
        {
            if (ModelState.IsValid)
            {
                league.Id = Guid.NewGuid();
                db.Leagues.Add(league);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SportKindId = new SelectList(db.SportKinds, "Id", "Name", league.SportKindId);
            return View(league);
        }

        // GET: /League/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            League league = db.Leagues.Find(id);
            if (league == null)
            {
                return HttpNotFound();
            }
            ViewBag.SportKindId = new SelectList(db.SportKinds, "Id", "Name", league.SportKindId);
            return View(league);
        }

        // POST: /League/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,SportKindId,MigrationId")] League league)
        {
            if (ModelState.IsValid)
            {
                db.Entry(league).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SportKindId = new SelectList(db.SportKinds, "Id", "Name", league.SportKindId);
            return View(league);
        }

        // GET: /League/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            League league = db.Leagues.Find(id);
            if (league == null)
            {
                return HttpNotFound();
            }
            return View(league);
        }

        // POST: /League/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            League league = db.Leagues.Find(id);
            db.Leagues.Remove(league);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
