﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sport.Configuration;
using Sport.Configuration.Models;


namespace Sport.Web.Configuration.Controllers
{
    public class SportKindController : Controller
    {
        private ConfigurationContext db = new ConfigurationContext();

        // GET: /SportKind/
        public ActionResult Index()
        {
            return View(db.SportKinds.ToList());
        }

        // GET: /SportKind/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SportKind sportkind = db.SportKinds.Find(id);
            if (sportkind == null)
            {
                return HttpNotFound();
            }
            return View(sportkind);
        }

        // GET: /SportKind/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /SportKind/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name")] SportKind sportkind)
        {
            if (ModelState.IsValid)
            {
                sportkind.Id = Guid.NewGuid();
                db.SportKinds.Add(sportkind);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sportkind);
        }

        // GET: /SportKind/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SportKind sportkind = db.SportKinds.Find(id);
            if (sportkind == null)
            {
                return HttpNotFound();
            }
            return View(sportkind);
        }

        // POST: /SportKind/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name")] SportKind sportkind)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sportkind).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sportkind);
        }

        // GET: /SportKind/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SportKind sportkind = db.SportKinds.Find(id);
            if (sportkind == null)
            {
                return HttpNotFound();
            }
            return View(sportkind);
        }

        // POST: /SportKind/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            SportKind sportkind = db.SportKinds.Find(id);
            db.SportKinds.Remove(sportkind);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
