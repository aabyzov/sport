﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Sport.Configuration;
using Sport.Configuration.Models;

namespace Sport.Web.Configuration.Controllers
{
    public class TeamController : Controller
    {
        private readonly ConfigurationContext _db = new ConfigurationContext();

        // GET: /Team/
        public ViewResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.LeagueSortParm = sortOrder == "league" ? "league_desc" : "league";
            ViewBag.ZoneSortParm = sortOrder == "zone" ? "zone_desc" : "zone";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var teams = from s in _db.Teams
                           select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                teams = teams.Where(s => s.Name.ToUpper().Contains(searchString.ToUpper()) ||
                    s.Zone.Name.ToUpper().Equals(searchString.ToUpper()));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    teams = teams.OrderByDescending(s => s.Name);
                    break;
                case "zone_desc":
                    teams = teams.OrderByDescending(s => s.Zone.Name);
                    break;
                case "zone":
                    teams = teams.OrderBy(t => t.Zone.Name);
                    break;
                case "league_desc":
                    teams = teams.OrderByDescending(s => s.League.Name);
                    break;
                case "league":
                    teams = teams.OrderBy(s => s.League.Name);
                    break;
                default:  // Name ascending 
                    teams = teams.OrderBy(s => s.Name);
                    break;
            }

            const int pageSize = 20;
            int pageNumber = (page ?? 1);
            return View(teams.ToPagedList(pageNumber, pageSize));
        }

        // GET: /Team/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = _db.Teams.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
            return View(team);
        }

        // GET: /Team/Create
        public ActionResult Create()
        {
            ViewBag.LeagueId = new SelectList(_db.Leagues, "Id", "Name");
            ViewBag.ZoneId = new SelectList(_db.Zones, "Id", "Name");
            return View();
        }

        // POST: /Team/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name,IsActive,Chemistry,OverallRating,LeagueId,MigrationId,ZoneId")] Team team)
        {
            if (ModelState.IsValid)
            {
                team.Id = Guid.NewGuid();
                _db.Teams.Add(team);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LeagueId = new SelectList(_db.Leagues, "Id", "Name", team.LeagueId);
            ViewBag.ZoneId = new SelectList(_db.Zones, "Id", "Name", team.ZoneId);
            return View(team);
        }

        // GET: /Team/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = _db.Teams.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
            ViewBag.LeagueId = new SelectList(_db.Leagues, "Id", "Name", team.LeagueId);
            ViewBag.ZoneId = new SelectList(_db.Zones, "Id", "Name", team.ZoneId);
            return View(team);
        }

        // POST: /Team/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,IsActive,Chemistry,OverallRating,LeagueId,MigrationId,ZoneId")] Team team)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(team).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LeagueId = new SelectList(_db.Leagues, "Id", "Name", team.LeagueId);
            ViewBag.ZoneId = new SelectList(_db.Zones, "Id", "Name", team.ZoneId);
            return View(team);
        }

        // GET: /Team/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Team team = _db.Teams.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
            return View(team);
        }

        // POST: /Team/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Team team = _db.Teams.Find(id);
            _db.Teams.Remove(team);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
