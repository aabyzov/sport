@echo off
::
:: Launch Agiblocks in IIS Express
::
SET IISEXPRESS=%ProgramFiles%\IIS Express\iisexpress.exe
IF EXIST '%ProgramFiles(86)%' SET IISEXPRESS=%ProgramFiles%\IIS Express\iisexpress.exe
::
ECHO ===========================================
ECHO Starting Agiblocks at http:\\localhost:14636
ECHO ===========================================
::
"%IISEXPRESS%" /path:"%CD%\AgiblocksWeb" /port:14636 /clr:v4.0