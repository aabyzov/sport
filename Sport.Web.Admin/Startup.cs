﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sport.Web.Admin.Startup))]
namespace Sport.Web.Admin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
