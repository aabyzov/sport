﻿using System.Web.Mvc;
using Sport.Common;

namespace Sport.Web.Admin.Utils
{
    public class MaintenanceModeAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (MaintenanceMode.IsInMaintainanceMode)
            {
                filterContext.Result = new ViewResult { ViewName = "MaintenanceMode" };
            }
            else
            {
                base.OnActionExecuting(filterContext);
            }
        }
    }
}