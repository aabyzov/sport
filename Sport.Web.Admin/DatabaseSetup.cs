﻿using System.Data.Entity;
using Sport.Common;

namespace Sport.Web.Admin
{
    internal static class DatabaseSetup
    {
        public static void Initialize()
        {
            Database.DefaultConnectionFactory = new ServiceConfigurationSettingConnectionFactory(Database.DefaultConnectionFactory);

            //Database.SetInitializer<ConferenceContext>(null);
        }
    }
}