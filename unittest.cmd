@echo off
:: test edit
::
:: Run this script from a VisualStudio command prompt. Syntax:
:: 
:: unittests [Configuration]
::
:: Where [Configuration] is a build configuration like Debug or Release (default).
::
:: If this build fails, try doing a full build first using build.cmd or rebuild.cmd 
::
SET configuration=%1
IF "%configuration%"=="" (
  SET configuration=Release
)
:: Uncomment the next if you have dotCover installed and want to use it for command line testing
::
::IF EXIST "C:\Program Files (x86)\JetBrains\dotCover\v2.2\Bin\dotcover.exe" (
::  SET params=;TEAMCITY_DOTCOVER_HOME="C:\Program Files (x86)\JetBrains\dotCover\v2.2\Bin"
::)
::
ECHO Building and running all Sport.*.UnitTests projects in %configuration% configuration
:: Run msbuild
:: Note that BuildTargets=Build enables incremental building for fast building
:: Remove that paramater or set to Rebuild to force cleaning and rebuilding the test projects
msbuild unittest.msbuild /m /t:test /p:Configuration=%configuration%;BuildTargets=Build%params% /p:MvcBuildViews=false