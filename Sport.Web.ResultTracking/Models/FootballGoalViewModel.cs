﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Sport.Domain.Models.Enums;

namespace Sport.Web.ResultTracking.Models
{

    public class FootballYellowCardViewModel : BaseFootballEventViewModel
    {

    }

    public class FootballInjuryViewModel : BaseFootballEventViewModel
    {
        public int MissedMatchCount { get; set; }

        [Display(Name = "Time")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        public DateTime? RecoveryDate { get; set; }
    }

    public class FootballRedCardViewModel : BaseFootballEventViewModel
    {
        public int MissedMatchCount { get; set; }
    }

    public class FootballMissedPenaltyViewModel : BaseFootballEventViewModel
    {
        [DisplayName("Details")]
        public MissedPenaltySituations DetailsId { get; set; }

        public SelectList DetailsList { get; set; }
    }

    public class FootballOwnGoalViewModel : BaseFootballEventViewModel
    {
        [DisplayName("Assistant")]
        public Guid AssistantId { get; set; }

        public SelectList AssistantPlayers { get; set; }
    }

    public class FootballGoalViewModel : BaseFootballEventViewModel
    {
        [DisplayName("Assistant")]
        public Guid AssistantId { get; set; }

        [DisplayName("How scored")]
        public HowScored HowScoredId { get; set; }

        public SelectList HowScoredList { get; set; }

        [DisplayName("Body part")]
        public BodyPart BodyPartId { get; set; }

        public SelectList BodyPartList { get; set; }

        [DisplayName("Goal distance (if 0 it means that it was scored from inside of penalty area)")]
        public int GoalDistance { get; set; }

        public SelectList AssistantPlayers { get; set; }
    }

    public class BaseFootballEventViewModel
    {
        [Required]
        [Range(1, 100)]
        public int Minute { get; set; }

        [DisplayName("Player")]
        public Guid PlayerId { get; set; }

        public SelectList Players { get; set; }

        public Guid FixtureParticipantId { get; set; }
    }
}