﻿using System;

namespace Sport.Web.ResultTracking.Models
{
    public class FixtureEditViewModel
    {
        public Guid FixtureId { get; set; }
        public DateTime Date { get; set; }
        public string Location { get; set; }
        public bool IsFriendly { get; set; }
        public Guid HomeParticipantId { get; set; }
        public Guid AwayParticipantId { get; set; }
        public BaseEventViewModel[] AllEvents { get; set; }
    }

    public class EventEditViewModel
    {
        
    }
}