﻿namespace Sport.Web.ResultTracking.Models
{
    public class FixturesViewModel
    {
        public string Group { get; set; }
        public BaseEventViewModel[] AllEvents { get; set; }
    }
}