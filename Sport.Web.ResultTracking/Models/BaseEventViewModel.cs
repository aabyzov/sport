﻿using System;

namespace Sport.Web.ResultTracking.Models
{
    public class BaseEventViewModel
    {
        public string HomeTeam { get; set; }
        public string AwayTeam { get; set; }
        public string Score { get; set; }

        public Guid FixtureId { get; set; }
    }

    public class EventViewModel : BaseEventViewModel
    {
        public string HomeType { get; set; }
        public string AwayType { get; set; }
        
        public Guid EventId { get; set; }
    }

    public class ResultViewModel : BaseEventViewModel
    {
    }

    public class PlayerOutOfFixtureViewModel : BaseEventViewModel
    {
        public string HomeReason { get; set; }
        public string AwayReason { get; set; }
    }
}