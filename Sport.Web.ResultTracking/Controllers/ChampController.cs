﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sport.Domain.Models;
using Sport.Domain.UoW;

namespace Sport.Web.ResultTracking.Controllers
{
    public class ChampController : Controller
    {
        private ISportUnitOfWork _sportUnitOfWork;
        private SportContext db = new SportContext();
        //private ISportUnitOfWork context = new SportUnitOfWork();
        
        public ChampController(ISportUnitOfWork sportUnitOfWork)
        {
            _sportUnitOfWork = sportUnitOfWork;
        }

        // GET: /Champ/
        public ActionResult Index(string champSeasonId)
        {
            var seasonId = new Guid(champSeasonId);
            var champs = _sportUnitOfWork.Champs.WithFixturesAndTeams().Where(x => x.ChampSeasonId == seasonId).ToList().OrderBy(x => x.Start);
            return View(champs.ToList());
        }

        // GET: /Champ/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Champ champ = db.Champs.Find(id);
            if (champ == null)
            {
                return HttpNotFound();
            }
            return View(champ);
        }

        // GET: /Champ/Create
        public ActionResult Create()
        {
            ViewBag.ChampSeasonId = new SelectList(db.ChampSeasons, "Id", "Name");
            ViewBag.ZoneId = new SelectList(db.Zones, "Id", "Name");
            return View();
        }

        // POST: /Champ/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name,ChampSeasonId,MigrationId,ZoneId,YellowsToMissNextMatch")] Champ champ)
        {
            if (ModelState.IsValid)
            {
                champ.Id = Guid.NewGuid();
                db.Champs.Add(champ);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ChampSeasonId = new SelectList(db.ChampSeasons, "Id", "Name", champ.ChampSeasonId);
            ViewBag.ZoneId = new SelectList(db.Zones, "Id", "Name", champ.ZoneId);
            return View(champ);
        }

        // GET: /Champ/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Champ champ = db.Champs.Find(id);
            if (champ == null)
            {
                return HttpNotFound();
            }
            ViewBag.ChampSeasonId = new SelectList(db.ChampSeasons, "Id", "Name", champ.ChampSeasonId);
            ViewBag.ZoneId = new SelectList(db.Zones, "Id", "Name", champ.ZoneId);
            return View(champ);
        }

        // POST: /Champ/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,ChampSeasonId,MigrationId,ZoneId,YellowsToMissNextMatch")] Champ champ)
        {
            if (ModelState.IsValid)
            {
                db.Entry(champ).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ChampSeasonId = new SelectList(db.ChampSeasons, "Id", "Name", champ.ChampSeasonId);
            ViewBag.ZoneId = new SelectList(db.Zones, "Id", "Name", champ.ZoneId);
            return View(champ);
        }

        // GET: /Champ/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Champ champ = db.Champs.Find(id);
            if (champ == null)
            {
                return HttpNotFound();
            }
            return View(champ);
        }

        // POST: /Champ/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Champ champ = db.Champs.Find(id);
            db.Champs.Remove(champ);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
