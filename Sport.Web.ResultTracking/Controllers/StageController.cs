﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sport.Domain.Models;

namespace Sport.Web.ResultTracking.Controllers
{
    public class StageController : Controller
    {
        private SportContext db = new SportContext();

        // GET: /Stage/
        public ActionResult Index(string champId)
        {
            var champGuid = new Guid(champId);
            var stages = db.Stages.Include(s => s.Champ).Include(s => s.Parent).Where(x => x.ChampId == champGuid).ToList().OrderBy(x => x.StartDate);
            return View(stages.ToList());
        }

        // GET: /Stage/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stage stage = db.Stages.Find(id);
            if (stage == null)
            {
                return HttpNotFound();
            }
            return View(stage);
        }

        // GET: /Stage/Create
        public ActionResult Create()
        {
            ViewBag.ChampId = new SelectList(db.Champs, "Id", "Name");
            ViewBag.StageId = new SelectList(db.Stages, "Id", "Name");
            return View();
        }

        // POST: /Stage/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name,RoundCount,ChampId,StageId,MigrationId")] Stage stage)
        {
            if (ModelState.IsValid)
            {
                stage.Id = Guid.NewGuid();
                db.Stages.Add(stage);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ChampId = new SelectList(db.Champs, "Id", "Name", stage.ChampId);
            ViewBag.StageId = new SelectList(db.Stages, "Id", "Name", stage.StageId);
            return View(stage);
        }

        // GET: /Stage/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stage stage = db.Stages.Find(id);
            if (stage == null)
            {
                return HttpNotFound();
            }
            ViewBag.ChampId = new SelectList(db.Champs, "Id", "Name", stage.ChampId);
            ViewBag.StageId = new SelectList(db.Stages, "Id", "Name", stage.StageId);
            return View(stage);
        }

        // POST: /Stage/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,RoundCount,ChampId,StageId,MigrationId")] Stage stage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ChampId = new SelectList(db.Champs, "Id", "Name", stage.ChampId);
            ViewBag.StageId = new SelectList(db.Stages, "Id", "Name", stage.StageId);
            return View(stage);
        }

        // GET: /Stage/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stage stage = db.Stages.Find(id);
            if (stage == null)
            {
                return HttpNotFound();
            }
            return View(stage);
        }

        // POST: /Stage/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Stage stage = db.Stages.Find(id);
            db.Stages.Remove(stage);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
