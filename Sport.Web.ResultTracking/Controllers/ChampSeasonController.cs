﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Sport.Domain.Models;

namespace Sport.Web.ResultTracking.Controllers
{
    public class ChampSeasonController : Controller
    {
        private SportContext db = new SportContext();

        // GET: /ChampSeason/
        public ActionResult Index()
        {
            var champseasons = db.ChampSeasons.Include(c => c.ChampType).Include(c => c.Parent);
            return View(champseasons.ToList());
        }

        // GET: /ChampSeason/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChampSeason champseason = db.ChampSeasons.Find(id);
            if (champseason == null)
            {
                return HttpNotFound();
            }
            return View(champseason);
        }

        // GET: /ChampSeason/Create
        public ActionResult Create()
        {
            ViewBag.ChampTypeId = new SelectList(db.ChampTypes, "Id", "Name");
            ViewBag.ChampSeasonId = new SelectList(db.ChampSeasons, "Id", "Name");
            return View();
        }

        // POST: /ChampSeason/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Id,Name,Start,End,ChampTypeId,ChampSeasonId,MigrationId")] ChampSeason champseason)
        {
            if (ModelState.IsValid)
            {
                champseason.Id = Guid.NewGuid();
                db.ChampSeasons.Add(champseason);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ChampTypeId = new SelectList(db.ChampTypes, "Id", "Name", champseason.ChampTypeId);
            ViewBag.ChampSeasonId = new SelectList(db.ChampSeasons, "Id", "Name", champseason.ChampSeasonId);
            return View(champseason);
        }

        // GET: /ChampSeason/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChampSeason champseason = db.ChampSeasons.Find(id);
            if (champseason == null)
            {
                return HttpNotFound();
            }
            ViewBag.ChampTypeId = new SelectList(db.ChampTypes, "Id", "Name", champseason.ChampTypeId);
            ViewBag.ChampSeasonId = new SelectList(db.ChampSeasons, "Id", "Name", champseason.ChampSeasonId);
            return View(champseason);
        }

        // POST: /ChampSeason/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Id,Name,Start,End,ChampTypeId,ChampSeasonId,MigrationId")] ChampSeason champseason)
        {
            if (ModelState.IsValid)
            {
                db.Entry(champseason).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ChampTypeId = new SelectList(db.ChampTypes, "Id", "Name", champseason.ChampTypeId);
            ViewBag.ChampSeasonId = new SelectList(db.ChampSeasons, "Id", "Name", champseason.ChampSeasonId);
            return View(champseason);
        }

        // GET: /ChampSeason/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChampSeason champseason = db.ChampSeasons.Find(id);
            if (champseason == null)
            {
                return HttpNotFound();
            }
            return View(champseason);
        }

        // POST: /ChampSeason/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            ChampSeason champseason = db.ChampSeasons.Find(id);
            db.ChampSeasons.Remove(champseason);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
