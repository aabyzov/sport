﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Sport.Domain.Models;
using Sport.Domain.Models.Enums;
using Sport.Domain.UoW;
using Sport.Web.Common.Extensions;
using Sport.Web.ResultTracking.Models;
using Sport.Web.Common;

namespace Sport.Web.ResultTracking.Controllers
{
    public class FixtureController : Controller
    {
        private ISportUnitOfWork _sportUnitOfWork;

        public FixtureController(ISportUnitOfWork sportUnitOfWork)
        {
            _sportUnitOfWork = sportUnitOfWork;
        }

        //
        // GET: /Fixture/
        public ActionResult Index(string groupId)
        {
            var groupGuid = new Guid(groupId);

            var fixtures = _sportUnitOfWork.Fixtures.WithEventsAndTeams().Where(x => x.FixtureParticipants.FirstOrDefault().GroupTeam.Group.Id == groupGuid).ToList().OrderBy(x => x.Date);
            var model = new FixturesViewModel()
            {
                Group = fixtures.FirstOrDefault() != null ? fixtures.FirstOrDefault().Group.Name + " <- " + fixtures.FirstOrDefault().Group.Stage.Name + " <- " + fixtures.FirstOrDefault().Champ.Name + " <- " + fixtures.FirstOrDefault().Champ.ChampSeason.Name : "No Fixtures yet"
            };

            var events = BindFixtureEvents(fixtures);
            model.AllEvents = events.ToArray();

            return View("Index", model);
        }

        public ActionResult List()
        {
            var fixtures = _sportUnitOfWork.Fixtures.WithEventsAndTeams().OrderBy(x => x.Date).ToList();
            var model = new FixturesViewModel()
            {
                Group = string.Empty
            };

            var events = BindFixtureEvents(fixtures);
            model.AllEvents = events.ToArray();

            return View("Index", model);
        }

        public ActionResult FinishMatch(string fixtureId)
        {
            var fixtureGuid = new Guid(fixtureId);
            var fixture = _sportUnitOfWork.Fixtures.WithEventsAndTeams().FirstOrDefault(x => x.Id == fixtureGuid);
            fixture.Home.Score = fixture.HomeScoreBasedOnEvents;
            fixture.Away.Score = fixture.AwayScoreBasedOnEvents;
            _sportUnitOfWork.Save();
            return RedirectToAction("Index", "Fixture", new {groupId = fixture.Home.GroupTeam.Group.Id});
        }

        private List<BaseEventViewModel> BindFixtureEvents(IEnumerable<Fixture> fixtures, bool onlyEvents = false)
        {
            var events = new List<BaseEventViewModel>();
            foreach (var fixture in fixtures)
            {
                var eventViewModel = new ResultViewModel()
                {
                    HomeTeam = "(" + fixture.Champ.Name + " - " + "Tour" + fixture.MatchDay + " - " + fixture.Date.Value.ToString("dd/MM/yyyy HH:mm") + ") " + fixture.HomeTeamName,
                    AwayTeam = fixture.AwayTeamName,
                    Score = fixture.HomeScore + " : " + fixture.AwayScore,
                    FixtureId = fixture.Id
                };
                events.Add(eventViewModel);

                if (!onlyEvents)
                {
                    if (fixture.Home.OutOfFixturePlayers.Any())
                    {
                        foreach (var player in fixture.Home.OutOfFixturePlayers)
                        {
                            var outOfFixture = new PlayerOutOfFixtureViewModel()
                            {
                                HomeReason = player.Reason.ToString(),
                                HomeTeam = player.Player.FullName + string.Format(" ({0})", player.MissedMatchCount),
                                AwayTeam = string.Empty,
                                Score = string.Empty,
                                FixtureId = fixture.Id
                            };
                            events.Add(outOfFixture);
                        }
                    }

                    if (fixture.Away.OutOfFixturePlayers.Any())
                    {
                        foreach (var player in fixture.Away.OutOfFixturePlayers)
                        {
                            var outOfFixture = new PlayerOutOfFixtureViewModel()
                            {
                                AwayReason = player.Reason.ToString(),
                                AwayTeam = player.Player.FullName + string.Format(" ({0})", player.MissedMatchCount),
                                HomeTeam = string.Empty,
                                Score = string.Empty,
                                FixtureId = fixture.Id
                            };
                            events.Add(outOfFixture);
                        }
                    }
                }

                if (fixture.FootballEvents.Any())
                {
                    var homeGoals = 0;
                    var awayGoals = 0;
                    foreach (var fixtureEvent in fixture.FootballEvents)
                    {
                        fixture.IncreaseHomeOrAwayGoal(fixtureEvent, ref homeGoals, ref awayGoals);

                        var footballEventViewModel = new EventViewModel()
                        {
                            HomeType =
                                fixtureEvent.FixtureParticipant.IsHome
                                    ? fixtureEvent.GetType().Name.Split('_')[0]
                                    : string.Empty,
                            AwayType =
                                !fixtureEvent.FixtureParticipant.IsHome
                                    ? fixtureEvent.GetType().Name.Split('_')[0]
                                    : string.Empty,
                            HomeTeam =
                                fixtureEvent.FixtureParticipant.IsHome
                                    ? DisplayPlayerEventInfo(fixtureEvent)
                                    : string.Empty,
                            AwayTeam =
                                !fixtureEvent.FixtureParticipant.IsHome
                                    ? DisplayPlayerEventInfo(fixtureEvent)
                                    : string.Empty,
                            Score =
                                fixtureEvent is FootballGoal || fixtureEvent is FootballOwnGoal
                                    ? string.Format("( {0} : {1} )", homeGoals, awayGoals)
                                    : string.Empty,
                            FixtureId = fixture.Id,
                            EventId = fixtureEvent.Id
                        };
                        events.Add(footballEventViewModel);
                    }
                }

                if (!onlyEvents)
                {
                    // add empty row
                    var emptyRow = new EventViewModel();
                    events.Add(emptyRow);
                }
            }
            return events;
        }

        private static string DisplayPlayerEventInfo(FootballEvent fixtureEvent)
        {
            if (fixtureEvent is FootballGoal)
                return DisplayPlayerEventInfo(fixtureEvent as FootballGoal);
            if (fixtureEvent is FootballOwnGoal)
                return DisplayPlayerEventInfo(fixtureEvent as FootballOwnGoal);
            if (fixtureEvent is FootballMissedPenalty)
                return DisplayPlayerEventInfo(fixtureEvent as FootballMissedPenalty);
            if (fixtureEvent is FootballRedCard)
                return DisplayPlayerEventInfo(fixtureEvent as FootballRedCard);
            if (fixtureEvent is FootballInjury)
                return DisplayPlayerEventInfo(fixtureEvent as FootballInjury);
            return fixtureEvent.Player.FullName + " '" +
                   ((FootballEventTime)fixtureEvent.EventTime).Minute;
        }

        private static string DisplayPlayerEventInfo(FootballGoal fixtureEvent)
        {
            return fixtureEvent.Player.FullName + (fixtureEvent.Assistant != null ? " (" + fixtureEvent.Assistant.FullName + ")" : string.Empty) + " '" +
                ((FootballEventTime)fixtureEvent.EventTime).Minute + (DisplayBodyPart(fixtureEvent)) +
                (DisplayHowScored(fixtureEvent)) +
                (fixtureEvent.GoalDistance == 0 || fixtureEvent.GoalDistance == null || fixtureEvent.GoalDistance < 16.5 ? string.Empty : " from " + fixtureEvent.GoalDistance + "m");
        }

        private static string DisplayPlayerEventInfo(FootballOwnGoal fixtureEvent)
        {
            return fixtureEvent.Player.FullName + (fixtureEvent.Assistant != null ? " (" + fixtureEvent.Assistant.FullName + ")" : string.Empty) + " '" +
                ((FootballEventTime)fixtureEvent.EventTime).Minute;
        }

        private static string DisplayPlayerEventInfo(FootballMissedPenalty fixtureEvent)
        {
            return fixtureEvent.Player.FullName + (" (" + fixtureEvent.Details + ")") + " '" +
                ((FootballEventTime)fixtureEvent.EventTime).Minute;
        }

        private static string DisplayPlayerEventInfo(FootballRedCard fixtureEvent)
        {
            return fixtureEvent.Player.FullName + (" (" + fixtureEvent.MissedMatchCountRedCard + ")") + " '" +
                ((FootballEventTime)fixtureEvent.EventTime).Minute;
        }

        private static string DisplayPlayerEventInfo(FootballInjury fixtureEvent)
        {
            return fixtureEvent.Player.FullName + (" (" + fixtureEvent.MissedMatchCountInjury + ")") + " '" +
                ((FootballEventTime)fixtureEvent.EventTime).Minute;
        }

        private static string DisplayBodyPart(FootballGoal fixtureEvent)
        {
            switch (fixtureEvent.BodyPart)
            {
                case BodyPart.Foot:
                    return string.Empty;
                case BodyPart.Head:
                    return " (H)";
                case BodyPart.Heal:
                    return " (heal)";
            }
            return string.Empty;
        }

        private static string DisplayHowScored(FootballGoal fixtureEvent)
        {
            switch (fixtureEvent.HowScored)
            {
                case HowScored.FromPlay:
                    return string.Empty;
                case HowScored.FromPenalty:
                    return " (P)";
                case HowScored.DirectlyFromFreeKick:
                    return " (FK)";
                case HowScored.AfterGoalkeeperSaved:
                    return " (after gk)";
                case HowScored.AfterMissedPenalty:
                    return " (after missed P)";
                case HowScored.LoftedKick:
                    return " (lofted)";
                case HowScored.BicycleKick:
                    return " (bicycle)";
            }
            return fixtureEvent.HowScored.ToString();
        }

        public ActionResult Edit(Guid fixtureId)
        {
            var model = new FixtureEditViewModel();
            var fixture = _sportUnitOfWork.Fixtures.WithEventsAndTeams().FirstOrDefault(x => x.Id == fixtureId);
            if (fixture == null) throw new Exception("No such fixture!");
            model.FixtureId = fixtureId;
            model.Location = fixture.Location;
            model.Date = fixture.Date != null ? (DateTime)fixture.Date : DateTime.Now;
            model.IsFriendly = fixture.IsFriendly;
            model.HomeParticipantId = fixture.Home.Id;
            model.AwayParticipantId = fixture.Away.Id;
            var events = BindFixtureEvents(new List<Fixture>() { fixture }, true);
            model.AllEvents = events.ToArray();

            return View("Edit", model);
        }

        //[HttpPost]
        public ActionResult DeleteEvent(Guid eventId)
        {
            var fixtureEvent = _sportUnitOfWork.FixtureEvents.
                //FindById(eventId); 
                AllIncluding(x => x.FixtureParticipant.Fixture).FirstOrDefault(x => x.Id == eventId);
            var fixtureId = fixtureEvent.FixtureParticipant.Fixture.Id;
            _sportUnitOfWork.FixtureEvents.Delete(fixtureEvent);
            _sportUnitOfWork.Save();
            return RedirectToAction("Edit", new {fixtureId = fixtureId});
        }


        public ActionResult AddGoal(Guid participantId)
        {
            var participant = _sportUnitOfWork.FixtureParticipants.FindById(participantId);
            var players =          GetParticipantTeamPlayers(participant);
            var assistantPlayers = GetParticipantTeamPlayers(participant);

            assistantPlayers.Insert(0,
                new Player() { Id = Guid.Empty, FirstName = string.Empty, LastName = string.Empty });
            var model = new FootballGoalViewModel()
            {
                FixtureParticipantId = participantId,
                Players = new SelectList(players.MapToSelectItems(x => x.FullName, x => x.Id.ToString(), x => false), "Value", "Text"),
                AssistantPlayers = new SelectList(assistantPlayers.MapToSelectItems(x => x.FullName, x => x.Id.ToString(), x => false), "Value", "Text"),
                BodyPartList = BodyPart.Foot.ToSelectList(),
                HowScoredList = HowScored.FromPlay.ToSelectList()
            };
            return PartialView("AddGoal", model);
        }

        public ActionResult AddMissedPenalty(Guid participantId)
        {
            var participant = _sportUnitOfWork.FixtureParticipants.FindById(participantId);
            var players = GetParticipantTeamPlayers(participant);
            var assistantPlayers = GetParticipantTeamPlayers(participant);

            assistantPlayers.Insert(0,
                new Player() { Id = Guid.Empty, FirstName = string.Empty, LastName = string.Empty });
            var model = new FootballMissedPenaltyViewModel()
            {
                FixtureParticipantId = participantId,
                Players = new SelectList(players.MapToSelectItems(x => x.FullName, x => x.Id.ToString(), x => false), "Value", "Text"),
                DetailsList = MissedPenaltySituations.GkSavedInTheLeftCorner.ToSelectList()
            };
            return PartialView("AddMissedPenalty", model);
        }

        public ActionResult AddYellowCard(Guid participantId)
        {
            var participant = _sportUnitOfWork.FixtureParticipants.FindById(participantId);
            var players = GetParticipantTeamPlayers(participant);
            var assistantPlayers = GetParticipantTeamPlayers(participant);

            assistantPlayers.Insert(0,
                new Player() { Id = Guid.Empty, FirstName = string.Empty, LastName = string.Empty });
            var model = new FootballYellowCardViewModel()
            {
                FixtureParticipantId = participantId,
                Players = new SelectList(players.MapToSelectItems(x => x.FullName, x => x.Id.ToString(), x => false), "Value", "Text")
            };
            return PartialView("AddYellowCard", model);
        }

        public ActionResult AddRedCard(Guid participantId)
        {
            var participant = _sportUnitOfWork.FixtureParticipants.FindById(participantId);
            var players = GetParticipantTeamPlayers(participant);
            var assistantPlayers = GetParticipantTeamPlayers(participant);

            assistantPlayers.Insert(0,
                new Player() { Id = Guid.Empty, FirstName = string.Empty, LastName = string.Empty });
            var model = new FootballRedCardViewModel()
            {
                FixtureParticipantId = participantId,
                Players = new SelectList(players.MapToSelectItems(x => x.FullName, x => x.Id.ToString(), x => false), "Value", "Text")
            };
            return PartialView("AddRedCard", model);
        }

        public ActionResult AddInjury(Guid participantId)
        {
            var participant = _sportUnitOfWork.FixtureParticipants.FindById(participantId);
            var players = GetParticipantTeamPlayers(participant);
            var assistantPlayers = GetParticipantTeamPlayers(participant);

            assistantPlayers.Insert(0,
                new Player() { Id = Guid.Empty, FirstName = string.Empty, LastName = string.Empty });
            var model = new FootballInjuryViewModel()
            {
                FixtureParticipantId = participantId,
                Players = new SelectList(players.MapToSelectItems(x => x.FullName, x => x.Id.ToString(), x => false), "Value", "Text")
            };
            return PartialView("AddInjury", model);
        }

        private List<Player> GetParticipantTeamPlayers(FixtureParticipant participant)
        {
            var players =_sportUnitOfWork.TeamMembers.FindAllBy(x => x.Team.Id == participant.GroupTeam.TeamId).Select(x => x.Player).OrderBy(x => x.LastName).ToList();
            var outOfFixturePlayers = participant.OutOfFixturePlayers;
            var result = players.RemoveAll(x => outOfFixturePlayers.FirstOrDefault(ofp => ofp.PlayerId == x.Id) != null);
            return players;
        }

        [HttpPost]
        public ActionResult SaveGoal(FootballGoalViewModel model)
        {
            var participant = _sportUnitOfWork.FixtureParticipants.AllIncluding(x => x.Events.Select(e => e.EventTime)).FirstOrDefault(fp => fp.Id == model.FixtureParticipantId);
            var currentEvents = _sportUnitOfWork.FixtureEvents.FootballEvents().Where(x => x.FixtureParticipantId == participant.Id || x.FixtureParticipantId == participant.Opponent.Id);
            Func<FixtureEvent, bool> minPredicate = x =>
            {
                var footballEventTime = x.EventTime as FootballEventTime;
                return footballEventTime != null && footballEventTime.Minute == model.Minute;
            };
            var eventOrder = currentEvents.Any(minPredicate)
                ? currentEvents.Where(minPredicate)
                .Max(x => x.EventOrder) : 0;
            participant.Events.Add(new FootballGoal()
            {
                EventTime = new FootballEventTime(model.Minute),
                EventOrder = eventOrder + 1,
                AssistantId = model.AssistantId != Guid.Empty ? model.AssistantId : (Guid?)null,
                PlayerId = model.PlayerId,
                BodyPart = model.BodyPartId,
                HowScored = model.HowScoredId,
                GoalDistance = model.GoalDistance
            });
            _sportUnitOfWork.Save();

            return new JsonResult() { Data = "Goal saved successfully!" };
        }

        [HttpPost]
        public ActionResult SaveOwnGoal(FootballOwnGoalViewModel model)
        {
            var participant = _sportUnitOfWork.FixtureParticipants.AllIncluding(x => x.Events.Select(e => e.EventTime)).FirstOrDefault(fp => fp.Id == model.FixtureParticipantId);
            var currentEvents = _sportUnitOfWork.FixtureEvents.FootballEvents().Where(x => x.FixtureParticipantId == participant.Id || x.FixtureParticipantId == participant.Opponent.Id);
            Func<FixtureEvent, bool> minPredicate = x =>
            {
                var footballEventTime = x.EventTime as FootballEventTime;
                return footballEventTime != null && footballEventTime.Minute == model.Minute;
            };
            var eventOrder = currentEvents.Any(minPredicate)
                ? currentEvents.Where(minPredicate)
                .Max(x => x.EventOrder) : 0;
            participant.Events.Add(new FootballOwnGoal()
            {
                EventTime = new FootballEventTime(model.Minute),
                EventOrder = eventOrder + 1,
                AssistantId = model.AssistantId != Guid.Empty ? model.AssistantId : (Guid?)null,
                PlayerId = model.PlayerId
            });
            _sportUnitOfWork.Save();

            return new JsonResult() { Data = "Own goal saved successfully!" };
        }

        [HttpPost]
        public ActionResult SaveMissedPenalty(FootballMissedPenaltyViewModel model)
        {
            var participant = _sportUnitOfWork.FixtureParticipants.AllIncluding(x => x.Events.Select(e => e.EventTime)).FirstOrDefault(fp => fp.Id == model.FixtureParticipantId);
            var currentEvents = _sportUnitOfWork.FixtureEvents.FootballEvents().Where(x => x.FixtureParticipantId == participant.Id || x.FixtureParticipantId == participant.Opponent.Id);
            Func<FixtureEvent, bool> minPredicate = x =>
            {
                var footballEventTime = x.EventTime as FootballEventTime;
                return footballEventTime != null && footballEventTime.Minute == model.Minute;
            };
            var eventOrder = currentEvents.Any(minPredicate)
                ? currentEvents.Where(minPredicate)
                .Max(x => x.EventOrder) : 0;
            participant.Events.Add(new FootballMissedPenalty()
            {
                EventTime = new FootballEventTime(model.Minute),
                EventOrder = eventOrder + 1,
                Details = model.DetailsId,
                PlayerId = model.PlayerId
            });
            _sportUnitOfWork.Save();

            return new JsonResult() { Data = "Missed penalty saved successfully!" };
        }

        [HttpPost]
        public ActionResult SaveYellowCard(FootballMissedPenaltyViewModel model)
        {
            var participant = _sportUnitOfWork.FixtureParticipants.AllIncluding(x => x.Events.Select(e => e.EventTime)).FirstOrDefault(fp => fp.Id == model.FixtureParticipantId);
            var currentEvents = _sportUnitOfWork.FixtureEvents.FootballEvents().Where(x => x.FixtureParticipantId == participant.Id || x.FixtureParticipantId == participant.Opponent.Id);
            Func<FixtureEvent, bool> minPredicate = x =>
            {
                var footballEventTime = x.EventTime as FootballEventTime;
                return footballEventTime != null && footballEventTime.Minute == model.Minute;
            };
            var eventOrder = currentEvents.Any(minPredicate)
                ? currentEvents.Where(minPredicate)
                .Max(x => x.EventOrder) : 0;
            participant.Events.Add(new FootballYellowCard()
            {
                EventTime = new FootballEventTime(model.Minute),
                EventOrder = eventOrder + 1,
                PlayerId = model.PlayerId
            });
            _sportUnitOfWork.Save();

            return new JsonResult() { Data = "Yellow card saved successfully!" };
        }

        [HttpPost]
        public ActionResult SaveRedCard(FootballRedCardViewModel model)
        {
            var participant = _sportUnitOfWork.FixtureParticipants.AllIncluding(x => x.Events.Select(e => e.EventTime)).FirstOrDefault(fp => fp.Id == model.FixtureParticipantId);
            var currentEvents = _sportUnitOfWork.FixtureEvents.FootballEvents().Where(x => x.FixtureParticipantId == participant.Id || x.FixtureParticipantId == participant.Opponent.Id);
            Func<FixtureEvent, bool> minPredicate = x =>
            {
                var footballEventTime = x.EventTime as FootballEventTime;
                return footballEventTime != null && footballEventTime.Minute == model.Minute;
            };
            var eventOrder = currentEvents.Any(minPredicate)
                ? currentEvents.Where(minPredicate)
                .Max(x => x.EventOrder) : 0;
            participant.Events.Add(new FootballRedCard()
            {
                EventTime = new FootballEventTime(model.Minute),
                EventOrder = eventOrder + 1,
                PlayerId = model.PlayerId,
                MissedMatchCountRedCard = model.MissedMatchCount
            });
            _sportUnitOfWork.Save();

            return new JsonResult() { Data = "Red card saved successfully!" };
        }

        [HttpPost]
        public ActionResult SaveInjury(FootballInjuryViewModel model)
        {
            var participant = _sportUnitOfWork.FixtureParticipants.AllIncluding(x => x.Events.Select(e => e.EventTime)).FirstOrDefault(fp => fp.Id == model.FixtureParticipantId);
            var currentEvents = _sportUnitOfWork.FixtureEvents.FootballEvents().Where(x => x.FixtureParticipantId == participant.Id || x.FixtureParticipantId == participant.Opponent.Id);
            Func<FixtureEvent, bool> minPredicate = x =>
            {
                var footballEventTime = x.EventTime as FootballEventTime;
                return footballEventTime != null && footballEventTime.Minute == model.Minute;
            };
            var eventOrder = currentEvents.Any(minPredicate)
                ? currentEvents.Where(minPredicate)
                .Max(x => x.EventOrder) : 0;
            participant.Events.Add(new FootballInjury()
            {
                EventTime = new FootballEventTime(model.Minute),
                EventOrder = eventOrder + 1,
                PlayerId = model.PlayerId,
                MissedMatchCountInjury = model.MissedMatchCount
            });
            _sportUnitOfWork.Save();

            return new JsonResult() { Data = "Injury saved successfully!" };
        }
        
        public ActionResult AddOwnGoal(Guid participantId)
        {
            var participant = _sportUnitOfWork.FixtureParticipants.FindById(participantId);
            var players = GetParticipantTeamPlayers(participant.Opponent);
            var assistantPlayers = GetParticipantTeamPlayers(participant);

            assistantPlayers.Insert(0,
                new Player() { Id = Guid.Empty, FirstName = string.Empty, LastName = string.Empty });
            var model = new FootballOwnGoalViewModel()
            {
                FixtureParticipantId = participantId,
                Players = new SelectList(players.MapToSelectItems(x => x.FullName, x => x.Id.ToString(), x => false), "Value", "Text"),
                AssistantPlayers = new SelectList(assistantPlayers.MapToSelectItems(x => x.FullName, x => x.Id.ToString(), x => false), "Value", "Text")
            };
            return PartialView("AddOwnGoal", model);
        }
    }
}