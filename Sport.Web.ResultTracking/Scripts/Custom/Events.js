﻿$(function () {
    

    $('.dialog').click(function () {
        $('<div>Loading ...</div>').load(this.href, function(e) {

            $('#result').html(e);

            $("#result").dialog({
                width: 500,
                height: 440,
                modal: true,
                hide: { effect: "fadeOut", duration: 1000 }
            });
        });
       
        return false;
    });

    $("#dialog-confirm").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        height: 180,
    });

    $(".deleteLink").click(function (e) {
        e.preventDefault();
        var targetUrl = $(this).attr("href");

        $("#dialog-confirm").dialog({
            buttons: {
                "Confirm": function () {
                    window.location.href = targetUrl;
                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });

        $("#dialog-confirm").dialog("open");
    });

    $(".finishMatch").click(function (e) {
        e.preventDefault();
        var targetUrl = $(this).attr("href");

        $("#dialog-confirmAction").dialog({
            buttons: {
                "Confirm": function () {
                    window.location.href = targetUrl;
                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });

        $("#dialog-confirmAction").dialog("open");
    });

    SaveFixtureEventSuccess = function(e) {
        $('#result').dialog('close');
        location.reload();
    };

    DeleteEventSuccess = function(e) {
        debugger;
    };
});