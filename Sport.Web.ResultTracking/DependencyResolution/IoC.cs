// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IoC.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


using Sport.Common.Core;
using Sport.Domain.Models;
using Sport.Domain.Repositories;
using Sport.Domain.UoW;
using StructureMap;
namespace Sport.Web.ResultTracking.DependencyResolution {
    public static class IoC {
        public static IContainer Initialize() {
            ObjectFactory.Initialize(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.AssemblyContainingType(typeof(SportKindRepository));
                    scan.AddAllTypesOf(typeof(IRepository<>));
                    scan.ConnectImplementationsToTypesClosing(typeof(IRepository<>));
                });
                x.For<ISportUnitOfWork>().HybridHttpOrThreadLocalScoped().Use<SportUnitOfWork>();
                x.For<ILogger>().Singleton().Use(new Logger("test", "pref"));
                x.For<SportContext>().HttpContextScoped().Use(new SportContext());

            });
            return ObjectFactory.Container;
        }
    }
}