using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Sport.Common.Core;
using Sport.Domain.Repositories;
using Sport.Domain.UoW;
using ILogger = Microsoft.Owin.Logging.ILogger;

namespace Sport.Web.ResultTracking.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

        //     container.Configure(x => { x.AddRegistry<>() }) Scan(scan =>
        //{
        //    scan.AssembliesInBaseDirectory();
        //    scan.ForRegistries();
        //    scan.With<FirstInterfaceConvention>();
        //    scan.With<AddAllConvention>()
        //        .TypesImplementing<IHaveManyImplementations>();
        //    scan.With<SetAllPropertiesConvention>().OfType<ILogger>();
        //    scan.ExcludeType<FooService>();
        //});


            // TODO: Register your types here
            container.RegisterType<ISportUnitOfWork, SportUnitOfWork>();
            container.RegisterType<Sport.Common.Core.ILogger, Logger>(new InjectionConstructor("test", "pref"));
            //container.Configure()
            container.RegisterType(typeof(IRepository<>), typeof(BaseRepository<,>));

      //      container.RegisterTypes(
      //AllClasses.FromLoadedAssemblies(),
      ////AllClasses.FromAssemblies()
      ////.Where(
      ////  t => t.Namespace.Contains("Repository")),
      //WithMappings.FromAllInterfacesInSameAssembly,
      //WithName.Default,
      //WithLifetime.ContainerControlled);
        }
    }

    //public class SportRegistry : UnityRegistry
    //{
        
    //}
}
