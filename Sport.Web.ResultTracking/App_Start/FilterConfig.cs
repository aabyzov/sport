﻿using System.Web;
using System.Web.Mvc;

namespace Sport.Web.ResultTracking
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
