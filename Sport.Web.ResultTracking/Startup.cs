﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sport.Web.ResultTracking.Startup))]
namespace Sport.Web.ResultTracking
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
