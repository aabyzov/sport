﻿using System;
using System.Threading;
using System.Transactions;
using NCrunch.Framework;
using Sport.Domain.IoC;
using StructureMap;
using Xunit;

namespace Sport.Migration
{
    public class MigrationManagerRunner
    {
        // That means process all of them
        private const int FixtureCount = 0;
        private readonly MigrationManager _migrationManager;
        public MigrationManagerRunner()
        {
            CompositionRoot.Initialize();
            _migrationManager = ObjectFactory.GetInstance<MigrationManager>();
        }

        [Timeout(0)]
        [Fact]
        public void Will_migrate_data_with_committing_to_db()
        {
            _migrationManager.MigrateData();
        }

        [Timeout(0)]
        [Fact]
        public void Will_migrate_data_without_committing_to_db()
        {
            _migrationManager.MigrateData(false);
        }

        [Timeout(0)]
        [Fact]
        public void Will_migrate_out_of_fixture_players_without_committing_to_db()
        {
            // Tran should be used to rollback all changes (on Dispose) made by migration
            using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.Zero))
            {
                _migrationManager.AddOutOfFixturePlayersBasedOnMigratedData(FixtureCount);
            }
        }

        [Timeout(0)]
        [Fact]
        public void Will_migrate_out_of_fixture_players_with_committing_to_db()
        {
            // Tran should be used to commit (or rollback in case of error) all changes (on Dispose) made by migration
            using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.Zero))
            {
                _migrationManager.AddOutOfFixturePlayersBasedOnMigratedData();
                tran.Complete();
            }
        }

        [Timeout(0)]
        [Fact]
        public void Will_cleanup_suspension_tables()
        {
            // Tran should be used to commit (or rollback in case of error) all changes (on Dispose) made by migration
            using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.Zero))
            {
                _migrationManager.CleanSuspensionTables();
                tran.Complete();
            }
        }

        [Fact]
        public void Will_CleanUpMintalRedCard()
        {
            using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.Zero))
            {
                _migrationManager.CleanUpMintalRedCard();
                tran.Complete();
            }
        }

        //[Fact]
        //public void WillApplyMintalFix()
        //{
        //    // Tran should be used to commit (or rollback in case of error) all changes (on Dispose) made by migration
        //    using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.Zero))
        //    {
        //        _migrationManager.FixMintalInjuryMissedMatchCount();
        //        tran.Complete();
        //    }
        //}

        //[Timeout(0)]
        //[Fact(Skip = "Test")]
        //public void Will_migrate_out_of_fixture_players_with_committing_to_db()
        //{
        //    const int fixtureCount = 60;
        //    using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.Zero))
        //    {
        //        _migrationManager.AddOutOfFixturePlayersBasedOnMigratedData(fixtureCount);
        //        tran.Complete();
        //    }
        //    using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.Zero))
        //    {
        //        _migrationManager.AddOutOfFixturePlayersBasedOnMigratedData(fixtureCount);
        //        tran.Complete();
        //    }
        //    using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.Zero))
        //    {
        //        _migrationManager.AddOutOfFixturePlayersBasedOnMigratedData(fixtureCount);
        //        tran.Complete();
        //    }
        //    using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.Zero))
        //    {
        //        _migrationManager.AddOutOfFixturePlayersBasedOnMigratedData(fixtureCount);
        //        tran.Complete();
        //    }
        //}
    }
}