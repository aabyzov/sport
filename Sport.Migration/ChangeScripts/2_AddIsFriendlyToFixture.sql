﻿alter table SportDB_prod.dbo.Fixtures
add IsFriendly bit NULL

update SportDB_prod.dbo.Fixtures set IsFriendly = c.IsFriendly
  from [Champs].[dbo].[Fixtures] c, SportDB_prod.dbo.Fixtures s
  where s.MigrationId = c.Id 