﻿alter table SportDB_prod.dbo.Stages
add RoundCount int NOT NULL

update SportDB_prod.dbo.Stages set RoundCount = c.RoundCount
  from [Champs].[dbo].[Stages] c, SportDB_prod.dbo.Stages s
  where s.MigrationId = c.Id 