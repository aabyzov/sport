﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Threading;
using System.Transactions;
using NCrunch.Framework;
using Sport.Domain.IoC;
using StructureMap;
using Xunit;


namespace Sport.Migration
{
    public class Program
    {
        static void Main(string[] args)
        {
            CompositionRoot.Initialize();
            var migrationManager = ObjectFactory.GetInstance<MigrationManager>();
            Console.WriteLine("Migrating data from champsDb to '{0}' at {1}", ConfigurationManager.ConnectionStrings["SportContext"].ConnectionString, DateTime.Now);
            migrationManager.MigrateData();
            Console.WriteLine("Migration has finished successfully at {0}", DateTime.Now);
            //Console.ReadLine();
        }
    }
}