﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Champs.DomainLayer.Entities;
using Champs.DomainLayer.Entities.Enums;
using Champs.DomainLayer.EntityFramework;
using EntityFramework.BulkInsert.Extensions;
using Sport.Domain.Dictionaries;
using Sport.Domain.Models;
using Sport.Domain.Models.Enums;
using Sport.Domain.Services;
using Sport.Domain.UoW;
using Champ = Sport.Domain.Models.Champ;
using ChampType = Sport.Domain.Models.ChampType;
using Fixture = Sport.Domain.Models.Fixture;
using FixtureEvent = Sport.Domain.Models.FixtureEvent;
using FixtureParticipant = Sport.Domain.Models.FixtureParticipant;
using Group = Sport.Domain.Models.Group;
using GroupTeam = Sport.Domain.Models.GroupTeam;
using League = Sport.Domain.Models.League;
using Player = Sport.Domain.Models.Player;
using Stage = Sport.Domain.Models.Stage;
using Team = Sport.Domain.Models.Team;

namespace Sport.Migration
{
    public class MigrationManager
    {
        private readonly ISportUnitOfWork _sportUnitOfWork;
        private readonly IScheduleManager _scheduleManager;
        private readonly Dictionary<string, string> _teamZoneDictionary;
        private readonly SportContext _db;
        private readonly MissingMatchManager _missingMatchManager;

        public MigrationManager(ISportUnitOfWork sportUnitOfWork, IScheduleManager scheduleManager, SportContext sportContext, MissingMatchManager missingMatchManager)
        {
            _missingMatchManager = missingMatchManager;
            _db = sportContext;
            _scheduleManager = scheduleManager;
            _sportUnitOfWork = sportUnitOfWork;
            _teamZoneDictionary = TeamZoneDictionary.Get();
        }

        public void MigrateData(bool commit = true)
        {
            //_db.Database.CreateIfNotExists();
            //!!! Just first request to db to create a new db if not exists;
            var needItToMigrateToLatest = _db.Fixtures.Any();
            using (var tran = new TransactionScope(TransactionScopeOption.Required, TimeSpan.Zero))
            {
                MigrateDataToRealDb();
                if (commit)
                    tran.Complete();
            }
        }

        public void AdjustFixtureDates(Champ migratedChamp)
        {
            var schedule = _scheduleManager.GetScheduleTable(migratedChamp.Zone, migratedChamp.ChampSeason.ChampType);
            var scheduledStages = schedule.GetStages(migratedChamp.ChampSeason.End.Year);
            var champ = _sportUnitOfWork.Champs.WithFixturesAndTeams().First(x => x.Id == migratedChamp.Id);
            var champStages = champ.StagesOrdered;
            for (var i = 0; i < champStages.Count(); i++)
            {
                var champStage = champStages[i];
                var scheduledStage = scheduledStages[i];
                foreach (var fixture in champStage.Fixtures)
                {
                    var matchDay = fixture.MatchDay;
                    if (matchDay == 0)
                        matchDay = 1;
                    fixture.Date = scheduledStage.MatchDays[matchDay - 1];
                }
            }
            _sportUnitOfWork.Save();
        }

        public void CleanSuspensionTables()
        {
            _sportUnitOfWork.SuspendedFixtures.DeleteAll();
            _sportUnitOfWork.Suspensions.DeleteAll();
            _sportUnitOfWork.OutOfFixturePlayers.DeleteAll();
            //reset fixtures HasOutOfFixtureProcessed flag
            var fixtures = _sportUnitOfWork.Fixtures.FindAllBy();
            fixtures.ForEach(x => x.HasOutOfFixtureProcessed = false);
            _sportUnitOfWork.Save();
        }

        public void AddOutOfFixturePlayersBasedOnMigratedData(int fixtureCount = 0)
        {
            // Clean up all data we have currently (in case we remove some events like yellow card) for the sake of data integrity
            CleanSuspensionTables();

            var champs = _sportUnitOfWork.Champs.FindAllOrderedByStartDate();
            var fixtures = new List<Fixture>();

            foreach (var champ in champs)
            {
                fixtures.AddRange(_sportUnitOfWork.Fixtures.FindAllByChampId(champ.Id).Where(x => !x.HasOutOfFixtureProcessed).ToList());
            }

            if (fixtureCount == 0)
                fixtureCount = fixtures.Count;
            var partOfFixtures = fixtures.Take(fixtureCount).ToList();

            foreach (var fixture in partOfFixtures)
            {
                _missingMatchManager.BeforeMatch(fixture);
                _missingMatchManager.AfterMatch(fixture);
            }
        }

        private void MigrateDataToRealDb()
        {
            var champsUnitOfWork = new ChampsUnitOfWork();
            _db.Configuration.AutoDetectChangesEnabled = false;
            _db.Configuration.ValidateOnSaveEnabled = false;
            _db.UseAudit = false;

            if (_db.Fixtures.Any()) return;

            var sportKind = new SportKind() { Name = "Football" };
            _db.SportKinds.Add(sportKind);
            _db.SaveChanges();

            MigrateLeagues(champsUnitOfWork, sportKind);

            MigrateZones();

            MigrateTeams(champsUnitOfWork);

            Console.WriteLine("Migrating players, teams at {0}", DateTime.Now);

            MigratePlayers(champsUnitOfWork);

            MigrateTeamPlayerLinks(champsUnitOfWork);

            MigrateChampTypes(champsUnitOfWork, sportKind);

            MigrateChampParents(champsUnitOfWork);

            MigrateChamps(champsUnitOfWork);

            MigrateStages(champsUnitOfWork);

            MigrateGroups(champsUnitOfWork);

            MigrateGroupTeams(champsUnitOfWork);

            Console.WriteLine("Migrating players, fixtures at {0}", DateTime.Now);

            MigrateFixtures(champsUnitOfWork);

            MigrateFixtureParticipants(champsUnitOfWork);

            Console.WriteLine("Migrating fixture events at {0}", DateTime.Now);

            MigrateFixtureEvents(champsUnitOfWork);

            AfterMigrationFixes();
        }

        private void AfterMigrationFixes()
        {
            Console.WriteLine("Applying Adjust_Event_order_for_yellow_red_on_same_minute at {0}", DateTime.Now);
            AdjustEventOrderForYellowRedCardsOnSameMinute();
            Console.WriteLine("Applying AdjustFixtureMatchDays at {0}", DateTime.Now);
            AdjustFixtureMatchDays();
            Console.WriteLine("Applying Each_team_should_not_play_several_times_per_one_day at {0}", DateTime.Now);
            AdjustFixtureDatesForAllChamps();
            Console.WriteLine("Applying CleanUpMintalRedCard at {0}", DateTime.Now);
            CleanUpMintalRedCard();
            Console.WriteLine("Applying Fix_Mintal_injury_missed_match_count at {0}", DateTime.Now);
            FixMintalInjuryMissedMatchCount();
            Console.WriteLine("Applying Fix_Salas_injury_missed_match_count at {0}", DateTime.Now);
            FixSalasInjuryMissedMatchCount();
            Console.WriteLine("Applying Clear_Hutchinson_assist_against_France at {0}", DateTime.Now);
            ClearHutchinsonAssistAgainstFrance();
        }

        public void CleanUpMintalRedCard()
        {
            var player = _sportUnitOfWork.Players.FindOneBy(x => x.LastName == "Mintal");
            var fixtureEvent =
                _sportUnitOfWork.FixtureEvents.FindOneBy(x => x.PlayerId == player.Id && x is FootballYellowCard && ((x.EventTime as FootballEventTime).Minute == 69));
            //Assert.NotNull(fixtureEvent);
            var secondYellow = (fixtureEvent as FootballYellowCard);
            _sportUnitOfWork.FixtureEvents.Delete(secondYellow);
            _sportUnitOfWork.Save();
        }

        private void FixSalasInjuryMissedMatchCount()
        {
            var player = _sportUnitOfWork.Players.FindOneBy(x => x.LastName == "Salas");
            var fixtureEvent =
                _sportUnitOfWork.FixtureEvents.FindOneBy(x => x.PlayerId == player.Id && x is FootballInjury && x.FixtureParticipant.Fixture.FixtureParticipants.FirstOrDefault(
                        fp => (fp.IsHome && (fp.GroupTeam.Team.Name == "Colombia"))) != null);
            //            Assert.NotNull(fixtureEvent);
            var footballInjury = (fixtureEvent as FootballInjury);
            footballInjury.MissedMatchCountInjury--;
            _sportUnitOfWork.Save();
        }

        private void ClearHutchinsonAssistAgainstFrance()
        {
            var fixtureEvent =
                _sportUnitOfWork.FixtureEvents.FootballGoals().FirstOrDefault(
                    x =>
                    x.Player.LastName == "De Rosario" && (x is FootballGoal) &&
                    (x as FootballGoal).Assistant.LastName == "Hutchinson" && x.FixtureParticipant.Fixture.FixtureParticipants.FirstOrDefault(fp => !fp.IsHome).GroupTeam.Team.Name == "France");
            if (fixtureEvent == null) return;
            //Assert.NotNull(fixtureEvent);
            (fixtureEvent as FootballGoal).Assistant = null;
            _sportUnitOfWork.Save();
        }

        public void FixMintalInjuryMissedMatchCount()
        {
            var player = _sportUnitOfWork.Players.FindOneBy(x => x.LastName == "Mintal");
            var fixtureEvent =
                _sportUnitOfWork.FixtureEvents.FindOneBy(x => x.PlayerId == player.Id && x is FootballInjury);
            //Assert.NotNull(fixtureEvent);
            var footballInjury = (fixtureEvent as FootballInjury);
            footballInjury.MissedMatchCountInjury--;

            var playerOutOfFixture =
                _sportUnitOfWork.OutOfFixturePlayers.FindOneBy(
                    x =>
                    x.PlayerId == player.Id &&
                    x.FixtureParticipant.Fixture.FixtureParticipants.FirstOrDefault(
                        fp => (!fp.IsHome && (fp.GroupTeam.Team.Name == "Russia"))) != null);
            if (playerOutOfFixture != null)
            {
                playerOutOfFixture.MissedMatchCount--;
                _sportUnitOfWork.Save();
            }
        }

        private void AdjustEventOrderForYellowRedCardsOnSameMinute()
        {
            var fixtureEvents = _sportUnitOfWork.FixtureEvents.FootballEvents();
            var query = from ev in fixtureEvents
                        group ev by new { FixtureParticipantFixtureId = ev.FixtureParticipantId, Minute = ev.EventTime is FootballEventTime ? (ev.EventTime as FootballEventTime).Minute : 0 }
                            into grouped
                            where grouped.Count() > 1
                            select new
                            {
                                FixtureParticipantFixtureId = grouped.FirstOrDefault().FixtureParticipantId,
                                grouped.FirstOrDefault().EventOrder,
                                Events = grouped.Select(x => x)
                            };

            //Act && assert
            foreach (var item in query)
            {
                var redCardEvent = item.Events.FirstOrDefault(x => x is FootballRedCard);
                var yellowCardEvent = item.Events.FirstOrDefault(x => x is FootballYellowCard);
                if (redCardEvent != null && yellowCardEvent != null)
                {
                    redCardEvent.EventOrder++;
                    yellowCardEvent.EventOrder--;
                    _sportUnitOfWork.Save();
                }
            }
        }

        private void MigrateFixtureEvents(ChampsUnitOfWork champsUnitOfWork)
        {
            var fixtureEvents = new List<FixtureEvent>();
            foreach (var fixtureEvent in champsUnitOfWork.FixtureEvents)
            {
                var fixture = _sportUnitOfWork.Fixtures.WithEventsAndTeams().FirstOrDefault(x => x.MigrationId == fixtureEvent.FixtureParticipant.FixtureId);
                var player = _db.Players.FirstOrDefault(x => x.MigrationId == fixtureEvent.PlayerId);
                FixtureEvent @event = null;
                var fixtureEvent1 = fixtureEvent;
                var participant =
                    _db.FixtureParticipants.FirstOrDefault(x => x.IsHome == (fixtureEvent1.FixtureParticipantType == 1) &&
                                                                x.GroupTeam.MigrationId ==
                                                                fixtureEvent1.FixtureParticipantGroupTeamId &&
                                                                x.Fixture.MigrationId ==
                                                                fixtureEvent1.FixtureParticipantFixtureId);
                if (fixtureEvent.Type == null || fixtureEvent.Type == 0)
                {
                    var goalEvent = (fixtureEvent as GoalEvent);
                    if (goalEvent == null) throw new Exception("Event should be goal!");
                    var assistant = goalEvent.PlayerAssistantId != null
                        ? _db.Players.FirstOrDefault(x => x.MigrationId == (Guid)goalEvent.PlayerAssistantId)
                        : null;
                    @event = new FootballGoal()
                    {
                        Player = player,
                        Assistant = assistant,
                        PlayerId = player.Id,
                        AssistantId = assistant != null ? assistant.Id : (Guid?)null,
                        //AssistantDetails = goalEvent.AssistantDetails,
                        BodyPart = (BodyPart)(goalEvent.BodyPart.HasValue ? goalEvent.BodyPart.Value : 0),
                        EventOrder = goalEvent.EventOrder,
                        EventTime = new FootballEventTime(goalEvent.Minute),
                        GoalDistance = goalEvent.GoalDistance,
                        HowScored =
                            (Domain.Models.Enums.HowScored)(goalEvent.HowScored.HasValue ? goalEvent.HowScored.Value + 1 : 0),
                        FixtureParticipant = participant,
                        FixtureParticipantId = participant.Id
                    };
                }
                if (fixtureEvent.Type == 1)
                {
                    var ownEvent = (fixtureEvent as OwnGoalEvent);
                    if (ownEvent == null) throw new Exception("Event should be own goal!");
                    var assistant = ownEvent.PlayerAssistantId != null
                        ? _db.Players.FirstOrDefault(x => x.MigrationId == (Guid)ownEvent.PlayerAssistantId)
                        : null;
                    @event = new FootballOwnGoal()
                    {
                        Player = player,
                        PlayerId = player.Id,
                        AssistantId = assistant != null ? assistant.Id : (Guid?)null,
                        Assistant = assistant,
                        //AssistantDetails = ownEvent.AssistantDetails,
                        EventOrder = ownEvent.EventOrder,
                        EventTime = new FootballEventTime(ownEvent.Minute),
                        FixtureParticipant = participant.Opponent,
                        FixtureParticipantId = participant.Opponent.Id
                    };
                }
                if (fixtureEvent.Type == 2)
                {
                    var missed = (fixtureEvent as MissedPenaltyEvent);
                    if (missed == null) throw new Exception("Event should be missed penalty!");
                    @event = new FootballMissedPenalty()
                    {
                        Player = player,
                        PlayerId = player.Id,
                        EventOrder = missed.EventOrder,
                        Details = (MissedPenaltySituations)missed.Details,
                        EventTime = new FootballEventTime(missed.Minute),
                        FixtureParticipant = participant,
                        FixtureParticipantId = participant.Id
                    };
                }
                if (fixtureEvent.Type == 3)
                {
                    var redCard = (fixtureEvent as RedCardEvent);
                    if (redCard == null) throw new Exception("Event should be red card!");
                    @event = new FootballRedCard()
                    {
                        Player = player,
                        PlayerId = player.Id,
                        EventOrder = redCard.EventOrder,
                        MissedMatchCountRedCard = redCard.MissedMatchCount,
                        EventTime = new FootballEventTime(redCard.Minute),
                        FixtureParticipant = participant,
                        FixtureParticipantId = participant.Id
                    };
                }
                if (fixtureEvent.Type == 4)
                {
                    var yellowCard = (fixtureEvent as YellowCardEvent);
                    if (yellowCard == null) throw new Exception("Event should be yellow card!");
                    @event = new FootballYellowCard()
                    {
                        Player = player,
                        PlayerId = player.Id,
                        EventOrder = yellowCard.EventOrder,
                        EventTime = new FootballEventTime(yellowCard.Minute),
                        FixtureParticipant = participant,
                        FixtureParticipantId = participant.Id
                    };
                }
                if (fixtureEvent.Type == 5)
                {
                    var injury = (fixtureEvent as InjuryEvent);
                    if (injury == null) throw new Exception("Event should be injury!");
                    @event = new FootballInjury()
                    {
                        Player = player,
                        PlayerId = player.Id,
                        EventOrder = injury.EventOrder,
                        MissedMatchCountInjury = injury.MissedMatchCount,
                        RecoveryDate = injury.RecoveryDate,
                        EventTime = new FootballEventTime(injury.Minute),
                        FixtureParticipant = participant,
                        FixtureParticipantId = participant.Id
                    };
                }

                fixtureEvents.Add(@event);
            }
            // We can't use bulk straightforward here, coz we have to insert event time first
            _db.FixtureEvents.AddRange(fixtureEvents);
            _db.SaveChanges();
        }

        private void MigrateFixtureParticipants(ChampsUnitOfWork champsUnitOfWork)
        {
            var fixtureParticipants = new List<FixtureParticipant>();
            foreach (var fixtureParticipant in champsUnitOfWork.FixtureParticipants)
            {
                var fixture = fixtureParticipant.Fixture;
                var fixtureItem = _db.Fixtures.FirstOrDefault(x => x.MigrationId == fixture.Id);
                //var home = fixture.Home;
                //var away = fixture.Away;
                //var groupTeam = _db.GroupTeams.FirstOrDefault(x => x.MigrationId == (fixtureParticipant.IsHome ? home.GroupTeamId : away.GroupTeamId));
                var groupTeam = _db.GroupTeams.FirstOrDefault(x => x.MigrationId == fixtureParticipant.GroupTeamId);

                //var homeFixtureParticipant = new FootballFixtureParticipant()
                //{
                //    GroupTeam = homeGroupTeam,
                //    GroupTeamId = homeGroupTeam.Id,
                //    Score = home.Score,
                //    IsHome = true,
                //    Fixture = fixtureItem,
                //    FixtureId = fixtureItem.Id
                //};
                var footballFixtureParticipant = new FootballFixtureParticipant()
                {
                    GroupTeam = groupTeam,
                    GroupTeamId = groupTeam.Id,
                    Score = fixtureParticipant.Score,
                    IsHome = fixtureParticipant.IsHome,
                    Fixture = fixtureItem,
                    FixtureId = fixtureItem.Id
                };
                //fixtureParticipants.Add(homeFixtureParticipant);
                fixtureParticipants.Add(footballFixtureParticipant);
            }
            _db.BulkInsert(fixtureParticipants);
        }

        private void MigrateFixtures(ChampsUnitOfWork champsUnitOfWork)
        {
            var fixtures = new List<Fixture>();
            foreach (var fixture in champsUnitOfWork.Fixtures)
            {
                var home = fixture.Home;
                var away = fixture.Away;
                var item = new Fixture()
                {
                    IsPlayed = home.Score != null && away.Score != null,
                    Date =
                        fixture.HomeParticipant().GroupTeam.Group.Stage.Champ.Name == "Friendlies Host" && fixture.Date.HasValue
                            ? new DateTime(fixture.Date.Value.Year, fixture.Date.Value.Month + 1, fixture.Date.Value.Day)
                            : fixture.Date,
                    IsFriendly = fixture.IsFriendly,
                    MigrationId = fixture.Id,
                    MatchDay = fixture.Tour
                };
                fixtures.Add(item);
            }
            _db.BulkInsert(fixtures);
        }

        private void MigrateGroupTeams(ChampsUnitOfWork champsUnitOfWork)
        {
            foreach (var groupTeam in champsUnitOfWork.GroupTeams)
            {
                var team = _db.Teams.FirstOrDefault(x => x.MigrationId == groupTeam.TeamId);
                var group = _db.Groups.FirstOrDefault(x => x.MigrationId == groupTeam.GroupId);
                _db.GroupTeams.Add(new GroupTeam()
                {
                    Team = team,
                    Group = @group,
                    MigrationId = groupTeam.Id
                });
            }
            _db.SaveChanges();
        }

        private void MigrateGroups(ChampsUnitOfWork champsUnitOfWork)
        {
            foreach (var @group in champsUnitOfWork.Groups)
            {
                var stage = _db.Stages.FirstOrDefault(x => x.MigrationId == @group.StageId);
                _db.Groups.Add(new Group()
                {
                    Name = @group.Name,
                    Stage = stage,
                    MigrationId = @group.Id
                });
            }
            _db.SaveChanges();
        }

        private void MigrateStages(ChampsUnitOfWork champsUnitOfWork)
        {
            foreach (var stage in champsUnitOfWork.Stages)
            {
                var champ = _db.Champs.FirstOrDefault(x => x.MigrationId == stage.ChampId);
                _db.Stages.Add(new Stage()
                {
                    Name = stage.Name,
                    Champ = champ,
                    RoundCount = stage.RoundCount,
                    MigrationId = stage.Id
                });
            }
            _db.SaveChanges();
        }

        private void MigrateChamps(ChampsUnitOfWork champsUnitOfWork)
        {
            foreach (var champ in champsUnitOfWork.Champs)
            {
                var champSeason = _db.ChampSeasons.FirstOrDefault(x => x.MigrationId == champ.ChampParentId);
                var zone = _db.Zones.FirstOrDefault(x => x.MigrationEnumId == champ.ZoneId);
                _db.Champs.Add(new Champ()
                {
                    Name = champ.Name,
                    ChampSeason = champSeason,
                    Zone = zone,
                    MigrationId = champ.Id,
                    YellowsToMissNextMatch = 2
                });
            }
            _db.SaveChanges();
        }

        private void MigrateChampParents(ChampsUnitOfWork champsUnitOfWork)
        {
            foreach (var champParent in champsUnitOfWork.ChampParents)
            {
                var champType = _db.ChampTypes.FirstOrDefault(x => x.MigrationId == champParent.ChampType.Id);
                _db.ChampSeasons.Add(new ChampSeason()
                {
                    Name = champParent.ChampType.Name + GetChampYear(champParent),
                    End = new DateTime(champParent.EndYear, 1, 1),
                    Start = new DateTime(champParent.StartYear, 1, 1),
                    ChampType = champType,
                    MigrationId = champParent.Id,
                });
            }
            _db.SaveChanges();
        }

        private void MigrateChampTypes(ChampsUnitOfWork champsUnitOfWork, SportKind sportKind)
        {
            foreach (var champType in champsUnitOfWork.ChampTypes)
            {
                _db.ChampTypes.Add(new ChampType()
                {
                    Name = champType.Name,
                    SportKind = sportKind,
                    MigrationId = champType.Id
                });
            }
            _db.SaveChanges();
        }

        private void MigrateTeamPlayerLinks(ChampsUnitOfWork champsUnitOfWork)
        {
            var teamPlayerLinks = new List<TeamMember>();
            foreach (var teamPlayerLink in champsUnitOfWork.TeamPlayerLinks)
            {
                var player = _db.Players.FirstOrDefault(x => x.MigrationId == teamPlayerLink.PlayerId);
                var team = _db.Teams.FirstOrDefault(x => x.MigrationId == teamPlayerLink.TeamId);
                teamPlayerLinks.Add(new TeamMember()
                {
                    Player = player,
                    PlayerId = player.Id,
                    TeamId = team.Id,
                    Team = team,
                    JerseyNumber = teamPlayerLink.JerseyNumber,
                    PositionNumber = teamPlayerLink.Position,
                    YearContractEnds = teamPlayerLink.YearContractValidTill
                });
            }

            _db.BulkInsert(teamPlayerLinks);
        }

        private void MigratePlayers(ChampsUnitOfWork champsUnitOfWork)
        {
            var players = new List<Player>();
            foreach (var player in champsUnitOfWork.Players)
            {
                players.Add(new Player()
                {
                    FirstName = player.FirstName,
                    LastName = player.LastName,
                    MigrationId = player.Id
                });
            }
            _db.BulkInsert(players);
        }

        private void MigrateTeams(ChampsUnitOfWork champsUnitOfWork)
        {
            var teams = new List<Team>();
            foreach (var team in champsUnitOfWork.Teams)
            {
                var league = _db.Leagues.FirstOrDefault(x => x.MigrationId == team.LeagueId);
                var item = new Team()
                {
                    Name = team.Name,
                    IsActive = true,
                    Chemistry = team.Chemistry,
                    OverallRating = team.OverallRating,
                    League = league,
                    LeagueId = league.Id,
                    MigrationId = team.Id
                };
                AssignZoneIdToTeam(item);
                if (_db.Teams.FirstOrDefault(x => x.MigrationId == team.Id) == null)
                    teams.Add(item);
            }
            _db.BulkInsert(teams);
        }

        private void MigrateZones()
        {
            var zoneId = 0;
            foreach (var zone in Enum.GetNames(typeof(ZonesEnum)))
            {
                var item = new Zone()
                {
                    Name = zone,
                    MigrationEnumId = zoneId
                };
                int id = zoneId;
                if (_db.Zones.FirstOrDefault(x => x.MigrationEnumId == id) == null)
                    _db.Zones.Add(item);
                zoneId++;
            }
            _db.SaveChanges();
        }

        private void MigrateLeagues(ChampsUnitOfWork champsUnitOfWork, SportKind sportKind)
        {
            foreach (var league in champsUnitOfWork.Leagues)
            {
                var item = new League()
                {
                    Name = league.Name,
                    SportKind = sportKind,
                    MigrationId = league.Id
                };
                if (_db.Leagues.FirstOrDefault(x => x.MigrationId == league.Id) == null)
                    _db.Leagues.Add(item);
                _db.SaveChanges();
            }
        }

        private void AssignZoneIdToTeam(Team team)
        {
            var zoneName = GetZoneName(team.Name);
            var zone = _db.Zones.FirstOrDefault(x => x.Name == zoneName);
            if (zone == null) throw new Exception("There should be saved zone already");
            team.ZoneId = zone.Id;
        }

        private string GetZoneName(string teamName)
        {
            return _teamZoneDictionary[teamName];
        }

        protected static string GetChampYear(ChampParent champParent)
        {
            if (champParent.StartYear == champParent.EndYear)
            {
                return " " + champParent.EndYear;
            }
            return " " + champParent.StartYear + "-" + champParent.EndYear;
        }

        private void AdjustFixtureDatesForAllChamps()
        {
            var champs = _sportUnitOfWork.Champs.FindAll();
            Array.ForEach(champs.ToArray(), AdjustFixtureDates);
        }

        private void AdjustFixtureMatchDays()
        {
            var champs = _sportUnitOfWork.Champs.FindAll();
            Array.ForEach(champs.ToArray(), AdjustMatchDays);
        }

        public void AdjustMatchDays(Champ champ)
        {
            foreach (Group @group in champ.Groups)
            {
                AdjustMatchDaysInGroup(@group);
            }
        }

        private void AdjustMatchDaysInGroup(Group @group)
        {
            if (!@group.HasFixtures) return;
            var fixtures = _sportUnitOfWork.Fixtures.FindAllByGroupId(@group.Id).OrderBy(x => x.Date).ToList();
            var groupTeams = @group.GroupTeams;
            var fixtureCountPerRound = @group.FixtureCountPerRound;
            if (fixtures.Count() < fixtureCountPerRound) throw new InvalidOperationException("There should be more fixtures for a group");
            var teams = groupTeams.Select(x => x.Team).ToList();
            var table = _scheduleManager.GenerateTable(groupTeams);
            var tourCountPerRound = teams.Count % 2 == 0 ? teams.Count - 1 : teams.Count;

            // Group by fixture date

            for (var i = 0; i < fixtureCountPerRound; i++)
            {
                var fixture = fixtures[i];
                fixture.MatchDay = table[teams.IndexOf(teams.First(x => x.Name == fixture.HomeTeamName)), teams.IndexOf(teams.First(x => x.Name == fixture.AwayTeamName))];
            } 

            if (fixtures.Count() > fixtureCountPerRound)
            {
                for (var i = fixtureCountPerRound; i < fixtures.Count(); i++)
                {
                    var currentRound = fixtures.Count() / fixtureCountPerRound;
                    var theFixture = fixtures[i];
                    theFixture.MatchDay = table[teams.IndexOf(teams.First(x => x.Name == theFixture.HomeTeamName)), teams.IndexOf(teams.First(x => x.Name == theFixture.AwayTeamName))] + (currentRound - 1) * tourCountPerRound;
                }
            }

            _sportUnitOfWork.Save();
        }
    }
}