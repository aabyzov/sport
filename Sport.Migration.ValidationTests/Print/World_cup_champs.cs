﻿using System;
using System.Linq;
using Sport.Common.Core;
using Sport.Domain.Services.Templates;
using Sport.Tests.Common;
using Xunit;

namespace Sport.Migration.ValidationTests.Print
{
    public class World_cup_champs : IntegrationBase
    {
        [Fact]
        public void Will_print_african_qualif_champ()
        {
            //Arrange
            var fixtures = _sportUnitOfWork.Fixtures.WithEventsAndTeams().Where(x => x.FixtureParticipants.FirstOrDefault().GroupTeam.Group.Stage.Champ.Zone.Name == Constants.Zones.Africa).OrderBy(x => x.Date).ToList();
            var standingsManager = CreateStandingsManager();

            var groups = fixtures.Select(x => x.Group).Distinct();
            var stages = fixtures.Select(x => x.Group.Stage).Distinct();
            Array.ForEach(groups.ToArray(), x => x.Standings = standingsManager.BuildStandings(x, new WorldCupTemplate()));

            //Act
            foreach (var stage in stages)
            {
                Write(stage);
            }

        }

        [Fact]
        public void Will_print_north_American_qualif_champ()
        {
            //Arrange
            var fixtures = _sportUnitOfWork.Fixtures.WithEventsAndTeams().Where(x => x.FixtureParticipants.FirstOrDefault().GroupTeam.Group.Stage.Champ.Zone.Name == Constants.Zones.NorthAmerica).OrderBy(x => x.Date).ToList();
            var standingsManager = CreateStandingsManager();

            var groups = fixtures.Select(x => x.Group).Distinct();
            var stages = fixtures.Select(x => x.Group.Stage).Distinct();
            Array.ForEach(groups.ToArray(), x => x.Standings = standingsManager.BuildStandings(x, new WorldCupTemplate()));

            //Act
            foreach (var stage in stages)
            {
                Write(stage);
            }

        }
 
    }
}