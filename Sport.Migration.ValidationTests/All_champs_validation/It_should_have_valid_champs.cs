﻿//using Sport.IntegrationTests;

using Sport.Tests.Common;
using Xunit;

namespace Sport.Migration.ValidationTests.All_champs_validation
{
    public class It_should_have_valid_champs : IntegrationBase
    {
        [Fact]
        public void Each_friendly_champ_should_have_only_friendly_matches()
        {
            var fixtures = _sportUnitOfWork.Fixtures.FindAllWhereChampNameStartWith("Friendl");
            foreach (var fixture in fixtures)
            {
                Assert.True(fixture.IsFriendly);
            }
        }
    }
}