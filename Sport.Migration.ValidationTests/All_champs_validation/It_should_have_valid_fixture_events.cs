﻿//using System.Data.Entity;

//using Sport.IntegrationTests;
using System.Linq;
using Should;
using Sport.Domain.Models;
using Sport.Tests.Common;
using Xunit;

namespace Sport.Migration.ValidationTests.All_champs_validation
{
    public class It_should_have_valid_fixture_events_for_all_champs : IntegrationBase
    {
        [Fact]
        public void Each_football_event_should_have_valid_event_order()
        {
            //Arrange
            var fixtureEvents = _sportUnitOfWork.FixtureEvents.FootballEvents();
            var query = from ev in fixtureEvents
                        group ev by new { FixtureParticipantFixtureId = ev.FixtureParticipantId, EventOrder = ev.EventOrder }
                            into grouped
                            where grouped.Count() > 1
                            select new { FixtureParticipantFixtureId = grouped.FirstOrDefault().FixtureParticipantId, grouped.FirstOrDefault().EventOrder, 
                                EventTimes = grouped.Select(x => x.EventTime as FootballEventTime)};

            //Act && assert
            foreach (var item in query)
            {
                var @event = item;
                var distinctEventTimesCount = @event.EventTimes.Distinct(new EventTimeComparer()).ToList();
                var eventTimesCount = @event.EventTimes.Count();
                Assert.True(distinctEventTimesCount.Count() == eventTimesCount);
            }
        }

        [Fact]
        public void Each_football_event_should_have_valid_event_order_for_cards()
        {
            //Arrange
            var fixtureEvents = _sportUnitOfWork.FixtureEvents.FootballEvents();
            var query = from ev in fixtureEvents
                        group ev by new { FixtureParticipantFixtureId = ev.FixtureParticipantId, Minute = ev.EventTime is FootballEventTime ? (ev.EventTime as FootballEventTime).Minute : 0 }
                            into grouped
                            where grouped.Count() > 1
                            select new
                            {
                                FixtureParticipantFixtureId = grouped.FirstOrDefault().FixtureParticipantId,
                                grouped.FirstOrDefault().EventOrder,
                                Events = grouped.Select(x => x)
                            };

            //Act && assert
            foreach (var item in query)
            {
                var redCardEvent = item.Events.FirstOrDefault(x => x is FootballRedCard);
                var yellowCardEvent = item.Events.FirstOrDefault(x => x is FootballYellowCard);
                if (redCardEvent != null && yellowCardEvent != null)
                {
                    Assert.True(redCardEvent.EventOrder > yellowCardEvent.EventOrder);
                }
            }
        }

        [Fact]
        public void Each_football_event_should_have_not_null_minute()
        {
            //Arrange
            var fixtureEvents = _sportUnitOfWork.FixtureEvents.FootballEvents();

            //Act && assert
            foreach (var fixtureEvent in fixtureEvents)
            {
                var time = fixtureEvent.EventTime as FootballEventTime;
                Assert.NotNull(time);
                Assert.True(time.Minute > 0);
            }
        }

        [Fact]
        public void Each_football_event_player_should_belong_to_team()
        {
            //Arrange
            var fixtureEvents = _sportUnitOfWork.FixtureEvents.FootballEvents();

            //Act & assert
            foreach (var fixtureEvent in fixtureEvents)
            {
                // For own goal we still specify player and team that produced own goal, so that's right
                var fixtureParticipant = fixtureEvent.FixtureParticipant;
                var eventTeam = fixtureParticipant.GroupTeam.Team;
                TeamMember teamMember;
                if (fixtureEvent is FootballOwnGoal)
                {
                    teamMember = fixtureEvent.Player.TeamMembers.FirstOrDefault(x => x.TeamId == fixtureParticipant.Opponent.GroupTeam.TeamId);
                }
                else
                {
                    teamMember = fixtureEvent.Player.TeamMembers.FirstOrDefault(x => x.TeamId == eventTeam.Id);
                }
                
                Assert.NotNull(teamMember);
            }

            foreach (var goal in _sportUnitOfWork.FixtureEvents.FootballGoals())
            {
                var eventTeam = goal.FixtureParticipant.GroupTeam.Team;
                if (goal.Assistant == null) continue;
                var teamMember =  goal.Assistant.TeamMembers.FirstOrDefault(x => x.TeamId == eventTeam.Id);
                Assert.NotNull(teamMember);
            }

            foreach (var ownGoal in _sportUnitOfWork.FixtureEvents.FootballOwnGoals())
            {
                var eventTeam = ownGoal.FixtureParticipant.GroupTeam.Team;
                if (ownGoal.Assistant == null) continue;
                var teamMember = ownGoal.Assistant.TeamMembers.FirstOrDefault(x => x.TeamId == eventTeam.Id);
                Assert.NotNull(teamMember);
            }
        }

        [Fact]
        public void Each_player_should_get_red_when_2_yellow_cards_received_during_a_fixture()
        {
            //Arrange
            var groupedFixtureEvents = _sportUnitOfWork.FixtureEvents.FootballEvents().GroupBy(x => new
                {
                    FixtureId = x.FixtureParticipant.Fixture.Id,
                    PlayerId = x.PlayerId,
                    //Events = x
                }).Select(x => new
                    {
                        Fixture = x.FirstOrDefault().FixtureParticipant.Fixture,
                        Player = x.FirstOrDefault().Player,
                        Events = x
                    });

            //Act && assert
            foreach (var groupedFixtureEvent in groupedFixtureEvents.ToList())
            {
                var cardEvents = groupedFixtureEvent.Events.Where(x => x.IsYellowCard || x.IsRedCard).ToArray();
                // that's for easy debug
                var fixture = _sportUnitOfWork.Fixtures.AllIncluding(x => x.FixtureParticipants.Select(fp => fp.GroupTeam.Team)).FirstOrDefault(x => x.Id == groupedFixtureEvent.Fixture.Id);
                var player = groupedFixtureEvent.Player;
                var yellowsCount = cardEvents.Count(x => x.IsYellowCard);
                if (yellowsCount == 2)
                    Assert.Equal(1, cardEvents.Count(x => x.IsRedCard));
            }
        }

        [Fact]
        public void Each_red_card_fixture_event_should_have_missed_match_count_greater_than_nil()
        {
            //Arrange
            var fixtureEvents = _sportUnitOfWork.FixtureEvents.FootballEvents().OfType<FootballRedCard>();

            //Act && assert
            foreach (var @event in fixtureEvents)
            {
                @event.MissedMatchCountRedCard.ShouldBeGreaterThan(0);
            }
        }
       
    }
}