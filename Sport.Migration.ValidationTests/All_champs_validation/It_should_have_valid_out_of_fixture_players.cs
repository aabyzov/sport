﻿using System.Linq;
using Sport.Tests.Common;
using Xunit;

namespace Sport.Migration.ValidationTests.All_champs_validation
{
    public class It_should_have_valid_out_of_fixture_players : IntegrationBase
    {
        [Fact]
        public void Each_fixture_event_should_not_have_referenced_player_from_out_of_fixture()
        {
            //Arrange
            var fixtureEvents = _sportUnitOfWork.FixtureEvents.FootballEventsWithOutOfFixture();

            //Act & assert
            foreach (var fixtureEvent in fixtureEvents) 
            {
                var player = fixtureEvent.Player;
                //var previousMatches = fixtureEvent.FixtureParticipant.PreviousFixtures;
                Assert.True(
                    fixtureEvent.FixtureParticipant.OutOfFixturePlayers.FirstOrDefault(x => x.PlayerId == player.Id) ==
                    null);
            }

            foreach (var goal in _sportUnitOfWork.FixtureEvents.FootballGoals())
            {
                var goalAssistant = goal.Assistant;
                if (goalAssistant == null) continue;
                Assert.True(
                    goal.FixtureParticipant.OutOfFixturePlayers.FirstOrDefault(x => x.PlayerId == goalAssistant.Id) ==
                    null);
            }

            foreach (var ownGoal in _sportUnitOfWork.FixtureEvents.FootballOwnGoals())
            {
                var player = ownGoal.Assistant;
                if (player == null) continue;
                Assert.True(
                    ownGoal.FixtureParticipant.OutOfFixturePlayers.FirstOrDefault(x => x.PlayerId == player.Id) ==
                    null);
            }
        }

        [Fact]
        public void Each_football_event_out_of_fixture_player_should_belong_to_team()
        {
            //Arrange
            var outOfFixturePlayers = _sportUnitOfWork.OutOfFixturePlayers.AllIncluding(x => x.FixtureParticipant.GroupTeam.Team, x => x.Player.TeamMembers);

            //Act & assert
            foreach (var outOfFixturePlayer in outOfFixturePlayers)
            {
                var eventTeam = outOfFixturePlayer.FixtureParticipant.GroupTeam.Team;
                var teamMember = outOfFixturePlayer.Player.TeamMembers.FirstOrDefault(x => x.TeamId == eventTeam.Id);
                Assert.NotNull(teamMember);
            }
        }
    }
}