﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Sport.Domain.Models;
using Sport.Tests.Common;
using Xunit;

namespace Sport.Migration.ValidationTests.All_champs_validation
{
    public class It_should_have_valid_teams : IntegrationBase
    {
        [Fact]
        public void Each_team_should_have_unique_name()
        {
            //Arrange
            var teams = _sportUnitOfWork.Teams.FindAll().GroupBy(x => x.Name).Select(x => new { x.FirstOrDefault().Name, Teams = x });

            //Act
            foreach (var team in teams)
            {
                Assert.Equal(1, team.Teams.Count());
            }
        }

        [Fact]
        public void Each_team_should_have_players()
        {
            //Arrange
            var teams = _sportUnitOfWork.Teams.FindAll();

            //Act & assert
            foreach (var team in teams)
            {
                var players = team.TeamMembers.Select(x => x.Player);
                Console.WriteLine(team.Name);
                Assert.True(players.Any());
            }
        }

        [Fact]
        public void Each_team_should_play_at_least_once()
        {
            //Arrange
            var teams = _sportUnitOfWork.Teams.WithFixtures();

            //Act
            var atLeastOneFixture = teams.SelectMany(x => x.GroupTeams.SelectMany(gt => gt.FixtureParticipants.Select(fp => fp.Fixture != null)))
                 .All(x => x);

            //Assert
            Assert.True(atLeastOneFixture);
        }

        [Fact]
        public void Each_team_should_not_play_several_times_per_one_day()
        {
            //Arrange
            Expression<Func<GroupTeam, object>> includeFixture = x => x.FixtureParticipants.Select(y => y.Fixture);
            var teamsGroupedByTeamId = _sportUnitOfWork.GroupTeams.AllIncluding(includeFixture).GroupBy(x => new { x.Team.Id })
                .Select(x => new { x.FirstOrDefault().Team.Name, Fixtures = x.SelectMany(y => y.FixtureParticipants.Select(f => f.Fixture)).OrderBy(f => f.Date).ThenBy( f => f.MatchDay) });
            var result = true;
            var resultMessage = string.Empty;

            //Act
            foreach (var groupedTeam in teamsGroupedByTeamId)
            {
                if (groupedTeam.Fixtures.Select(x => x.Date).Distinct().Count() ==
                    groupedTeam.Fixtures.Select(x => x.Date).Count()) continue;
                result = false;
                resultMessage += string.Format("\n\n{0} team has fixtures:", groupedTeam.Name);
                foreach (var fixture in groupedTeam.Fixtures)
                {
                    //var theFixture = _sportUnitOfWork.Fixtures.AllIncluding(x => x.FixtureParticipants.Select(fp => fp.GroupTeam.Team)).FirstOrDefault(x => x.Id == fixture.Id);
                    Fixture fixture1 = fixture;
                    var theFixture = _sportUnitOfWork.Fixtures.WithEventsAndTeams().FirstOrDefault(x => x.Id == fixture1.Id);
                    resultMessage += string.Format("\n      {0} {1} matchDay {2} stage {3}", fixture.Date.Value.ToShortDateString(), theFixture, fixture.MatchDay, theFixture.Group.Stage);
                }
            }

            //Assert
            Assert.True(result, resultMessage);
        }

        [Fact]
        public void Each_team_should_have_champs_without_overlapping_dates()
        {
            //Arrange
            var teams = _sportUnitOfWork.Teams.WithFixturesAndChamp().ToList();

            //Act & assert
            foreach (var team in teams)
            {
                var champs = team.Champs.Where(x => !x.IsFriendly);
                var dateRanges = champs.Select(x => x.Start != null && x.End != null ? new DateRange((DateTime)x.Start, (DateTime)x.End) : null);
                
                // Should not throw exception
                var result = dateRanges.OrderBy(x => x).ToArray();
            }
        } 
    }
}