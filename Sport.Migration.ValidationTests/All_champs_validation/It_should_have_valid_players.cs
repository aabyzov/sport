﻿//using System.Data.Entity;

using System.Linq;
using Sport.Domain.Models;
using Sport.Tests.Common;
using Xunit;

namespace Sport.Migration.ValidationTests.All_champs_validation
{
    public class It_should_have_valid_players : IntegrationBase
    {
        [Fact]
        public void Each_player_can_not_be_in_several_national_teams()
        {
            //Arrange
            var players = _sportUnitOfWork.Players.AllIncluding(x => x.TeamMembers.Select(tm => tm.Team));
            var internationalLeague = _sportUnitOfWork.Leagues.International();

            //Act & assert
            foreach (var player in players)
            {
                Assert.True(player.TeamMembers.Count(x => x.Team.LeagueId == internationalLeague.Id) <= 1);
            }
        }

        [Fact]
        public void Each_player_should_miss_appropriate_matches_when_on_injury()
        {
            //Arrange
            var players = _sportUnitOfWork.Players.WithAnyEventsAndTeamOutOfFixturePlayers();

            //Act & assert
            foreach (var player in players)
            {
                var thePlayer = player;
                foreach (var fixture in player.Fixtures)
                {
                    var footballEvents = fixture.FootballEvents.ToList();

                    var injuries = footballEvents.Where(x => x.IsInjury && x.PlayerId == thePlayer.Id).Cast<FootballInjury>().ToArray();
                    if (!injuries.Any()) continue;

                    foreach (var injury in injuries)
                    {
                        var team = injury.FixtureParticipant.GroupTeam.Team;

                        var fixtures = team.Fixtures.ToList();
                        var currentFixture = injury.FixtureParticipant.Fixture;
                        var currentFixtureIndex = fixtures.IndexOf(currentFixture);

                        var totalMissMatchCount = injury.MissedMatchCountInjury.HasValue
                                                      ? injury.MissedMatchCountInjury.Value
                                                      : 0;
                        var nextFixtures =
                            fixtures.Skip(currentFixtureIndex + 1)
                                    .Take(totalMissMatchCount);

                        foreach (var nextFixture in nextFixtures)
                        {
                            var homeOrAwayTeam =
                                nextFixture.FixtureParticipants.FirstOrDefault(x => x.GroupTeam.TeamId == team.Id);
                            Assert.NotNull(homeOrAwayTeam);
                            var outOfFixturePlayer =
                                homeOrAwayTeam.OutOfFixturePlayers.FirstOrDefault(x => x.PlayerId == thePlayer.Id);
                            Assert.NotNull(outOfFixturePlayer);
                        }
                    }
                }
            }

            
        }

        [Fact]
        public void Each_player_should_miss_appropriate_official_matches_when_on_red_card()
        {
            //Arrange
            var players = _sportUnitOfWork.Players.WithAnyEventsAndTeamOutOfFixturePlayers();

            //Act & assert
            foreach (var player in players)
            {
                var thePlayer = player;
                foreach (var fixture in player.Fixtures)
                {
                    var footballEvents = fixture.FootballEvents.ToList();
                    
                    var redCards = footballEvents.Where(x => x.IsRedCard && x.PlayerId == thePlayer.Id).Cast<FootballRedCard>().ToArray();
                    if (!redCards.Any()) continue;

                    foreach (var redCard in redCards)
                    {
                        var team = redCard.FixtureParticipant.GroupTeam.Team;

                        var fixtures = team.OfficialFixtures.ToList();
                        var currentFixture = redCard.FixtureParticipant.Fixture;
                        var currentFixtureIndex = fixtures.IndexOf(currentFixture);

                        var totalMissMatchCount = redCard.MissedMatchCountRedCard;
                        var nextFixtures =
                            fixtures.Skip(currentFixtureIndex + 1)
                                    .Take(totalMissMatchCount);

                        foreach (var nextFixture in nextFixtures)
                        {
                            var homeOrAwayTeam =
                                nextFixture.FixtureParticipants.FirstOrDefault(x => x.GroupTeam.TeamId == team.Id);
                            Assert.NotNull(homeOrAwayTeam);
                            var outOfFixturePlayer =
                                homeOrAwayTeam.OutOfFixturePlayers.FirstOrDefault(x => x.PlayerId == thePlayer.Id);
                            Assert.NotNull(outOfFixturePlayer);
                        }
                    }
                }
            }
        }

        [Fact]
        public void Each_player_should_miss_appropriate_official_matches_when_on_yellow_card()
        {
            //Arrange
            var players = _sportUnitOfWork.Players.WithAnyYellowCardEvents();

            //Act & assert
            foreach (var player in players)
            {
                var thePlayer = player;
                foreach (var teamMember in player.TeamMembers)
                {
                    var team = teamMember.Team;
                    var fixtures = team.OfficialFixtures.OrderBy(x => x.Date).ToList();
                    var groupedFixtures = fixtures.GroupBy(x => x.Champ).Select(x => new {Champ = x.Key, Fixtures = x});
                    foreach (var groupedFixture in groupedFixtures)
                    {
                        var champ = groupedFixture.Champ;
                        var playerYellowsTotal = 0;
                        foreach (var fixture in groupedFixture.Fixtures)
                        {
                            var playerYellows =
                                fixture.FootballEvents.Where(x => x.PlayerId == thePlayer.Id && x.IsYellowCard);
                            if (!playerYellows.Any()) continue;
                            var playerYellowsCount = playerYellows.Count();
                            if (playerYellowsCount == 1)
                            {
                                playerYellowsTotal++;
                            }
                            if (playerYellowsCount == 2)
                            {
                                var redCard =
                                    fixture.FootballEvents.Where(x => x.PlayerId == thePlayer.Id && x.IsRedCard);
                                Assert.NotNull(redCard);
                            }
                            if (playerYellowsTotal == 0 || playerYellowsTotal%champ.YellowsToMissNextMatch != 0)
                                continue;
                            var nextFixture =
                                fixtures.Skip(fixtures.IndexOf(fixtures.FirstOrDefault(x => x.Id == fixture.Id)) + 1)
                                        .Take(1).FirstOrDefault();
                            // This is the case when player got card in last match of champ, but new one is not generated yet
                            if (null == nextFixture) continue;
                            var nextFixtureParticipant =
                                nextFixture.FixtureParticipants.FirstOrDefault(x => x.GroupTeam.Team.Id == team.Id);
                            Assert.NotNull(nextFixtureParticipant);
                            var outOfFixturePlayer =
                                nextFixtureParticipant.OutOfFixturePlayers.FirstOrDefault(
                                    x => x.PlayerId == thePlayer.Id);
                            Assert.NotNull(outOfFixturePlayer);
                        }
                    }
                }
            }

        }
    }
}