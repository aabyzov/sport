﻿//using System.Data.Entity;

using System.Linq;
using Sport.Tests.Common;
using Xunit;

namespace Sport.Migration.ValidationTests.All_champs_validation
{
    public class It_should_have_valid_stages : IntegrationBase
    {
        [Fact]
        public void Each_stage_should_have_valid_number_of_fixtures()
        {
            foreach (var @group in _sportUnitOfWork.Groups.FindAll())
            {
                var groupVar = @group;
                //var fixtures = _sportUnitOfWork.Fixtures.FindAllBy(x => x.FixtureParticipants.FirstOrDefault().GroupTeam.Group.Id == groupVar.Id);

                var fixtures = _sportUnitOfWork.Fixtures.FindAllByGroupId(groupVar.Id);
                var fixtureCountToCompare = GetMatchCount(group.GroupTeams.Count)*group.Stage.RoundCount;
                Assert.Equal(fixtureCountToCompare, fixtures.Count());
            }
        }

        public int GetMatchCount(int teamNumber)
        {
            var result = 1;
            if (teamNumber > 2)
            {
                double average = ((teamNumber - 1) / 2.0);
                result = (int)(average * teamNumber);
            }
            return result;
        }

        [Fact]
        public void Each_stage_should_have_at_least_one_group_and_fixture()
        {
            var eachStageHasOneGroupAtLeast = _sportUnitOfWork.Stages.AllIncluding(x => x.Groups).Select(x => x.Groups.Any()).All(x => x);
            Assert.True(eachStageHasOneGroupAtLeast);

            var eachGroupHasOneFixtureAtLeast =
                _sportUnitOfWork.Groups.AllIncluding(
                    x => x.GroupTeams.Select(gt => gt.FixtureParticipants.Select(fp => fp.Fixture)))
                                .SelectMany(
                                    x =>
                                    x.GroupTeams.SelectMany(
                                        gt => gt.FixtureParticipants.Select(fp => fp.Fixture != null))).All(x => x);
            Assert.True(eachGroupHasOneFixtureAtLeast);
        }
        
        [Fact]
        public void Each_stage_should_have_not_repeated_teams()
        {
            foreach (var stage in _sportUnitOfWork.Stages.AllIncluding(x => x.Groups.Select(g => g.GroupTeams.Select(gt => gt.Team))))
            {
                foreach (var @group in stage.Groups)
                {
                    var groupTeams = @group.GroupTeams.GroupBy(x => x.TeamId).Where(x => x.Count() > 1);
                    Assert.True(!groupTeams.Any());
                }
            }
        }
        
    }
}