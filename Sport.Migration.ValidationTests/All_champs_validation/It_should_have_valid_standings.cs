﻿using System.Linq;
using Should;
using Sport.Common.Core;
using Sport.Domain.Services.Templates;
using Sport.Tests.Common;
using Xunit;

namespace Sport.Migration.ValidationTests.All_champs_validation
{
    public class It_should_have_valid_standings : IntegrationBase
    {
        [Fact]
        public void It_should_have_valid_europe_qualif_standings()
        {
            //Arrange
            var standingsManager = CreateStandingsManager();
            var champ = _sportUnitOfWork.Champs.WorldCup2006WithQualificationWithTeams().FirstOrDefault(x => x.Zone.Name == Constants.Zones.Europe);
            var groups = champ.Stages.First().Groups;

            //Act
            foreach (var @group in groups)
            {
                var standings = standingsManager.BuildStandings(@group, new WorldCupTemplate());
                Write(standings);
            }

            //Assert
            var firstGroup = groups.First();
            var standings1 = standingsManager.BuildStandings(firstGroup, new WorldCupTemplate());
            standings1.Teams[0].Team.Team.Name.ShouldEqual("Slovakia");
            standings1.Teams[1].Team.Team.Name.ShouldEqual("Turkey");
        }
 
    }
}