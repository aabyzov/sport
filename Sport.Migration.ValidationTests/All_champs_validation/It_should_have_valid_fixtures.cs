﻿using System.Linq;
using Sport.Tests.Common;
using Xunit;

namespace Sport.Migration.ValidationTests.All_champs_validation
{
    public class It_should_have_valid_fixtures_for_all_champs : IntegrationBase
    {
        [Fact]
        internal void Each_football_fixture_should_have_valid_participants()
        {
            //Arrange
            var fixtures = _sportUnitOfWork.Fixtures.WithEventsAndTeams().ToList();

            //Act & assert
            foreach (var fixture in fixtures)
            {
                var homeTeam = fixture.FixtureParticipants.FirstOrDefault(x => x.IsHome);
                var awayTeam = fixture.FixtureParticipants.FirstOrDefault(x => !x.IsHome);
                Assert.NotNull(homeTeam);
                Assert.NotNull(awayTeam);
                Assert.Equal(2, fixture.FixtureParticipants.Count);
            }
        }

        [Fact(Skip = "Not implemented")]
        public void Each_football_fixture_date_should_be_within_range_of_champ_period()
        {
            //Arrange
            

            //Act


            //Assert

        }


        [Fact]
        public void Each_football_fixture_should_have_home_and_away_score()
        {
            //Arrange
            var fixtures = _sportUnitOfWork.Fixtures.FindAll();

            //Act & assert
            foreach (var fixture in fixtures)
            {

                var homeTeam = fixture.Home;
                var awayTeam = fixture.Away;
                var homeScore = homeTeam.Score;
                var awayScore = awayTeam.Score;
                if (homeScore.HasValue)
                {
                    Assert.True(awayScore.HasValue);
                }
            }
        }

        [Fact]
        public void Each_football_fixture_should_have_score_the_same_as_based_on_the_events()
        {
            //Arrange
            var fixtures = _sportUnitOfWork.Fixtures.WithEventsAndTeams().ToList();

            //Act & assert
            foreach (var fixture in fixtures)
            {
                if (!fixture.IsPlayed) continue;
                Assert.Equal(fixture.Home.Score, fixture.HomeScoreBasedOnEvents);
                Assert.Equal(fixture.Away.Score, fixture.AwayScoreBasedOnEvents);
            }
        }
    }
}