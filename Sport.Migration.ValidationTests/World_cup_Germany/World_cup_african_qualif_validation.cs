﻿using System;
using System.Linq;
using Should;
using Sport.Common.Core;
using Sport.Tests.Common;
using Xunit;

namespace Sport.Migration.ValidationTests.World_cup_Germany
{
    public class World_cup_african_qualif_validation : IntegrationBase
    {
        [Fact]
        public void It_should_have_valid_matchday_number()
        {
            //Arrange
            var fixtures = _sportUnitOfWork.Fixtures.WithEventsAndTeams().Where(x => x.FixtureParticipants.FirstOrDefault().GroupTeam.Group.Stage.Champ.Zone.Name == Constants.Zones.Africa).OrderBy(x => x.Date);

            //Act
            foreach (var fixture in fixtures)
            {
                Write(fixture);
            }

            //Assert
            fixtures.Select(x => x.MatchDay).Max().ShouldEqual(6);
        }

        [Fact]
        public void It_should_have_one_stage_2_groups()
        {
            //Arrange
            var champ = _sportUnitOfWork.Champs.FindOneBy(x => x.Zone.Name == Constants.Zones.Africa);

            //Act && Assert
            champ.Stages.Count.ShouldEqual(1);
            var groups = champ.Stages.SelectMany(x => x.Groups).ToArray();
            groups.Count().ShouldEqual(2);
            Array.ForEach(groups, x => x.Fixtures.Count().ShouldEqual(12));
        }

 
    }
}