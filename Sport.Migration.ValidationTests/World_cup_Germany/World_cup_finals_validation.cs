﻿//using Sport.Migration.Helpers;
using System.Linq;
using Sport.Common.Core;
using Sport.Tests.Common;
using Xunit;

//using DbExtensions;

namespace Sport.Migration.ValidationTests.World_cup_Germany
{
    public class World_cup_finals_validation : IntegrationBase
    {
        [Fact]
        public void World_cup_finals_should_have_32_participants()
        {
            //Arrange
            var germanyWorldCup = _sportUnitOfWork.Champs.WorldCup2006FinalsWithTeams();

            //Act & assert
            Assert.Equal(32, germanyWorldCup.Teams.Count());
        }

        [Fact]
        public void World_cup_finals_should_have_valid_participants_per_zones()
        {
            //Arrange
            var germanyWorldCup = _sportUnitOfWork.Champs.WorldCup2006FinalsWithTeams();

            //Act & assert
            Assert.Equal(17, germanyWorldCup.Teams.Count(x => x.Zone.Name == "UEFA"));
            Assert.Equal(3, germanyWorldCup.Teams.Count(x => x.Zone.Name == "AFC"));
            Assert.Equal(4, germanyWorldCup.Teams.Count(x => x.Zone.Name == "CAF"));
            Assert.Equal(5, germanyWorldCup.Teams.Count(x => x.Zone.Name == "CONMEBOL"));
            Assert.Equal(3, germanyWorldCup.Teams.Count(x => x.Zone.Name == "CONCACAF"));
        }
       
        [Fact]
        public void World_cup_finals_should_have_more_than_2_played_stages()
        {
            //Arrange
            var germanyWorldCup = _sportUnitOfWork.Champs.WorldCup2006FinalsWithFixtures();
            var stages = germanyWorldCup.Stages;

            //Act
            var stagesCount = stages.Count(x => x.IsPlayed);

            //Assert
            Assert.True(stagesCount >= 2);
        }

        [Fact]
        public void World_cup_finals_should_have_more_than_56_played_matches()
        {
            //Arrange
            var germanyWorldCup = _sportUnitOfWork.Champs.WorldCup2006FinalsWithFixtures();
            var fixtures = germanyWorldCup.Fixtures;

            //Act & assert
            Assert.True(fixtures.Count(x => x.IsPlayed) >= 56);
        }

        [Fact]
        public void World_cup_finals_each_group_on_group_stage_should_have_6_played_matches()
        {
            //Arrange
            var germanyWorldCup = _sportUnitOfWork.Champs.WorldCup2006FinalsWithFixtures();
            var groupStage = germanyWorldCup.Stages.FirstOrDefault(x => x.Name.StartsWith(Constants.GroupStage));

            //Act & assert
            Assert.NotNull(groupStage);
            foreach (var @group in groupStage.Groups)
            {
                var count = @group.Fixtures.Count(x => x.IsPlayed);
                Assert.Equal(6, count);
            }
        }

        [Fact]
        public void World_cup_finals_each_group_team_should_play_3_matches()
        {
            //Arrange
            var germanyWorldCup = _sportUnitOfWork.Champs.WorldCup2006FinalsWithFixtures();
            var groupStage = germanyWorldCup.Stages.FirstOrDefault(x => x.Name.StartsWith(Constants.GroupStage));

            //Act & assert
            Assert.NotNull(groupStage);
            foreach (var groupTeam in groupStage.GroupTeams)
            {
                Assert.Equal(3, groupTeam.Fixtures.Count(f => f.IsPlayed));
            }
        }
    }
}