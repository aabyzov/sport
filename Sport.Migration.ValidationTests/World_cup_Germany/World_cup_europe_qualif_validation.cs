﻿using System;
using System.Linq;
using Should;
using Sport.Common.Core;
using Sport.Tests.Common;
using Xunit;

namespace Sport.Migration.ValidationTests.World_cup_Germany
{
    public class World_cup_europe_qualif_validation : IntegrationBase
    {
        [Fact]
        public void It_should_have_valid_number_of_groups()
        {
            //Arrange
            var groups = _sportUnitOfWork.Groups.FindAllBy(x => x.Stage.Champ.Zone.Name == Constants.Zones.Europe).OrderBy(x => x.Name);

            //Assert
            groups.Count().ShouldEqual(8);
        }


        [Fact]
        public void It_should_have_3_teams_in_last_group()
        {
            //Arrange
            var groups = _sportUnitOfWork.Groups.FindAllBy(x => x.Stage.Champ.Zone.Name == Constants.Zones.Europe).OrderBy(x => x.Name).ToList();

            //Assert
            groups[7].GroupTeams.Count.ShouldEqual(3);
            for (var i = 0; i < 7; i++)
            {
                groups[i].GroupTeams.Count.ShouldEqual(4);
            }
        }

        [Fact]
        public void It_should_have_valid_matchdays_for_group_of_3_teams()
        {
            var groups = _sportUnitOfWork.Groups.FindAllBy(x => x.Stage.Champ.Zone.Name == Constants.Zones.Europe).OrderBy(x => x.Name).ToList();
            var groupWithThreeTeams = groups[7];
            var fixtures = _sportUnitOfWork.Fixtures.WithEventsAndTeams().Where(x => x.FixtureParticipants.FirstOrDefault().GroupTeam.GroupId == groupWithThreeTeams.Id).ToList().OrderBy(x => x.Date);
            foreach (var fixture in fixtures)
            {
                Console.WriteLine(fixture);
            }
        }
    }
}