﻿//using Sport.Migration.Helpers;
using System.Linq;
using Sport.Tests.Common;
using Xunit;

namespace Sport.Migration.ValidationTests.World_cup_Germany
{
    public class World_cup_with_qualification_validation : IntegrationBase
    {
        [Fact]
        //This is including teams in friendly matches
        public void World_cup_should_have_57_teams_at_all()
        {
            //Arrange
            var germanyWorldCup = _sportUnitOfWork.ChampSeasons.WorldCup2006WithTeams();

            //Act & assert
            Assert.Equal(57, germanyWorldCup.Teams.Count());
        }

        [Fact]
        //This is including teams in friendly matches
        public void World_cup_should_have_valid_participants_per_zones()
        {
            //Arrange
            var teams = _sportUnitOfWork.Champs.GetWorldCup2006Teams();
            //var teams = worldCupChamps.SelectMany(x => x.Teams);

            //Act & assert
            Assert.Equal(32, teams.Count(x => x.Zone.Name == "UEFA"));
            Assert.Equal(5, teams.Count(x => x.Zone.Name == "AFC"));
            Assert.Equal(8, teams.Count(x => x.Zone.Name == "CAF"));
            Assert.Equal(8, teams.Count(x => x.Zone.Name == "CONMEBOL"));
            Assert.Equal(4, teams.Count(x => x.Zone.Name == "CONCACAF"));
        }
    }
}