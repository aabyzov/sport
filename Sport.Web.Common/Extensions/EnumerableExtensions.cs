﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Sport.Web.Common.Extensions
{
    public static class EnumerableExtensions
    {
        public static IList<SelectListItem> MapToSelectItems<T>(this IEnumerable<T> itemsToMap, Func<T, string> textProperty, Func<T, string> valueProperty, Predicate<T> isSelected)
        {
            var result = new List<SelectListItem>();

            foreach (var item in itemsToMap)
            {
                result.Add(new SelectListItem
                {
                    Value = valueProperty(item),
                    Text = textProperty(item),
                    Selected = isSelected(item)
                });
            }
            return result;
        }

        public static SelectList ToSelectList<TEnum>(this TEnum enumObj)
            where TEnum : struct, IComparable, IFormattable, IConvertible
        {
            var values = from TEnum e in Enum.GetValues(typeof(TEnum))
                         select new { Id = (e).ToString(), Name = e.ToString() };
            return new SelectList(values, "Id", "Name", enumObj);
        }
    }
}